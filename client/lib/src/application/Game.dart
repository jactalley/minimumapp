part of application;

class Game extends GameApplication
{
  static Game get               app               => Application.app as Game;
  static FacebookInstant        sSocialNetwork    = new FacebookInstant();
  static Uuid                   sUUIDGenerator    = new Uuid();
  //static LocalizationManager    sLocalization     = new LocalizationManager( ENGLISH_XML, LANGUAGES_XML, LANGUAGE_VERSION );
  
  static const String     VERSION = "18.3.23.1";
  
  // we are treating these as const, but they are write-once properties pulled from html.
  static String           LANGUAGE_VERSION = "";
  static String           MAP_BACKGROUND_VERSION = "";
  static String           MAP_FOREGROUND_VERSION = "";
  static String           MAP_MARKER_VERSION = "";
  static String           PRODUCT_VERSION = "";
  static String           MARKET_VERSION = "";
  static String           MESSAGE_OF_THE_DAY = "";
  
  // dev (local testing)
  static const String     FACEBOOK_APPLICATION_ID = "718161331665133";
  static const String   	FACEBOOK_APPLICATION_URI = "https://apps.facebook.com/pearlsdev/";
  static const String     OPEN_GRAPH_OBJECTS_URL = "http://dkpstaging.dragonkinggames.com/server/og/";
  static const String     APP_RESOURCE_PATH = "assets/";
  static const String     STATIC_RESOURCE_PATH = "http://localhost:8080/assets/";
  static const String     DYNAMIC_RESOURCE_PATH = "http://localhost:8080/assets/";
  static const String     MARKET_ITEM_IMAGES_PATH = "assets/images";
  static const String     SERVER_URI = "https://localhost:8443/gameupdate";
  static const bool       LIVES_CHEAT = true;
  static const bool       ALLOW_EDITOR = true;
  
  // staging (remote testing)
//  static const String     FACEBOOK_APPLICATION_ID = "493269320821003";// pearls staging app
//  static const String   	FACEBOOK_APPLICATION_URI = "https://apps.facebook.com/pearlsdev/";
//  static const String     OPEN_GRAPH_OBJECTS_URL = "http://dkpstaging.dragonkinggames.com/dkp/og/";
//  static const String     APP_RESOURCE_PATH = "https://s3.amazonaws.com/pstaging.dragonkinggames.com/app/";
//  static const String     STATIC_RESOURCE_PATH = "https://s3.amazonaws.com/pstaging.dragonkinggames.com/data/";
//  static const String     DYNAMIC_RESOURCE_PATH = "https://s3.amazonaws.com/pstaging.dragonkinggames.com/data/";
//  static const String     MARKET_ITEM_IMAGES_PATH = "assets/images";
//  static const String     SERVER_URI = "//dkpstaging.dragonkinggames.com/dkp/messagebroker/amf";
//  static const bool       LIVES_CHEAT = false;
//  static const bool       ALLOW_EDITOR = true;
  
  static const String     ALL_POPUPS_CLOSED = "AllPopupsClosed";
  
  
  // valid state values
  static const int        LOADING     = 0x01;
  static const int        PLAYING     = 0x02;
  
  
  static const num DEFAULT_APP_WIDTH = 1600.0;
  static const num DEFAULT_APP_HEIGHT = 900.0;
  
  static const int TOOL_TIP_X_OFFSET = 0;
  static const int TOOL_TIP_Y_OFFSET = 32;
  
  ResourceManager loadingResources;
  Group         _loadingGroup;
  
  bool          _permissionsComplete = false;
  bool          _authorizationComplete = true;//assume user has authorized app until FB says otherwise
  bool          _resourcesLoaded = false;
  
  static bool   sInitialized      = false;
  
  bool          _validLanguage;               // was the user's language cookie valid during startup?
  
  TextElement   loadingLabel1;
  TextElement   loadingLabel2;
  TextElement   versionLabel;
  String        _loadingLabel1Text = "L1";
  String        _loadingLabel2Text = "L2";
  DefaultButton _playButton;
  
  SoundPlayListSequence mainMusicPlayList;
  SoundPlayListSequence storyMusicPlayList;
  SoundPlayListSequence pregameMusicPlayList;
  SoundPlayListSequence gameMusicPlayList;
  SoundPlaylistSequencer musicSequencer = new SoundPlaylistSequencer();
  SoundPlayListDirect   appSoundFX;
  SoundPlayListDirect   gameSoundFX;
  
  Game() : super()
  {
    awaitsCreateElements();
    
    name="application";
    explicitWidthPercent = 1.0;
    explicitHeightPercent = 1.0;
    initialStageWidth = 800;
    initialStageHeight = 600;
    stageOptions.stageScaleMode = StageScaleMode.NO_SCALE;
    stageOptions.stageAlign = StageAlign.TOP_LEFT;
    stageOptions.backgroundColor = Color.Blue;
    
    //stageOptions.renderEngine = RenderEngine.WebGL;
    stageOptions.renderEngine = RenderEngine.Canvas2D;
    
    stageOptions.inputEventMode = InputEventMode.MouseAndTouch;
    
    // for friend image loading.
    BitmapData.defaultLoadOptions.corsEnabled=true;
    
    DisplayElement.cacheAsBitmapAllFilteredObjects = true;
    
    ResourcePaths.appResourcePath = APP_RESOURCE_PATH;
    ResourcePaths.staticResourcePath = STATIC_RESOURCE_PATH;
    ResourcePaths.dynamicResourcePath = DYNAMIC_RESOURCE_PATH;
    
    states = new States()
      ..addState(LOADING)
      ..addState(PLAYING,onEnter:startPlaying);
    
    initializeSystems();
    
    this.addEventListener( Event.ENTER_FRAME, enterFrame );
  }
  
  @override
  void preInitializeElements()
  {
    super.preInitializeElements();
    _setScaleForStageSize();
  }
  
  @override
  Future  createElements() async
  {
    loadingResources = new ResourceManager(true);
    await loadingResources.load();
    addElements
    ([
      versionLabel = Label_() ..text=VERSION ..left=6 ..bottom=2 ..height=20 ..color=0xffffffff ..strokeColor=0xff000000 ..strokeWidth=2 ..visible=true ..fontSize=16,
      
      _playButton = DefaultButton_() ..horizontalCenter=0 ..verticalCenter=-20 ..width=100 ..height=39 ..label="Take Turn" ..onClick.listen(_takeTurn) ..includeIn=PLAYING,
      DefaultButton_() ..horizontalCenter=0 ..verticalCenter=20 ..width=100 ..height=39 ..label="Change matches" ..onClick.listen(_changeMatches) ..includeIn=PLAYING,
      DefaultButton_() ..horizontalCenter=0 ..verticalCenter=60 ..width=100 ..height=39 ..label="Reset" ..onClick.listen(_reset) ..includeIn=PLAYING
    ]);
    
    dispatchEvent(new Event(INITIALIZED, false));
    completeInitialization();
  }
  
  void _takeTurn(Event e)
  {
    Map data = new Map();
    data["myTurn"] = true;
  
    sSocialNetwork.postGameUpdate(data);

    sSocialNetwork.requestGameData(data,_setPlayability);
  }
  
  void _changeMatches(Event e)
  {
    sSocialNetwork.changeContext(_setPlayability);
  }
  
  void _reset(Event e)
  {
    Map data = new Map();
    data["myTurn"] = true;
    
    sSocialNetwork.postGameUpdate(data);

    sSocialNetwork.requestGameData(data,_setPlayability);
  }
  
  void _setPlayability(Map data)
  {
    print(data.toString());
    _playButton.enabled = (data.containsKey("myTurn") && data["myTurn"]);
//    sSocialNetwork.loadData(new List<String>.from(["myTurn"]))
//        .then((data)
//        {
//          _playButton.enabled = getProperty(data,"myTurn") != null ? getProperty(data,"myTurn"):true;
//        });
  }
  
  void _releaseLoadingResources()
  {
    this.removeElement( _loadingGroup );
    _loadingGroup = null;
    loadingLabel1 = null;
    loadingLabel2 = null;
    if ( loadingResources != null )
    {
      loadingResources.dispose( );
      loadingResources = null;
    }
  }
  
  void _onMaintenanceWarningUpdate( Event event )
  {
//    if ( gameView != null && !MaintenanceDialog.currentlyShowing )
//    {
//      gameView.maintenanceDialog.show();
//    }
  }
  
  void _readSGGVars()
  {
    js.JsObject versionData = js.context["sggvars"];
    if ( versionData != null )
    {
      if ( versionData["langver"] != null )
      {
        LANGUAGE_VERSION = versionData["langver"];
      }
      if ( versionData["bgver"] != null )
      {
        MAP_BACKGROUND_VERSION = versionData["bgver"];
      }
      if ( versionData["fgver"] != null )
      {
        MAP_FOREGROUND_VERSION = versionData["fgver"];
      }
      if ( versionData["markerver"] != null )
      {
        MAP_MARKER_VERSION = versionData["markerver"];
      }
      if ( versionData["productver"] != null )
      {
        PRODUCT_VERSION = versionData["productver"];
      }
      if ( versionData["marketver"] != null )
      {
        MARKET_VERSION = versionData["marketver"];
      }
      if( versionData["motd"] != null )
      {
        MESSAGE_OF_THE_DAY = versionData["motd"];
      }
    }
  }
  
  void initializeSystems()
  {
    updateLoadingLabel1( "Talking to Facebook..." );
    
    _readSGGVars();
    
//    sSocialNetwork.addEventListener( Facebook.LOADING_UPDATE, _setLoadLabel2 );
    sSocialNetwork.initialize(FACEBOOK_APPLICATION_ID,_loadAssets);
    HTTPUtil.serverURL = SERVER_URI;
    
    Cookies.Instance.initialize();
    String musicSetting = Cookies.Instance.getCookie("MusicSetting");
    String soundSetting = Cookies.Instance.getCookie("SoundSetting");
    soundFXMuted = soundSetting == "true";
    musicMuted = musicSetting == "true";
    
    // if their language is invalid, give them the default for now, until we can make them pick...
    String languageCookie = Cookies.Instance.getCookie("Language");
//    _validLanguage = ( languageCookie != null && sLocalization.getLanguage( languageCookie ) != null );
//    if( _validLanguage )
//    {
//      sLocalization.loadLanguage( languageCookie );
//    }
//    else
//    {
//      sLocalization.loadLanguage( LocalizationManager.DEFAULT_LANGUAGE );
//    }
    
    // uncomment for testing that all strings are in localization system
    //sLocalization.loadLanguage( LocalizationManager.TEST_LANGUAGE );
  }
  
  void _loadAssets()
  {
    sSocialNetwork.updateLoadProgress(100.0);   //Tie this to actual loading progress once things are getting loaded, obviously
    
    sSocialNetwork.addEventListener(FacebookInstant.STARTED_EVENT, beginPlaying);
    sSocialNetwork.startGame();
  }
  
  void beginPlaying(Event e)
  {
    checkFullyLoaded(null);

    sSocialNetwork.requestGameData(new Map(),_setPlayability);
  }
  
  void _handleIncompleteSocialAuthorization( Event event )
  {
    updateLoadingLabel1( "Requesting Authorization..." );
    if ( this.initialized )
    {
      requestSocialAppAuthorization(null);
    }
    else
    {
      this.addEventListener( INITIALIZED, _handleIncompleteSocialAuthorizationPostInitialize );
    }
  }
  
  void _handleIncompleteSocialAuthorizationPostInitialize( Event event )
  {
    updateLoadingLabel1( "Requesting Authorization..." );
    this.removeEventListener( INITIALIZED, _handleIncompleteSocialAuthorizationPostInitialize );
    requestSocialAppAuthorization(null);
  }
  
  void requestSocialAppAuthorization( Event event )
  {
    if ( _resourcesLoaded )
    {
      _authorizationComplete = true;
//      BlueAlertDialog dialog = new BlueAlertDialog();
//      dialog.hasCancelButton = false;
//
//      dialog.addEventListener( BlueAlertDialog.DIALOG_COMPLETE, _userWillAuthorize );
//
//      dialog.setLocalizationKeyForTitle( "Need_Login" );
//      dialog.setLocalizationKeyForBody( "To_Use_App_Login_Required" );
//      dialog.setLocalizationKeyForAccept( "Login" );
//
//      dialog.depth = 1000;
//      dialog.show( this );
    }
    else
    {
      _authorizationComplete = false;
    }
  }
  
  void _userWillAuthorize( Event event )
  {
    //sSocialNetwork.userRequestedLogin();
  }
  
  void _handleIncompleteSocialPermissions( Event event )
  {
    updateLoadingLabel1( "Checking Permissions..." );
    
    _permissionsComplete = false;
    //sSocialNetwork.removeEventListener( Facebook.PERMISSIONS_INCOMPLETE, _handleIncompleteSocialPermissions );
    //sSocialNetwork.removeEventListener( Facebook.PERMISSIONS_COMPLETE, _handleCompleteSocialPermissions );
    
//    if ( sPlayer.serverData == null )
//    {
//      sPlayer.addEventListener( Player.PLAYER_DATA_AVAILABLE, _playerDataAvailableAfterIncompleteSocialPermissions );
//    }
//    else
//    {
      _playerDataAvailableAfterIncompleteSocialPermissions(null);
//    }
  }
  
  void _playerDataAvailableAfterIncompleteSocialPermissions( Event event )
  {
//    sPlayer.removeEventListener( Player.PLAYER_DATA_AVAILABLE, _playerDataAvailableAfterIncompleteSocialPermissions );
//
//    if ( sPlayer.serverData.totalLogins % 4 == 0 )
//    {
//      // every fourth login, we will ask player for full permissions...
//      BlueAlertDialog dialog = new BlueAlertDialog();
//      dialog.hasCancelButton = true;
//
//      dialog.addEventListener( BlueAlertDialog.DIALOG_COMPLETE, userGavePermission );
//      dialog.addEventListener( BlueAlertDialog.DIALOG_CANCELLED, userWitheldPermission );
//
//      dialog.setLocalizationKeyForTitle( "Need_Permissions" );
//      dialog.setLocalizationKeyForBody( "To_Enhance_Experience_Permissions" );
//
//      dialog.depth = 1000;
//      dialog.show(this);
//    }
//    else
//    {
      // most of the time we will just accept that the player
      // has denied some permissions (access to friends list)
      _handlePermissionsDenied();
//    }
  }
  
  void userGavePermission( Event event )
  {
//    BlueAlertDialog dialog = event.currentTarget as BlueAlertDialog;
//
//    dialog.removeEventListener( BlueAlertDialog.DIALOG_COMPLETE, userGavePermission );
//    dialog.removeEventListener( BlueAlertDialog.DIALOG_CANCELLED, userWitheldPermission );
    
//    List<String> missingPermissions = sSocialNetwork.missingPermissions;
//
//    if ( missingPermissions.length > 0 )
//    {
//      sSocialNetwork.addEventListener( Facebook.REQUEST_PERMISSIONS_CHECK_COMPLETE, _handlePermissionsComplete );
//      sSocialNetwork.requestFullPermissions();
//    }
  }
  
  void userWitheldPermission( Event event )
  {
//    BlueAlertDialog dialog = event.currentTarget as BlueAlertDialog;
//
//    dialog.removeEventListener( BlueAlertDialog.DIALOG_COMPLETE, userGavePermission );
//    dialog.removeEventListener( BlueAlertDialog.DIALOG_CANCELLED, userWitheldPermission );
    
    _handlePermissionsDenied();
  }
  
  void _handleCompleteSocialPermissions( Event event )
  {
    updateLoadingLabel1( "Checking Permissions Complete..." );
    
    sSocialNetwork.removeEventListener( Facebook.PERMISSIONS_INCOMPLETE, _handleIncompleteSocialPermissions );
    sSocialNetwork.removeEventListener( Facebook.PERMISSIONS_COMPLETE, _handleCompleteSocialPermissions );
    _handlePermissionsComplete(null);
  }
  
  void _handlePermissionsComplete( Event event )
  {
    updateLoadingLabel1( "Permissions Ready..." );
    
    _permissionsComplete = true;
    
    if ( sInitialized )
    {
      checkFullyLoaded(null);
    }
    else
    {
      checkFinalInitialization();
    }
  }
  
  void showDailyMessage()
  {
    if( MESSAGE_OF_THE_DAY != "" )
    {
//      MessageDialog dialog = new MessageDialog();
//      dialog.titleKey = "Message_Of_The_Day";
//      dialog.show( MESSAGE_OF_THE_DAY );
    }
  }
  
  void gameViewInitialized(Event event)
  {
    checkFinalInitialization();
  }
  
  void _handlePermissionsDenied()
  {
    // at this point there is no difference
    // between _handlePermissionsDenied & _handlePermissionsComplete
    _handlePermissionsComplete(null);
  }
  
  void checkFinalInitialization()
  {
    if ( !sInitialized && _permissionsComplete )
    {
      updateLoadingLabel1( "Managers Ready..." );
      
      sInitialized = true;
      
//      if ( sPlayer.playerDataAvailable )
//      {
//        updateLoadingLabel1( "Initialization Complete!" );
//        currentState = PLAYING;
//      }
//      else
//      {
//        updateLoadingLabel1( "Waiting on Facebook..." );
//
//        sPlayer.addEventListener( Player.PLAYER_DATA_AVAILABLE, checkFullyLoaded );
//        Game.app.gameView.addEventListener( GameView.MAP_READY, checkFullyLoaded );
//      }
    }
  }
  
  void checkFullyLoaded( Event event )
  {
//    if ( !sPlayer.playerDataAvailable )
//    {
//      String label = "Waiting on ";
//      //int count = 0;
//
//      label += "Player Data";
//      //count++;
//
//      label += "...";
//      updateLoadingLabel1( label );
//      return;
//    }
//    else if( !Game.app.gameView.mapReady )
//    {
//      updateLoadingLabel1( "Waiting on data...");
//      return;
//    }
    
    // everything seems to be loaded, start playing...
    updateLoadingLabel1( "Initialization Completed!" );
    //LevelAccessManager.Instance.playerLevelUpdated();
    currentState = PLAYING;
  }
  
  void _handleSocialUIWarning( Event event )
  {
    if ( fullscreen )
    {
      fullscreen = false;
    }
  }
  
  void updateLoadingLabel1(String text)
  {
    _loadingLabel1Text = text;
    if ( loadingLabel1 != null )
    {
      loadingLabel1.text = text;
    }
  }
  
  void updateLoadingLabel2(String text)
  {
    _loadingLabel2Text = text;
    if ( loadingLabel2 != null )
    {
      loadingLabel2.text = text;
    }
  }
  
  void _setLoadLabel2( Event event )
  {
    if ( event is TextDataEvent )
    {
      TextDataEvent textEvent = event;
      updateLoadingLabel2(textEvent.text);
    }
  }
  
  static bool sStartedPlaying = false;
  
  void startPlaying(int stateID)
  {
    return;
    if ( !sStartedPlaying )
    {
      sStartedPlaying = true;
      _releaseLoadingResources();
      initializeMetrics();
      
      if( !_validLanguage )
      {
//        String newLanguage = sLocalization.defaultLanguageForLocale( sPlayer.locale );
//        // if there is a locale default which this game supports, set it and begin playing.
//        // otherwise, set English and let them change it.
//        if( newLanguage != null && sLocalization.getLanguage( newLanguage ) != null )
//        {
//          loadLanguage( newLanguage );
//          _checkForInitialPuzzleDialog( null );
//        }
//        else
//        {
          loadLanguage( LocalizationManager.DEFAULT_LANGUAGE );
//          gameView.languagesDialog.addEventListener( LanguagesDialog.DIALOG_CLOSED, _checkForInitialPuzzleDialog );
//          gameView.languagesDialog.show();
//        }
      }
      else
      {
        _checkForInitialPuzzleDialog( null );
      }
    }
  }
  
  void _checkForInitialPuzzleDialog( Event event )
  {
//    gameView.languagesDialog.removeEventListener( LanguagesDialog.DIALOG_CLOSED, _checkForInitialPuzzleDialog );
    
    // first time loaded, so we need to show the preGameDialog?
//    if( Game.sPlayer.lastPuzzleSolved < 0 )
//    {
//      if ( Game.app.gameView.gameMap.mapMarkersLoaded )
//      {
//        _launchInitialPuzzleDialog(null);
//      }
//      else
//      {
//        Game.app.gameView.gameMap.addEventListener( GameMap.MAP_MARKERS_LOADED, _launchInitialPuzzleDialog );
//      }
//    }
//    else
//    {
      showDailyMessage();
//    }
  }
  
  void _launchInitialPuzzleDialog( Event event )
  {
    if ( event != null )
    {
//      Game.app.gameView.gameMap.removeEventListener( GameMap.MAP_MARKERS_LOADED, _launchInitialPuzzleDialog );
    }
//    Game.app.gameView.gameMap.showPreGameDialogForMarker( Game.app.gameView.gameMap.markerManager.mapMarkerByPuzzleID(0) );
    showDailyMessage();
  }
  
  void initializeMetrics()
  {
    // TODO: enable this when we are ready.
    /*
    if ( LOGGING_METRICS )
    {
      String metricsURL = METRICS_URL;
      String appName = METRICS_APP_NAME;
      String userID = sPlayer.serverData.userID;
      String sessionID = sServices.session;
      String locale = sPlayer.locale;
      Map properties = new Map();
      sPlayer.addPurchaseSummarySessionProperties( properties );

      EventManager.Instance.initializeSession( metricsURL, appName, userID, sessionID, locale, properties );

      /*
      String linkCode = sourceLinkCode;
      int linkCodeValue = int.parse(linkCode);
      if ( LinkCodes.isStreamPost(linkCodeValue) )
      {
        EventManager.Instance.wallpostAccepted( linkCode, sourceLinkSender );
      }
  */

      bool newUser = sPlayer.serverData.totalLogins == 1;
      if ( newUser )
      {
        EventManager.Instance.eventOccurred("CreateAccount"/*,{"linkCode":linkCode}*/);
      }
      EventManager.Instance.eventOccurred("Login"/*,{"linkCode":linkCode}*/);
    }
    */
  }
  
  void _setScaleForStageSize()
  {
    this.scaleX = this.scaleY = stage.stageHeight / DEFAULT_APP_HEIGHT;
  }
  
  @override
  void resized(Event event)
  {
    super.resized(event);
    _setScaleForStageSize();
//    gameView.onResize();
    dispatchEvent(new Event("Resized"));
  }
  
  num get stageWidth
  {
    if ( stage == null )
    {
      return DEFAULT_APP_WIDTH;
    }
    else
    {
      return stage.stageWidth;
    }
  }
  
  num get stageHeight
  {
    if ( stage == null )
    {
      return DEFAULT_APP_HEIGHT;
    }
    else
    {
      return stage.stageHeight;
    }
  }
  
  set fullscreen( bool value )
  {
    if (value)
    {
      app.stage.displayState = StageDisplayState.FULL_SCREEN;
    }
    else
    {
      app.stage.displayState = StageDisplayState.NORMAL;
    }
  }
  
  bool get fullscreen
  {
    return ( app.stage.displayState == StageDisplayState.FULL_SCREEN );
  }
  
  Future  loadGlobalResources()
  {
    //register cursors here
    
    resources = new ResourceManager();
    
    return resources.load();
  }
  
  void globalResourcesLoaded()
  {
    _resourcesLoaded = true;
    
    if ( !_authorizationComplete )
    {
      requestSocialAppAuthorization( null );
    }
  }
  
  Future  loadGlobalFonts()
  {
    Completer completer = new Completer();
    js.JsObject webFont = js.context["WebFont"];
    js.JsObject webFontConfig = new js.JsObject.jsify({
      "custom": { "families": ['Spicy', 'Panefresco', 'PanefrescoItalic'] },
      //"loading": () => print("loading fonts"),
      "active": () => completer.complete(null),
      "inactive": () => completer.completeError("Error loading fonts"),
      //"fontloading": (familyName, fvd) => print("fontloading: $familyName, $fvd"),
      //"fontactive": (familyName, fvd) => print("fontactive: $familyName, $fvd"),
      //"fontinactive": (familyName, fvd) => print("fontinactive: $familyName, $fvd")
    });
    
    webFont.callMethod("load", [webFontConfig]);
    return completer.future;
  }
  
  void  loadIntroMusic()
  {
    List<SoundDefinition> mainMusicSoundDefinitions =
    [
      SoundDefinition_() ..name="Music-Main01" ..url=staticPath("sound/music/tea/tea_ceremony.mp3") ..volume=0.5 ..looping=true ..startTime=94,
    ];
    
    List<SoundDefinition> storySoundDefinitions =
    [
      SoundDefinition_() ..name="Music-Intro"		..url=staticPath("sound/music/strut/panda_strut.mp3") ..volume=1.0 ..looping=false
    ];
    
    List<SoundDefinition> pregameSoundDefinitions =
    [
      SoundDefinition_() ..name="Music-PreGame"	..url=staticPath("sound/music/fuji/fuji_loop_percussion.mp3") ..volume=0.3 ..looping=false ..startTime=50.0
    ];
    
    List<SoundDefinition> gameSoundDefinitions =
    [
      SoundDefinition_() ..name="Music-MiniGame01"	..url=staticPath("sound/music/fuji/fuji_loop.mp3") ..volume=0.6 ..looping=false ..startTime=50.0,
      SoundDefinition_() ..name="Music-MiniGame02"	..url=staticPath("sound/music/fuji/fuji_loop2.mp3") ..volume=0.6 ..looping=false ..startTime=50.0
    ];
    
    mainMusicPlayList = new SoundPlayListSequence( Application.app.musicChannelManager, mainMusicSoundDefinitions );
    storyMusicPlayList = new SoundPlayListSequence( Application.app.musicChannelManager, storySoundDefinitions);
    pregameMusicPlayList = new SoundPlayListSequence( Application.app.musicChannelManager, pregameSoundDefinitions);
    gameMusicPlayList = new SoundPlayListSequence( Application.app.musicChannelManager, gameSoundDefinitions);
    
    musicSequencer.addPlayList("Main", mainMusicPlayList);
    musicSequencer.addPlayList("Story", storyMusicPlayList);
    musicSequencer.addPlayList("PreGame", pregameMusicPlayList);
    musicSequencer.addPlayList("Game", gameMusicPlayList);
    
    musicSequencer.startPlayList("Main");
  }
  
  void  loadGlobalAudio()
  {
    List<SoundDefinition> soundDefinitions =
    [
    
    ];
    appSoundFX = new SoundPlayListDirect( Application.app.soundChannelManager, soundDefinitions );
    appSoundFX.play();
    
    soundDefinitions =
    [
    
    ];
    gameSoundFX = new SoundPlayListDirect( GameApplication.app.gameSoundChannelManager, soundDefinitions );
    gameSoundFX.play();
  }
  
  @override
  bool get soundFXMuted => Application.app.soundChannelManager.isMuted;
  @override
  void set soundFXMuted( bool muted )
  {
    Application.app.soundChannelManager.muted = muted;
    GameApplication.app.gameSoundChannelManager.muted = muted;
    String setting = Cookies.Instance.getCookie("SoundSetting");
    if ( setting == null || ( setting == "true" && muted == false ) || ( setting == "false" && muted == true ) )
    {
      setting = muted ? "true" : "false";
      Cookies.Instance.setCookie( "SoundSetting", setting, expiresHours:24*365 );// cookie expires in 365 days
    }
  }
  
  @override
  bool get musicMuted => Application.app.musicChannelManager.isMuted;
  @override
  void set musicMuted( bool muted )
  {
    Application.app.musicChannelManager.muted = muted;
    String setting = Cookies.Instance.getCookie("MusicSetting");
    if ( setting == null || ( setting == "true" && muted == false ) || ( setting == "false" && muted == true ) )
    {
      setting = muted ? "true" : "false";
      Cookies.Instance.setCookie( "MusicSetting", setting, expiresHours:24*365 );// cookie expires in 365 days
    }
  }
  
  void loadLanguage( String language )
  {
    Cookies.Instance.setCookie( "Language", language, expiresHours:24*365 );  // cookie expires in 365 days
//    sLocalization.loadLanguage( language );
  }
  
  void loadLevelAccess()
  {
    //LevelAccessManager.Instance.initialize( LEVEL_ACCESS_XML );
  }
  
  int mLastFrameTime = -1;
  int mCurrentFrameTime = -1;
  int mDeltaFrameTime = 0;
  double mFrameRate = 0.0;
  int mFrameCount = 0;
  static const int AVERAGE_FRAME_COUNT = 5;
  
  void enterFrame( Event event )
  {
    mCurrentFrameTime = getTimer();
    if ( mLastFrameTime >= 0 )
    {
      mDeltaFrameTime += mCurrentFrameTime - mLastFrameTime;
      ++mFrameCount;
      if ( mFrameCount >= AVERAGE_FRAME_COUNT )
      {
        mFrameRate = AVERAGE_FRAME_COUNT*1000 / mDeltaFrameTime;
        mFrameCount = 0;
        mDeltaFrameTime = 0;
      }
    }
    if ( versionLabel != null )
    {
      versionLabel.text = "FPS: " + mFrameRate.toString();
    }
    mLastFrameTime = mCurrentFrameTime;
  }
  
  DisplayElement toolTipFactory( String toolTip )
  {
    return new TextElement.formatted(new TextFormat('Helvetica,Arial', 20, 0xFFf9c85a, bold:true, align:"center", verticalAlign:"center")) ..text=toolTip ..border=true ..borderColor=0xFFf9c85a ..background= true ..backgroundColor=0xFF543417 ..mouseIgnore=true ..leftMargin=8 ..topMargin=8 ..rightMargin=8 ..bottomMargin=8;
  }
}