package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class TransactionDataQueryProcessor implements DynamoDBQueryProcessor
{
	public List<TransactionData> list = new ArrayList<TransactionData>();

	public void	processItem( Map<String, AttributeValue> object )
	{
		TransactionData data = new TransactionData();
		data.setUserID( object.get("userID").getS() );
		data.setType( Integer.parseInt(object.get("type").getN()) );
		data.setTime( Long.parseLong( object.get("time").getN() ) );
		data.setSku( Integer.parseInt(object.get("sku").getN()) );
		data.setCount( Integer.parseInt(object.get("count").getN()) );
		data.setCoins( Integer.parseInt(object.get("coins").getN()) );
		data.setGems( Integer.parseInt(object.get("gems").getN()) );
		list.add( data );
	}

}
