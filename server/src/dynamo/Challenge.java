package dynamo;

import java.util.List;

public class Challenge 
{
	public static final String		STANDARD_CHALLENGE = "STANDARD_CHALLENGE";
	
	public static final String 		NEW = "NEW";
	public static final String 		VIEWING_RESULTS = "VIEWING_RESULTS";
	public static final String 		COMPLETE = "COMPLETE";
	
	public String			challengeID;
	public List<String> 	userIds;
	public List<Integer>	scores;
	public String 			type;
	public long 			timeoutTime;
	public String			status;
	public Integer			resultsViewed;
	public Integer			level;
	public Integer			powerLevel;
}
