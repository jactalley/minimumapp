package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class UserDataQueryProcessor implements DynamoDBQueryProcessor
{
	public List<UserData> list = new ArrayList<UserData>();

	public void	processItem( Map<String, AttributeValue> object )
	{
		UserData data = new UserData();
		data.setFacebookUserID( object.get("fbID").getS() );
		data.setUserId( object.get("userID").getS() );
		data.setFirstName( object.get("fname").getS() );
		data.setLastName( object.get("lname").getS() );
		
		AttributeValue av = object.get("crossAppID");
		data.setCrossAppID(  av != null ? av.getS() : "");
		list.add( data );
	}

}
