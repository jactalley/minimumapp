package dynamo;

import services.TransactionLog;


public class TransactionData 
{
	public static final int	TILE_TRADE 					= 1;
	public static final int	COIN_PURCHASE 				= 2;
	public static final int	GEM_PURCHASE 				= 3;
	public static final int	DAILY_BONUS 				= 4;
	public static final int	SUPPORT_ADJUSTMENT 			= 5;
	public static final int	SUPPORT_SCORE_ADJUSTMENT 	= 6;
	public static final int	SUPPORT_XP_ADJUSTMENT 		= 7;
	public static final int	PLAYER_DATA_BACKUP 			= 8;
	public static final int	GIFT 						= 9;
	public static final int	GAME_REWARD 				= 10;
	public static final int	COINS_CLAIMED 				= 11;
	public static final int	USE_ITEM 					= 12;
	public static final int	ENERGY_UPDATE 				= 13;
	public static final int	GAME_RESULT 				= 14;
	
	public TransactionData(){}
	public TransactionData( String userID, TransactionLog oldLog )
	{
		this.userID = userID;
		this.type = oldLog.getStatus();
		this.time = oldLog.getOrderTimestamp().getTime();
		this.sku = oldLog.getSku();
		this.count = 1;
		this.coins = oldLog.getCoins();
		this.gems = 0;
	}

	private String	userID;
	private int 	type;
	private long	time;
	private int 	sku;
	private int 	count;
	private int 	coins;
	private int 	gems;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getSku() {
		return sku;
	}
	public void setSku(int sku) {
		this.sku = sku;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public int getGems() {
		return gems;
	}
	public void setGems(int gems) {
		this.gems = gems;
	}
}
