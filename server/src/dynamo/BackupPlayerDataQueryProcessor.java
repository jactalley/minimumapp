package dynamo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import services.Transaction;
import services.TransactionType;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class BackupPlayerDataQueryProcessor implements DynamoDBQueryProcessor
{
	public List<DisputeData> list = new ArrayList<DisputeData>();
	
	static public int sUsersBackedUp = 0;

	public void	processItem( Map<String, AttributeValue> object )
	{
		try 
		{
			long			timeOfAction = Calendar.getInstance().getTime().getTime();
	
			String userID = object.get("userID").getS();
	
			object.put( "time", new AttributeValue().withN( Long.toString( timeOfAction ) ) );
			DynamoDBManager.WriteObject( DKMDynamoDB.BACKUP_PLAYER_DATA_TABLE, object );
		
			DKMDynamoDB.SaveTransaction( new Transaction( userID, TransactionType.DATA_BACKUP, timeOfAction ) );
			
			sUsersBackedUp++;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
