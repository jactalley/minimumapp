package dynamo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import services.MahjongPlayer;
import services.Transaction;
import services.TransactionResult;
import services.TransactionType;

import app.FBID;

//import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
//import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
//import com.amazonaws.services.dynamodbv2.model.ReturnValue;
//import com.amazonaws.services.dynamodbv2.model.UpdateItemResult;

public class DKMDynamoDB 
{
	static public final String		USER_DATA_TABLE 				= FBID.APP_STATIC_TABLES + ".UserData";
	static public final String		PLAYER_DATA_TABLE 				= FBID.APP_STATIC_TABLES + ".PlayerData";
	static public final String		LOGIN_TABLE 					= FBID.APP_STATIC_TABLES + ".LoginLog";
	static public final String		REQUEST_TABLE 					= FBID.APP_STATIC_TABLES + ".RequestLog";
	static public final String		TRANSACTION_TABLE 				= FBID.APP_STATIC_TABLES + ".TransactionLog";
	static public final String		TRANSACTIONS_PROCESSED_TABLE	= FBID.APP_STATIC_TABLES + ".TransactionsProcessed";
	static public final String		PURCHASE_TABLE 					= FBID.APP_STATIC_TABLES + ".PurchaseLog";
	static public final String		UNCLAIMED_PURCHASE_TABLE 		= FBID.APP_STATIC_TABLES + ".UnclaimedPurchases";
	static public final String		DISPUTE_TABLE 					= FBID.APP_STATIC_TABLES + ".DisputeLog";
	static public final String		SUPPORT_TABLE 					= FBID.APP_STATIC_TABLES + ".Support";
	static public final String		BACKUP_PLAYER_DATA_TABLE 		= FBID.APP_STATIC_TABLES + ".PlayerDataBackup";
	static public final String		CHALLENGE_TABLE					= FBID.APP_STATIC_TABLES + ".Challenges";
	static public final String		CHALLENGE_MAP_TABLE				= FBID.APP_STATIC_TABLES + ".ChallengeMap";
	static public final String		COMPLETED_CHALLENGE_TABLE		= FBID.APP_STATIC_TABLES + ".CompletedChallenges";

	/*
	static public final String		USER_DATA_TABLE_HASHKEY 	= "fbID";
	static public final String		PLAYER_DATA_TABLE_HASHKEY 	= "userID";
	static public final String		LOGIN_TABLE_HASHKEY 		= "userID";
	static public final String		REQUEST_TABLE_HASHKEY 		= "userID";
	
	static public final String		TRANSACTION_TABLE_HASHKEY 	= "userID";
	static public final String		TRANSACTION_TABLE_RANGEKEY 	= "time";-number
	
	static public final String		TRANSACTIONS_PROCESSED_TABLE_HASHKEY 	= "userID";
	static public final String		TRANSACTIONS_PROCESSED_TABLE_RANGEKEY 	= "txnID";
	
	static public final String		PURCHASE_TABLE_HASHKEY 		= "userID";
	static public final String		PURCHASE_TABLE_RANGEKEY		= "purchaseID";
	
	static public final String		UNCLAIMED_PURCHASE_TABLE_HASHKEY	= "userID";
	static public final String		UNCLAIMED_PURCHASE_TABLE_RANGEKEY	= "purchaseID";
	
	static public final String		DISPUTE_TABLE_HASHKEY		= "purchaseID";
	static public final String		DISPUTE_TABLE_RANGEKEY		= "disputeTime";-number
	
	static public final String		SUPPORT_TABLE_HASHKEY		= "userID";
	
	static public final String		BACKUP_PLAYER_DATA_TABLE_HASHKEY 	= "userID";
	static public final String		BACKUP_PLAYER_DATA_TABLE_RANGEKEY 	= "time";-number
	
	*/
	
	private static final Logger  				sLogger = Logger.getLogger(DKMDynamoDB.class);

    public static void Initialize()
    {
    	DynamoDBManager.Initialize();
    }

	//
	////////////////////////////////////////////////////////////////////
	//
	
	
	static public UserData	LoadUserData( String fbUserID ) throws Exception
	{
		return LoadUserDataFromTable( fbUserID, USER_DATA_TABLE );
	}
	
	static public void	SaveUserData( UserData userData ) throws Exception
	{
		SaveUserDataToTable( userData, USER_DATA_TABLE );
	}
	
	static public UserData	LoadUserDataFromTable( String fbUserID, String table ) throws Exception
	{
		if ( fbUserID == null || fbUserID.length() == 0 )
		{
			return null;
		}
		
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "fbID", new AttributeValue().withS(fbUserID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( table, key, true );
		if ( object == null )
		{
			return null;
		}
		
		UserData data = new UserData();
		data.setFacebookUserID(fbUserID);
		String value = object.get("fname").getS();
		data.setFirstName(value);
		value = object.get("lname").getS();
		data.setLastName(value);
		value = object.get("userID").getS();
		data.setUserId(value);
		
		AttributeValue av = object.get("crossAppID");
		data.setCrossAppID( av != null ? av.getS() : "" );
		
		return data;
	}
	
	static public void	SaveUserDataToTable( UserData userData, String table ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "fbID", new AttributeValue().withS( userData.getFacebookUserID() ) );
		item.put( "userID", new AttributeValue().withS( userData.getUserId() ) );
		item.put( "fname", new AttributeValue().withS( userData.getFirstName() ) );
		item.put( "lname", new AttributeValue().withS( userData.getLastName() ) );
		item.put( "crossAppID", new AttributeValue().withS( userData.getCrossAppID() ) );
		
		DynamoDBManager.WriteObject( table, item);
	}
	
	//
	//////////
	//
	
	static public List<UserData>	LoadUserDataByName( String lname )
	{
		return LoadUserDataByNameFromTable( lname, USER_DATA_TABLE );
	}
	
	static public List<UserData>	LoadUserDataByNameFromTable( String lname, String table )
	{
		UserDataQueryProcessor processor = new UserDataQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( lname ));
		keyConditions.put("lname", hashKeyCondition);
		DynamoDBManager.QueryObjects( table, "lname-index", keyConditions, false, processor, 5, 5, 0 );
		
		return processor.list;
	}
	
	//
	////////////////////////////////////////////////////////////////////
	//
	
	static public MahjongPlayer	LoadPlayerData( String userID ) throws Exception
	{
		return LoadPlayerDataFromTable( userID, PLAYER_DATA_TABLE );
	}
	
	static public BinaryMahjongPlayer	LoadBinaryPlayerData( String userID ) throws Exception
	{
		return LoadBinaryPlayerDataFromTable( userID, PLAYER_DATA_TABLE );
	}
	
	static public void	SavePlayerData( MahjongPlayer player ) throws Exception
	{
		SavePlayerDataToTable( player, PLAYER_DATA_TABLE );
	}

	static public void	SaveBackupBinaryPlayerData( BinaryMahjongPlayer player ) throws Exception
	{// this method should be commented out after the old table is migrated to DynamoDB
		SaveBinaryPlayerDataToTable( player, BACKUP_PLAYER_DATA_TABLE );
	}
	
	static public MahjongPlayer	LoadPlayerDataFromTable( String userID, String table ) throws Exception
	{
		BinaryMahjongPlayer data = LoadBinaryPlayerDataFromTable( userID, table );
		if ( data == null ) return null;
		MahjongPlayer player = new MahjongPlayer( data );
		return player;
	}
	
	static public BinaryMahjongPlayer	LoadBinaryPlayerDataFromTable( String userID, String table ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( table, key, true );
		if ( object == null )
		{
			return null;
		}
		
		BinaryMahjongPlayer data = new BinaryMahjongPlayer();
		data.userId = userID;
		data.sessionID = object.get("session").getS();
		data.hostUserID = object.get("fbID").getS();
		data.format = object.get("format").getS();
		data.data = object.get("data").getB();
		if ( object.get("time") != null )
		{
			data.time = Long.parseLong( object.get("time").getN() );
		}
		else
		{
			data.time = 0;
		}
		
		return data;
	}

	static public void	SavePlayerDataToTable( MahjongPlayer player, String table ) throws Exception
	{
		BinaryMahjongPlayer data = player.CreateBinaryData();
		SaveBinaryPlayerDataToTable( data, table );
	}
	
	static public void	SaveBinaryPlayerDataToTable( BinaryMahjongPlayer data, String table ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "session", new AttributeValue().withS( data.sessionID ) );
		item.put( "userID", new AttributeValue().withS( data.userId ) );
		item.put( "fbID", new AttributeValue().withS( data.hostUserID ) );
		if ( data.time != 0 )
		{
			item.put( "time", new AttributeValue().withN( Long.toString( data.time ) ) );
		}
		item.put( "format", new AttributeValue().withS( data.format ) );
		item.put( "data", new AttributeValue().withB( data.data ) );
		item.put( "dataSize", new AttributeValue().withN( Integer.toString(data.data.capacity()) ) );
		
		DynamoDBManager.WriteObject( table, item );
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public long	BackupPlayerData( String userID ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( PLAYER_DATA_TABLE, key, true );
		if ( object == null )
		{
			return 0L;
		}
		
		long			timeOfAction = Calendar.getInstance().getTime().getTime();
		object.put( "time", new AttributeValue().withN( Long.toString( timeOfAction ) ) );
		DynamoDBManager.WriteObject( DKMDynamoDB.BACKUP_PLAYER_DATA_TABLE, object );
		SaveTransaction( new Transaction( userID, TransactionType.DATA_BACKUP, timeOfAction ) );

		return timeOfAction;
	}
	
	static public boolean	RestoreBackupPlayerData( String userID, long backupTime ) throws Exception
	{
		// pull in backup data
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		key.put( "time", new AttributeValue().withN(Long.toString(backupTime) ) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( BACKUP_PLAYER_DATA_TABLE, key, true );
		if ( object == null )
		{
			return false;
		}
		
		// make a backup of current user data
		if ( BackupPlayerData( userID ) == 0L )
		{
			return false;
		}

		object.remove("time");// player table doesn't have a time field
		// write backup copy to player table
		DynamoDBManager.WriteObject( DKMDynamoDB.PLAYER_DATA_TABLE, object );
		
		long			timeOfAction = Calendar.getInstance().getTime().getTime();
		SaveTransaction( new Transaction( userID, TransactionType.DATA_RESTORE, timeOfAction ) );

		return true;
	}

	static public void	BackupAllPlayerData()
	{
		BackupPlayerDataQueryProcessor.sUsersBackedUp = 0;
		BackupPlayerDataQueryProcessor processor = new BackupPlayerDataQueryProcessor();
		DynamoDBManager.ScanObjects( PLAYER_DATA_TABLE, null, true, processor, 100, 10 );
	}
	
	static public int	UsersBackedUp()
	{
		return BackupPlayerDataQueryProcessor.sUsersBackedUp;
	}
	
	static public BinaryMahjongPlayer	LoadBinaryBackupPlayerData( String userID, long backupTime ) throws Exception
	{
		// pull in backup data
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		key.put( "time", new AttributeValue().withN(Long.toString(backupTime) ) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( BACKUP_PLAYER_DATA_TABLE, key, true );
		if ( object == null )
		{
			return null;
		}
		
		BinaryMahjongPlayer data = new BinaryMahjongPlayer();
		data.userId = userID;
		data.sessionID = object.get("session").getS();
		data.hostUserID = object.get("fbID").getS();
		data.format = object.get("format").getS();
		data.data = object.get("data").getB();
		if ( object.get("time") != null )
		{
			data.time = Long.parseLong( object.get("time").getN() );
		}
		else
		{
			data.time = 0;
		}
		
		return data;
	}
	
	static public MahjongPlayer	LoadBackupPlayerData( String userID, long backupTime ) throws Exception
	{
		BinaryMahjongPlayer data = LoadBinaryBackupPlayerData( userID, backupTime );
		MahjongPlayer player = new MahjongPlayer( data );
		return player;
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public LoginData	LoadLoginData( String userID ) throws Exception
	{
		return LoadLoginDataFromTable( userID, LOGIN_TABLE );
	}
	
	static public void	SaveLoginData( LoginData data ) throws Exception
	{
		SaveLoginDataToTable( data, LOGIN_TABLE );
	}
	
	static public LoginData	LoadLoginDataFromTable( String userID, String table ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( table, key, true );
		if ( object == null )
		{
			return null;
		}
		
		LoginData data = new LoginData( userID, null, false );
		data.setDailySessionID( object.get("dailySessionID").getS() );
		data.setLoginCount( Integer.parseInt( object.get("logins").getN() ) );
		data.setConsecutiveLoginCount( Integer.parseInt( object.get("consecutiveLogins").getN() ) );
		data.setFirstTimestamp( Long.parseLong( object.get("firstTime").getN() ) );
		data.setLastTimestamp( Long.parseLong( object.get("lastTime").getN() ) );
		data.setDailyTimestamp( Long.parseLong( object.get("dailyTime").getN() ) );
		
		return data;
	}
	
	static public void	SaveLoginDataToTable( LoginData data, String table ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getUserID() ) );
		item.put( "dailySessionID", new AttributeValue().withS( data.getDailySessionID() ) );
		item.put( "logins", new AttributeValue().withN( Integer.toString( data.getLoginCount() ) ) );
		item.put( "consecutiveLogins", new AttributeValue().withN( Integer.toString( data.getConsecutiveLoginCount() ) ) );
		item.put( "firstTime", new AttributeValue().withN( Long.toString( data.getFirstTimestamp() ) ) );
		item.put( "lastTime", new AttributeValue().withN( Long.toString( data.getLastTimestamp() ) ) );
		item.put( "dailyTime", new AttributeValue().withN( Long.toString( data.getDailyTimestamp() ) ) );
		
		DynamoDBManager.WriteObject( table, item);
	}
	
	//
	////////////////////////////////////////////////////////////////////
	//
	
	static public List<RequestLog2>	LoadRequestEligibility( String userID ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( REQUEST_TABLE, key, true );
		if ( object == null )
		{
			// return empty list...
			List<RequestLog2> logs = new ArrayList<RequestLog2>();
			return logs;
		}
		
		BinaryRequestLog data = new BinaryRequestLog();
		data.userId = userID;
		data.format = object.get("format").getS();
		data.data = object.get("data").getB();
		
		List<RequestLog2> logs = data.createLogs();
		
		return logs;
	}
	
	static public void	SaveRequestEligibility( String userID, List<RequestLog2> logs ) throws Exception
	{
		BinaryRequestLog data = BinaryRequestLog.createFromLogs( userID, logs );
		
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.userId ) );
		item.put( "format", new AttributeValue().withS( data.format ) );
		item.put( "data", new AttributeValue().withB( data.data ) );
		item.put( "dataSize", new AttributeValue().withN( Integer.toString(data.data.capacity()) ) );
		
		DynamoDBManager.WriteObject( REQUEST_TABLE, item);
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public List<PurchaseData>	LoadAllUnclaimedPurchaseData( String userID )
	{
		PurchaseDataQueryProcessor processor = new PurchaseDataQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( userID ));
		keyConditions.put("userID", hashKeyCondition);
		DynamoDBManager.QueryObjects( UNCLAIMED_PURCHASE_TABLE, null, keyConditions, true, processor, 5, 5, 0 );
		
		return processor.list;
	}
	
	static public void	ReleaseUnclaimedPurchaseData( List<PurchaseData> purchases ) throws Exception
	{
		for ( int i = 0; i < purchases.size(); i++ )
		{
			PurchaseData purchaseLog = purchases.get(i);
			Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
			key.put( "userID", new AttributeValue().withS(purchaseLog.getHostUserID()) );
			key.put( "purchaseID", new AttributeValue().withS(purchaseLog.getHostPurchaseID()) );
			DynamoDBManager.DeleteObject( UNCLAIMED_PURCHASE_TABLE, key );
		}
	}
	
	static public List<PurchaseData>	LoadAllPurchaseData( String userID )
	{
		return LoadAllPurchaseDataFromTable( userID, PURCHASE_TABLE );
	}
	static public PurchaseData	LoadPurchaseData( String userID, String purchaseID ) throws Exception
	{
		return LoadPurchaseDataFromTable( userID, purchaseID, PURCHASE_TABLE );
	}
	static public void	SavePurchaseData( PurchaseData data ) throws Exception
	{
		SavePurchaseDataToTable( data, PURCHASE_TABLE, true );
	}
	
	static public List<PurchaseData>	LoadAllPurchaseDataFromTable( String userID, String table )
	{
		PurchaseDataQueryProcessor processor = new PurchaseDataQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( userID ));
		keyConditions.put("userID", hashKeyCondition);
		DynamoDBManager.QueryObjects( table, null, keyConditions, true, processor, 6, 3, 0 );
		
		return processor.list;
	}

	static public PurchaseData	LoadPurchaseDataFromTable( String userID, String purchaseID, String table ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		key.put( "purchaseID", new AttributeValue().withS(purchaseID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( table, key, true );
		if ( object == null )
		{
			return null;
		}
		
		PurchaseData data = new PurchaseData();
		data.setHostUserID( object.get("userID").getS() );
		data.setHostPurchaseID( object.get("purchaseID").getS() );
		data.setSku( object.get("sku").getS() );
		data.setChargeAction( object.get("chargeAction").getS() );
		data.setChargeStatus( object.get("chargeStatus").getS() );
		data.setCurrency( object.get("currency").getS() );
		data.setAmount( Float.parseFloat(object.get("amount").getN()) );
		data.setExchangeRate( Float.parseFloat(object.get("exchangeRate").getN()) );
		data.setQuantity( Integer.parseInt(object.get("quantity").getN()) );
		data.setCoins( Integer.parseInt(object.get("coins").getN()) );
		data.setClaimStatus( Integer.parseInt(object.get("claimStatus").getN()) );
		data.setTest( Integer.parseInt(object.get("test").getN()) );
		data.setPurchaseTimestamp( Long.parseLong( object.get("purchaseTime").getN() ) );
		data.setClaimedTimestamp( Long.parseLong( object.get("claimTime").getN() ) );
		
		return data;
	}
	
	static public void	SavePurchaseDataToTable( PurchaseData data, String table, boolean placeUnclaimedInTable ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getHostUserID() ) );
		item.put( "purchaseID", new AttributeValue().withS( data.getHostPurchaseID() ) );
		item.put( "sku", new AttributeValue().withS( data.getSku() ) );
		item.put( "chargeAction", new AttributeValue().withS( data.getChargeAction() ) );
		item.put( "chargeStatus", new AttributeValue().withS( data.getChargeStatus() ) );
		item.put( "currency", new AttributeValue().withS( data.getCurrency() ) );
		item.put( "amount", new AttributeValue().withN( Float.toString( data.getAmount() ) ) );
		item.put( "exchangeRate", new AttributeValue().withN( Float.toString( data.getExchangeRate() ) ) );
		item.put( "quantity", new AttributeValue().withN( Integer.toString( data.getQuantity() ) ) );
		item.put( "coins", new AttributeValue().withN( Integer.toString( data.getCoins() ) ) );
		item.put( "claimStatus", new AttributeValue().withN( Integer.toString( data.getClaimStatus() ) ) );
		item.put( "test", new AttributeValue().withN( Integer.toString( data.getTest() ) ) );
		item.put( "purchaseTime", new AttributeValue().withN( Long.toString( data.getPurchaseTimestamp() ) ) );
		item.put( "claimTime", new AttributeValue().withN( Long.toString( data.getClaimedTimestamp() ) ) );
		
		DynamoDBManager.WriteObject( table, item );
		
		if ( data.getClaimStatus() == PurchaseData.UNCLAIMED_STATUS && placeUnclaimedInTable )
		{
			DynamoDBManager.WriteObject( UNCLAIMED_PURCHASE_TABLE, item );
		}
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public List<DisputeData>	LoadAllDisputeData()
	{
		DisputeDataQueryProcessor processor = new DisputeDataQueryProcessor();
		DynamoDBManager.ScanObjects( DISPUTE_TABLE, null, true, processor, 5, 5 );
		return processor.list;
	}
	
	static public List<DisputeData>	LoadAllNewDisputeData()
	{
		DisputeDataQueryProcessor processor = new DisputeDataQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition statusCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withN( "1" ));
		keyConditions.put("status", statusCondition);
		DynamoDBManager.QueryObjects( DISPUTE_TABLE, "status-purchaseID-index", keyConditions, false, processor, 5, 5, 0 );
		
		return processor.list;
	}

	static public List<DisputeData>	LoadAllDisputeData( String purchaseID )
	{
		DisputeDataQueryProcessor processor = new DisputeDataQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( purchaseID ));
		keyConditions.put("purchaseID", hashKeyCondition);
		DynamoDBManager.QueryObjects( DISPUTE_TABLE, null, keyConditions, true, processor, 5, 5, 0 );
		
		return processor.list;
	}

	static public DisputeData	LoadDisputeData( String purchaseID, long disputeTime ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "purchaseID", new AttributeValue().withS(purchaseID) );
		key.put( "disputeTime", new AttributeValue().withN( Long.toString(disputeTime) ) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( DISPUTE_TABLE, key, true );
		if ( object == null )
		{
			return null;
		}
		
		DisputeData data = new DisputeData();
		data.setHostUserID( object.get("userID").getS() );
		data.setHostPurchaseID( object.get("purchaseID").getS() );
		data.setEmail( object.get("email").getS() );
		data.setUserComment( object.get("userComment").getS() );
		data.setSupportComment( object.get("supportComment").getS() );
		data.setStatus( Integer.parseInt(object.get("status").getN()) );
		data.setHostTimeCreated( object.get("hostTimeCreated").getS() );
		data.setDisputeTimestamp( Long.parseLong( object.get("disputeTime").getN() ) );
		data.setUpdatedTimestamp( Long.parseLong( object.get("updatedTime").getN() ) );
		data.setTest( Integer.parseInt(object.get("test").getN()) );
		
		return data;
	}
	
	static public void	SaveDisputeData( DisputeData data ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "purchaseID", new AttributeValue().withS( data.getHostPurchaseID() ) );
		item.put( "disputeTime", new AttributeValue().withN( Long.toString( data.getDisputeTimestamp() ) ) );
		item.put( "userID", new AttributeValue().withS( data.getHostUserID() ) );
		item.put( "email", new AttributeValue().withS( data.getEmail() ) );
		item.put( "userComment", new AttributeValue().withS( data.getUserComment() ) );
		item.put( "supportComment", new AttributeValue().withS( data.getSupportComment() ) );
		item.put( "status", new AttributeValue().withN( Integer.toString( data.getStatus() ) ) );
		item.put( "hostTimeCreated", new AttributeValue().withS( data.getHostTimeCreated() ) );
		item.put( "updatedTime", new AttributeValue().withN( Long.toString( data.getUpdatedTimestamp() ) ) );
		item.put( "test", new AttributeValue().withN( Integer.toString( data.getTest() ) ) );

		DynamoDBManager.WriteObject( DISPUTE_TABLE, item);
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public void SaveChallenge(Challenge data, String table)
	{
		String allUserIDs = data.userIds.get(0);
		
		for(int i = 1; i < data.userIds.size(); i++)
		{
			allUserIDs += "," + data.userIds.get(i);
		}
		
		String allScores = data.scores.get(0).toString();
		
		for(int i = 1; i < data.userIds.size(); i++)
		{
			allScores += "," + data.scores.get(i).toString();
		}
		
		if( data.challengeID == null)
		{
			UUID id = UUID.randomUUID();
			data.challengeID = id.toString();
		}
		
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put("challengeID", new AttributeValue().withS( data.challengeID ));
		item.put("userIDs", new AttributeValue().withS(allUserIDs));
		item.put("scores", new AttributeValue().withS(allScores));
		item.put("type", new AttributeValue().withS( data.type ));
		item.put("status", new AttributeValue().withS( data.status ));
		item.put("timeoutTime", new AttributeValue().withN( Long.toString(data.timeoutTime) ));
		item.put("resultsViewed", new AttributeValue().withN( Integer.toString(data.resultsViewed) ));
		item.put("level", new AttributeValue().withN( Integer.toString(data.level) ));
		item.put("powerLevel", new AttributeValue().withN( Integer.toString(data.powerLevel) ));
		
		try
		{
			DynamoDBManager.WriteObject( table, item);
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to SaveChallenge!",ex);
		}
	}
	
	static public Challenge LoadChallenge(String challengeID) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "purchaseID", new AttributeValue().withS(challengeID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( CHALLENGE_TABLE, key, true );
		if ( object == null )
		{
			return null;
		}
		
		Challenge data = new Challenge();
		data.challengeID = challengeID;
		data.userIds = Arrays.asList(object.get("userIDs").getS().split(","));
		List<String> scoreStrings = Arrays.asList(object.get("scores").getS().split(","));
		for(int i = 0; i < scoreStrings.size(); i++)
		{
			data.scores.add(Integer.parseInt(scoreStrings.get(i)));
		}
		data.type = object.get("type").getS();
		data.status = object.get("status").getS();
		data.timeoutTime = Long.parseLong(object.get("timeoutTime").getN());
		data.resultsViewed = Integer.parseInt(object.get("resultsViewed").getN());
		data.level = Integer.parseInt(object.get("level").getN());
		data.powerLevel = Integer.parseInt(object.get("powerLevel").getN());
		
		if(Calendar.getInstance().getTime().getTime() > data.timeoutTime)
		{
			data.status = Challenge.COMPLETE;
		}
		
		return data;
	}
	
	static public List<Challenge> ChallengesForUser(String userID)
	{
		UserChallengeProcessor processor = new UserChallengeProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( userID ));
		keyConditions.put("userID", hashKeyCondition);
		DynamoDBManager.QueryObjects( CHALLENGE_MAP_TABLE, null, keyConditions, true, processor, 5, 5, 0 );
		
		List<Challenge> challenges = processor.list;
		
		return challenges;
	}
	
	static public void AddChallengeToMapTable(Challenge challenge)
	{
		for(int i = 0; i < challenge.userIds.size(); i++)
		{
			try
			{
				Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
				item.put("userID", new AttributeValue().withS(challenge.userIds.get(0)));
				item.put("challengeID", new AttributeValue().withS( challenge.challengeID ));
				
				DynamoDBManager.WriteObject( CHALLENGE_MAP_TABLE, item);
			}
			catch( Exception ex )
			{
				sLogger.error("Failed to AddChallengeToMapTable!",ex);
			}
			
		}
	}
	
	static public void MoveChallengeToCompleteTable(Challenge challenge)
	{
		if(challenge.status != Challenge.COMPLETE) return;
		
		if(challenge.resultsViewed == Math.pow(2,challenge.userIds.size())-1)
		{
			Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
			key.put( "challengeID", new AttributeValue().withS(challenge.challengeID) );
			try
			{
				DynamoDBManager.DeleteObject( CHALLENGE_TABLE, key );
				SaveChallenge(challenge,COMPLETED_CHALLENGE_TABLE);
			}
			catch( Exception ex )
			{
				sLogger.error("Failed to MoveChallengeToCompleteTable!",ex);
			}
		}
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public List<Transaction>	LoadTransactionsForTimeRange( String userID, long startTime, long endTime )
	{
		TransactionQueryProcessor processor = new TransactionQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( userID ));
		
		Condition rangeKeyCondition = new Condition()
        .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
        .withAttributeValueList(new AttributeValue().withN(Long.toString( startTime )), 
                                new AttributeValue().withN(Long.toString( endTime )));		
		
		keyConditions.put("userID", hashKeyCondition);
		keyConditions.put("time", rangeKeyCondition);
		DynamoDBManager.QueryObjects( TRANSACTION_TABLE, null, keyConditions, true, processor, 20, 10, 0 );
		
		return processor.list;
	}

	static public List<Transaction>	LoadTransactions( String userID )
	{
		return LoadTransactionsFromTable( userID, TRANSACTION_TABLE );
	}
	
	static public void	SaveTransaction( Transaction data )
	{
		try
		{
			SaveTransactionToTable( data, TRANSACTION_TABLE );
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to SaveTransactionData!",ex);
		}
	}
	
	static public int	TransactionHasBeenSaved( Transaction transaction ) throws Exception
	{
		// query the processed txn table to determine if the transaction has been saved already...
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS( transaction.getPlayerID() ) );
		key.put( "txnID", new AttributeValue().withS( transaction.getID() ) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( TRANSACTIONS_PROCESSED_TABLE, key, true );
		if ( object == null )
		{
			return TransactionResult.NONE;
		}
		
		int previousResult = Integer.parseInt( object.get("result").getN() );
		
		return previousResult;
	}
	
	static public List<Transaction>	LoadTransactionsFromTable( String userID, String table )
	{
		TransactionQueryProcessor processor = new TransactionQueryProcessor();
		Map<String, Condition> keyConditions = new HashMap<String, Condition>();
		Condition hashKeyCondition = new Condition()
			.withComparisonOperator(ComparisonOperator.EQ)
			.withAttributeValueList(new AttributeValue().withS( userID ));
		keyConditions.put("userID", hashKeyCondition);
		DynamoDBManager.QueryObjects( table, null, keyConditions, true, processor, 20, 10, 0 );
		
		return processor.list;
	}
	
	static public void	SaveTransactionToTable( Transaction data, String table ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getPlayerID() ) );
		item.put( "txnID", new AttributeValue().withS( data.getID() ) );
		item.put( "time", new AttributeValue().withN( Long.toString( data.getTimestamp() ) ) );
		item.put( "result", new AttributeValue().withN( Integer.toString( data.getResultCode() ) ) );

		// first write this transaction to the processed table, so any more attempts to apply this txn will be ignored
		// BTW, we could use a global secondary index, but that would keep us from moving this write from this method 
		// if we decide to move it earlier in the transaction processing
		DynamoDBManager.WriteObject( TRANSACTIONS_PROCESSED_TABLE, item );

		// now add the data fields we want to store with the full transaction
		item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getPlayerID() ) );
		item.put( "txnID", new AttributeValue().withS( data.getID() ) );
		item.put( "time", new AttributeValue().withN( Long.toString( data.getTimestamp() ) ) );
		item.put( "result", new AttributeValue().withN( Integer.toString( data.getResultCode() ) ) );
		item.put( "action", new AttributeValue().withS( data.getAction() ) );
		item.put( "marketID", new AttributeValue().withN( Integer.toString( data.getMarketID() ) ) );
		
		if ( data.getData().size() > 0 )
		{
			String dataString = data.getData().get(0).toString();
			for ( int i = 1; i < data.getData().size(); ++i )
			{
				dataString += "," + data.getData().get(i).toString();
			}
			item.put( "items", new AttributeValue().withS( dataString ) );
		}
		
		DynamoDBManager.WriteObject( table, item );
	}

	//
	//////////////////////////////////////////////////////////////////////
	//

	//
	//////////////////////////////////////////////////////////////////////
	//
	
	
	static public SupportUserData	LoadSupportUserData( String userID ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( SUPPORT_TABLE, key, true );
		if ( object == null )
		{
			return null;
		}
		
		SupportUserData data = new SupportUserData();
		data.setUserID( object.get("userID").getS() );
		data.setLoginKey( object.get("loginKey").getS() );
		data.setAuthority( Integer.parseInt( object.get("authority").getN() ) );

		return data;
	}
	
	static public void	SaveSupportUserData( SupportUserData data ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getUserID() ) );
		item.put( "loginKey", new AttributeValue().withS( data.getLoginKey() ) );
		item.put( "authority", new AttributeValue().withN( Integer.toString( data.getAuthority() ) ) );
		
		DynamoDBManager.WriteObject( SUPPORT_TABLE, item);
	}
	
	//
	//////////////////////////////////////////////////////////////////////
	//
	
	static public ServiceTimeData	LoadServiceTimeData( String userID ) throws Exception
	{
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put( "userID", new AttributeValue().withS(userID) );
		Map<String, AttributeValue> object = DynamoDBManager.ReadObject( SUPPORT_TABLE, key, true );
		if ( object == null )
		{
			return null;
		}
		
		ServiceTimeData data = new ServiceTimeData();
		data.setId( object.get("userID").getS() );
		data.setName( object.get("name").getS() );
		data.setActive( Integer.parseInt( object.get("active").getN() ) );
		data.setTime( Long.parseLong( object.get("time").getN() ) );
		data.setDuration( Integer.parseInt( object.get("duration").getN() ) );

		return data;
	}
	
	static public void	SaveServiceTimeData( ServiceTimeData data ) throws Exception
	{
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put( "userID", new AttributeValue().withS( data.getId() ) );
		item.put( "name", new AttributeValue().withS( data.getName() ) );
		item.put( "active", new AttributeValue().withN( Integer.toString( data.getActive() ) ) );
		item.put( "time", new AttributeValue().withN( Long.toString( data.getTime() ) ) );
		item.put( "duration", new AttributeValue().withN( Integer.toString( data.getDuration() ) ) );
		
		DynamoDBManager.WriteObject( SUPPORT_TABLE, item);
	}

	//
	//////////////////////////////////////////////////////////////////////
	//
	

}
