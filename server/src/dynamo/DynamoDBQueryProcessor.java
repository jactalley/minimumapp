package dynamo;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public interface DynamoDBQueryProcessor 
{
	public void	processItem( Map<String, AttributeValue> item );
}
