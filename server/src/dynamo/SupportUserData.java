package dynamo;

import java.util.Calendar;

import util.Encrypt;

public class SupportUserData 
{
	public static final String	DEFAULT_ADMIN_ID = "dkadmin#01";
	public static final int		ADMIN_AUTHORITY = 0;
	public static final int		SUPPORT_AUTHORITY = 1;

	private String userID;
	private String loginKey;
	private int authority;
	private String sessionKey;
	private long activityTime;
	
	public void	CreateUser( String user, String password, int auth )
	{
		userID = user;
		authority = auth;
		loginKey = Encrypt.CryptWithMD5( password );
		activityTime = Calendar.getInstance().getTime().getTime();
	}
	
	public void	CreateDefaultAdmin()
	{
		String	DEFAULT_ADMIN_PWD = "5upp0rtm3";
		CreateUser( DEFAULT_ADMIN_ID, DEFAULT_ADMIN_PWD, ADMIN_AUTHORITY );
	}
	
	public void UpdateActivityTime()
	{
		activityTime = Calendar.getInstance().getTime().getTime();
	}
	
	public void CreateSessionKey()
	{
		sessionKey = userID + Long.toHexString(Calendar.getInstance().getTime().getTime());
	}
	
	public boolean CheckPassword( String password )
	{
		return loginKey.equals( Encrypt.CryptWithMD5( password ) );
	}
	
	public void SetPassword( String password )
	{
		loginKey = Encrypt.CryptWithMD5( password );
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getLoginKey() {
		return loginKey;
	}

	public void setLoginKey(String loginKey) {
		this.loginKey = loginKey;
	}

	public int getAuthority() {
		return authority;
	}

	public void setAuthority(int authority) {
		this.authority = authority;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public long getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(long activityTime) {
		this.activityTime = activityTime;
	}

}
