package dynamo;

import services.DisputeLog;

public class DisputeData 
{
	public static final int		UNKNOWN_STATUS = 0;
	public static final int		NEW_STATUS = 1;
	public static final int		REFUNDED_STATUS = 2;
	public static final int		REPLACED_STATUS = 3;
	public static final int		DENIED_STATUS = 4;
	public static final int		BANNED_STATUS = 5;
	
	public DisputeData(){}
	public DisputeData( DisputeLog oldLog )
	{
		this.hostUserID 		= oldLog.getHostUserID();	
		this.hostPurchaseID 	= oldLog.getHostPurchaseID();	
		this.email 				= oldLog.getEmail();
		this.userComment 		= oldLog.getUserComment();
		this.supportComment 	= oldLog.getSupportComment();
		this.status 			= oldLog.getStatus();
		this.hostTimeCreated 	= oldLog.getHostTimeCreated();	
		this.disputeTimestamp 	= oldLog.getDisputeTimestamp().getTime();
		this.updatedTimestamp 	= oldLog.getUpdatedTimestamp().getTime();
		this.test 				= oldLog.getTest();
	}
	
	private String hostUserID;	
	private String hostPurchaseID;	
	private String email;
	private String userComment;
	private String supportComment;
	private int status;
	private String hostTimeCreated;	
	private long disputeTimestamp;
	private long updatedTimestamp;
	private int test;
	
	public String getHostUserID() {
		return hostUserID;
	}
	public void setHostUserID(String hostUserID) {
		this.hostUserID = hostUserID;
	}
	public String getHostPurchaseID() {
		return hostPurchaseID;
	}
	public void setHostPurchaseID(String hostPurchaseID) {
		this.hostPurchaseID = hostPurchaseID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserComment() {
		return userComment;
	}
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}
	public String getSupportComment() {
		return supportComment;
	}
	public void setSupportComment(String supportComment) {
		this.supportComment = supportComment;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getHostTimeCreated() {
		return hostTimeCreated;
	}
	public void setHostTimeCreated(String hostTimeCreated) {
		this.hostTimeCreated = hostTimeCreated;
	}
	public long getDisputeTimestamp() {
		return disputeTimestamp;
	}
	public void setDisputeTimestamp(long disputeTimestamp) {
		this.disputeTimestamp = disputeTimestamp;
	}
	public long getUpdatedTimestamp() {
		return updatedTimestamp;
	}
	public void setUpdatedTimestamp(long updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}
	public int getTest() {
		return test;
	}
	public void setTest(int test) {
		this.test = test;
	}
}
