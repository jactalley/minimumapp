package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class PurchaseDataQueryProcessor implements DynamoDBQueryProcessor
{
	public List<PurchaseData> list = new ArrayList<PurchaseData>();

	public void	processItem( Map<String, AttributeValue> object )
	{
		PurchaseData data = new PurchaseData();
		data.setHostUserID( object.get("userID").getS() );
		data.setHostPurchaseID( object.get("purchaseID").getS() );
		data.setSku( object.get("sku").getS() );
		data.setChargeAction( object.get("chargeAction").getS() );
		data.setChargeStatus( object.get("chargeStatus").getS() );
		data.setCurrency( object.get("currency").getS() );
		data.setAmount( Float.parseFloat(object.get("amount").getN()) );
		data.setExchangeRate( Float.parseFloat(object.get("exchangeRate").getN()) );
		data.setQuantity( Integer.parseInt(object.get("quantity").getN()) );
		data.setCoins( Integer.parseInt(object.get("coins").getN()) );
		data.setClaimStatus( Integer.parseInt(object.get("claimStatus").getN()) );
		data.setTest( Integer.parseInt(object.get("test").getN()) );
		data.setPurchaseTimestamp( Long.parseLong( object.get("purchaseTime").getN() ) );
		data.setClaimedTimestamp( Long.parseLong( object.get("claimTime").getN() ) );
		list.add( data );
	}

}
