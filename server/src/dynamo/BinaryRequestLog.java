package dynamo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

public class BinaryRequestLog 
{
	public static final short		MAJOR_VERSION = 1;
	public static final short		MINOR_VERSION = 1;
	
	public static final String		UNCOMPRESSED_FORMAT = "uncompressed";
	public static final String		COMPRESSED_FORMAT = "compressed";
	
	public String 		userId;
	public String 		format;
	public ByteBuffer	data;
	
	private static final Logger  				sLogger = Logger.getLogger(BinaryRequestLog.class);
	
	static public BinaryRequestLog	createFromLogs( String userID, List<RequestLog2> logs )
	{
		try
		{
			BinaryRequestLog binaryData = new BinaryRequestLog();
			binaryData.userId = userID;
			binaryData.format = UNCOMPRESSED_FORMAT;
			
			Collections.sort( logs );// sort logs by time stamp
			
			ByteArrayOutputStream outArray = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream( outArray );
			
			out.writeShort(BinaryMahjongPlayer.MAJOR_VERSION);
			out.writeShort(BinaryMahjongPlayer.MINOR_VERSION);
			
			out.writeUTF( userID );
			out.writeInt( logs.size() );
			
			long currentTimeStamp = 0;
			for ( int i = 0; i < logs.size(); i++ )
			{
				RequestLog2 log = logs.get(i);
				if ( currentTimeStamp != log.getRequestTimestamp() )
				{
					currentTimeStamp = log.getRequestTimestamp();
					out.writeLong( 0 );// group separation marker
					out.writeLong( currentTimeStamp );// save group time stamp
				}
				long friendID = Long.parseLong( log.getFriendID() );
				out.writeLong( friendID );
			}
			
			binaryData.data = ByteBuffer.wrap( outArray.toByteArray() );
			
			return binaryData;			
		}
		catch( Exception exception ) 
	    {
	    	sLogger.error("Failed to create binary data for RequestLog!",exception);
	    	exception.printStackTrace();
	    }
		
		return null;
	}
	
	public List<RequestLog2> createLogs()
	{
		List<RequestLog2> logs = new ArrayList<RequestLog2>();
		
		try
		{
			if ( !format.equals( UNCOMPRESSED_FORMAT ) )
			{
				return logs;
			}
			
			ByteBuffer dataBuffer = data;
			ByteArrayInputStream inArray = new ByteArrayInputStream(dataBuffer.array());
			DataInputStream in = new DataInputStream( inArray );
						
			@SuppressWarnings("unused")
			int majorVersion = in.readUnsignedShort();
			@SuppressWarnings("unused")
			int minorVersion = in.readUnsignedShort();
			
			@SuppressWarnings("unused")
			String userID = in.readUTF();

			int count = in.readInt();
			
			// get current Time
			long currentTimestamp = Calendar.getInstance().getTime().getTime();
			long minEligibleTime = 18*60*60;// 18 hours - in seconds
			long groupTimeStamp = 0;
			int	 groupEligible = 0;
			for ( int i = 0; i < count; i++ )
			{
				long friendID = in.readLong();
				if ( friendID == 0 )// check to see if this is the beginning of a same time-stamp group of friends
				{
					groupTimeStamp = in.readLong();
					long	seconds = ( currentTimestamp - groupTimeStamp )/1000;
					if ( seconds < minEligibleTime )
					{
						// not eligible, set RequestEligible value to seconds until available (client could do something with that)
						groupEligible = (int)( minEligibleTime - seconds );
					}
					else
					{
						// eligible, set value to 0 to mark it so
						groupEligible = 0;
					}
					
					friendID = in.readLong();
				}

				String friendIDString = Long.toString( friendID );
				
				RequestLog2 log = new RequestLog2();
				log.setFriendID( friendIDString );
				log.setRequestTimestamp( groupTimeStamp );
				log.setRequestEligible( groupEligible );
				logs.add( log );
			}
		}
		catch( Exception exception ) 
        {
        	sLogger.error("Failed to create RequestLog List from binary data!",exception);
        	exception.printStackTrace();
        }
		
		return logs;
	}

}
