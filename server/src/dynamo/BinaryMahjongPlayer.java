package dynamo;

import java.nio.ByteBuffer;

public class BinaryMahjongPlayer 
{
	public static final short		MAJOR_VERSION = 1;
	public static final short		MINOR_VERSION = 2;
	
	public static final String		UNCOMPRESSED_FORMAT = "uncompressed";
	public static final String		COMPRESSED_FORMAT = "compressed";
	
	public String		sessionID;
	public String		hostUserID;
	public String 		userId;
	public String 		format;
	public long 		time;
	public ByteBuffer	data;
}
