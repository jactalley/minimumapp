package dynamo;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.DeleteItemResult;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateItemResult;
//import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;

public class DynamoDBManager 
{
	private static final Logger  				sLogger = Logger.getLogger(DynamoDBManager.class);
	
    public static AmazonDynamoDBClient			sDynamoDB = null;
    
    public static void Initialize() 
    {
    	if ( sDynamoDB != null )
    	{
    		return;
    	}
    	
        try 
        {
        	InputStream is = DynamoDBManager.class.getResourceAsStream("AwsCredentials.properties");
        	PropertiesCredentials pc =  new PropertiesCredentials( is );
        	sDynamoDB = new AmazonDynamoDBClient(pc);
            Region region = Region.getRegion(Regions.US_EAST_1);
            sDynamoDB.setRegion(region);
        } 
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to init DynamoDB",exception);
        	exception.printStackTrace();
        }
    }
    
    public static Map<String, AttributeValue> ReadObject( String tableName, Map<String, AttributeValue> key, Boolean consistentRead ) throws Exception
    {
        try
        {
        	GetItemResult getItemResult = sDynamoDB.getItem( tableName, key, consistentRead );
        	return getItemResult.getItem();
        }
        catch ( ResourceNotFoundException notFound )
        {
        	return null;
        }
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
			throw( ase );
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
			throw( ace );
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to read object from DynamoDB",exception);
        	exception.printStackTrace();
        	throw(exception);
        }
    }
    
    public static void WriteObject( String tableName, Map<String, AttributeValue> item ) throws Exception
    {
    	try
    	{
    		@SuppressWarnings("unused")
			PutItemResult putItemResult = sDynamoDB.putItem( tableName, item );
    	}
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
	       	throw(ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
	       	throw(ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to write object to DynamoDB",exception);
        	exception.printStackTrace();
           	throw(exception);
        }
	}
    
    public static UpdateItemResult UpdateObject( String tableName, Map<String, AttributeValue> keys, Map<String, AttributeValueUpdate> attributeUpdates, Map<String,ExpectedAttributeValue> expectedValues, ReturnValue returnValues ) throws Exception
    {
    	try
    	{
    		UpdateItemRequest request = new UpdateItemRequest() 
    			.withTableName(tableName)
    			.withKey(keys)
    			.withAttributeUpdates(attributeUpdates)
    			.withReturnValues(returnValues);
    		if ( expectedValues != null )
    		{
    			request.setExpected(expectedValues);
    		}
			UpdateItemResult updateItemResult = sDynamoDB.updateItem( request );
    		return updateItemResult;
    	}
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
	       	throw(ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
	       	throw(ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to update object to DynamoDB",exception);
        	exception.printStackTrace();
           	throw(exception);
        }
    }
    
	public static boolean DeleteObject( String tableName, Map<String, AttributeValue> key ) throws Exception
	{
		try
		{
			@SuppressWarnings("unused")
			DeleteItemResult deleteItemResult = sDynamoDB.deleteItem( tableName, key );
			return true;
    	}
        catch ( ResourceNotFoundException notFound )
        {
        	return true;
        }
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
	       	throw(ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
	       	throw(ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to delete object from DynamoDB",exception);
        	exception.printStackTrace();
           	throw(exception);
        }
	}
    
	private static long	WaitBetweenOperations( long lastTime, long minTimeBetweenOperations ) throws InterruptedException
	{
		long timestamp = Calendar.getInstance().getTime().getTime();
		long sleepTime = minTimeBetweenOperations - (timestamp - lastTime);
		if ( sleepTime > 0 ) Thread.sleep(sleepTime);
		timestamp = Calendar.getInstance().getTime().getTime();
		return timestamp;
	}

	public static boolean QueryObjects( String tableName, String indexName, Map<String, Condition> keyConditions, Boolean consistentRead, DynamoDBQueryProcessor itemProcessor, int maxItemsPerSecond, int batchSize, int maxResults )
	{
		try
		{
			if ( batchSize <= 0 )
        	{
        		batchSize = 15;
        	}
			
			if ( maxResults > 0 && batchSize > maxResults )
			{
				batchSize = maxResults;
			}
        	
        	if ( maxItemsPerSecond <= 0 )
        	{
        		maxItemsPerSecond = -1;
        	}
    		
    		if ( indexName != null )
    		{// consistent read is not supported with secondary indices
    			consistentRead = false;
    		}
       	
        	long minTimeBetweenOperations = 1000 * batchSize / maxItemsPerSecond;
        	long timeToken = 0;
        	
        	int resultsToGO = maxResults;
        	
        	Map<String, AttributeValue> lastEvaluatedKey = null;
        	
        	do
        	{
        		if ( maxItemsPerSecond > 0 )
        		{// only pace our queries if requested to do so
        			timeToken = WaitBetweenOperations( timeToken, minTimeBetweenOperations );
        		}
        		
        		QueryRequest queryRequest = new QueryRequest()
	                .withTableName( tableName )
	                .withKeyConditions(keyConditions)
	                .withLimit(batchSize)
	                .withExclusiveStartKey(lastEvaluatedKey)
	                .withConsistentRead(consistentRead);
        		
        		if ( indexName != null )
        		{
        			queryRequest.withIndexName( indexName );
        		}
        		
        		QueryResult result = sDynamoDB.query(queryRequest);
        	    for ( Map<String, AttributeValue> item : result.getItems() ) 
        	    {
        	    	itemProcessor.processItem(item);
        	    	--resultsToGO;
        	    }
        	    
        	    lastEvaluatedKey = result.getLastEvaluatedKey();

        	    if ( maxResults > 0 )
        	    {
	        	    if ( resultsToGO <= 0 )
	        	    {
	        	    	lastEvaluatedKey = null;
	        	    }
	        	    else
		        	if ( batchSize > resultsToGO )
		        	{
		        		batchSize = resultsToGO;
		        	}
        	    }
        	} 
        	while ( lastEvaluatedKey != null );
        	
        	return true;
        }
        catch ( ResourceNotFoundException notFound )
        {
        }
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to query objects from DynamoDB",exception);
        	exception.printStackTrace();
        }
        
        return false;
    }
    
    public static boolean ScanObjects( String tableName, Map<String, Condition> filterConditions, Boolean consistentRead, DynamoDBQueryProcessor itemProcessor, int maxItemsPerSecond, int batchSize )
    {
        try
        {
			if ( batchSize <= 0 )
        	{
        		batchSize = 15;
        	}
        	
        	if ( maxItemsPerSecond <= 0 )
        	{
        		maxItemsPerSecond = -1;
        	}
        	
        	long minTimeBetweenOperations = 1000 * batchSize / maxItemsPerSecond;
        	long timeToken = 0;

        	Map<String, AttributeValue> lastEvaluatedKey = null;
        	
        	do
        	{
        		if ( maxItemsPerSecond > 0 )
        		{// only pace our queries if requested to do so
        			timeToken = WaitBetweenOperations( timeToken, minTimeBetweenOperations );
        		}
        		
        		ScanRequest scanRequest;
        		if ( filterConditions != null )
        		{
        			scanRequest = new ScanRequest()
		                .withTableName( tableName )
		                .withScanFilter(filterConditions)
		                .withLimit(batchSize)
		                .withExclusiveStartKey(lastEvaluatedKey);
        		}
        		else
        		{
        			scanRequest = new ScanRequest()
		                .withTableName( tableName )
		                .withLimit(batchSize)
		                .withExclusiveStartKey(lastEvaluatedKey);
        		}
        		
        		ScanResult result = sDynamoDB.scan(scanRequest);
        	    for ( Map<String, AttributeValue> item : result.getItems() ) 
        	    {
        	    	itemProcessor.processItem(item);
        	    }
        	    lastEvaluatedKey = result.getLastEvaluatedKey();
        	} 
        	while ( lastEvaluatedKey != null );
        	
        	return true;
        	
        }
        catch ( ResourceNotFoundException notFound )
        {
        	sLogger.error("Failed to scan objects from DynamoDB with ResourceNotFoundException",notFound);
        }
		catch (AmazonServiceException ase)
		{
			String message = "Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon DynamoDB, but was rejected with an error response for some reason.\n"
			        + "Error Message:    " + ase.getMessage() + "\n"
			        + "HTTP Status Code: " + ase.getStatusCode() + "\n"
			        + "AWS Error Code:   " + ase.getErrorCode() + "\n"
			        + "Error Type:       " + ase.getErrorType() + "\n"
			        + "Request ID:       " + ase.getRequestId();
			System.out.println(message);
			sLogger.error(message,ase);
		} 
        catch (AmazonClientException ace) 
        {
        	String message = "Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with DynamoDB, "
			        + "such as not being able to access the network.\nError Message: " + ace.getMessage();
			System.out.println(message);
			sLogger.error(message,ace);
        } 
        catch ( Exception exception ) 
        {
        	sLogger.error("Failed to scan objects from DynamoDB",exception);
        	exception.printStackTrace();
        }
        
        return false;
    }

}
