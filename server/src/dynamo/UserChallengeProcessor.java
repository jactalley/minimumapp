package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class UserChallengeProcessor implements DynamoDBQueryProcessor
{
	public List<Challenge> list = new ArrayList<Challenge>();
	
	public void	processItem( Map<String, AttributeValue> object )
	{
		try 
		{
			String challengeID = object.get("challengeID").getS();
			
			list.add(DKMDynamoDB.LoadChallenge(challengeID));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}