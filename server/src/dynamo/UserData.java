package dynamo;

import java.util.UUID;

public class UserData 
{
	private String	crossAppID;
	private String	facebookUserID;
	private String	userId;
	private String	firstName;
	private String	lastName;
	
	public void initialize( String crossAppID, String fbID, String fName, String lName )
	{
		this.crossAppID = crossAppID;
		facebookUserID = fbID;
		firstName = fName;
		lastName = lName;
		userId = generateNewUserID();
	}
	
	public static String generateNewUserID()
	{
		UUID id = UUID.randomUUID();
		String newID = id.toString();
		return newID;
	}
	
	public String getCrossAppID() 
	{
		return crossAppID;
	}
	public void setCrossAppID(String crossAppID) 
	{
		this.crossAppID = crossAppID;
	}
	
	public String getFacebookUserID() 
	{
		return facebookUserID;
	}
	public void setFacebookUserID(String facebookUserID) 
	{
		this.facebookUserID = facebookUserID;
	}
	
	public String getUserId() 
	{
		return userId;
	}
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
	
	public String getFirstName() 
	{
		return firstName;
	}
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	
	public String getLastName() 
	{
		return lastName;
	}
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
}
