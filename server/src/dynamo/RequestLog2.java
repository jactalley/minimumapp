package dynamo;

public class RequestLog2 implements Comparable<RequestLog2>
{
	private String	friendID;	
	private long 	requestTimestamp;
	private int		requestEligible;
	
	public String getFriendID() 
	{
		return friendID;
	}

	public void setFriendID(String friendID) 
	{
		this.friendID = friendID;
	}

	public long getRequestTimestamp() 
	{
		return requestTimestamp;
	}

	public void setRequestTimestamp(long requestTimestamp) 
	{
		this.requestTimestamp = requestTimestamp;
	}

	public int getRequestEligible() 
	{
		return requestEligible;
	}

	public void setRequestEligible(int requestEligible) 
	{
		this.requestEligible = requestEligible;
	}
	
	public int compareTo( RequestLog2 other ) 
	{
		long result = this.requestTimestamp - other.getRequestTimestamp();
		if ( result > 0 ) return 1;
		if ( result < 0 ) return -1;
		return 0;
	}
}
