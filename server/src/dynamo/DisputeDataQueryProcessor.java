package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class DisputeDataQueryProcessor implements DynamoDBQueryProcessor
{
	public List<DisputeData> list = new ArrayList<DisputeData>();

	public void	processItem( Map<String, AttributeValue> object )
	{
		DisputeData data = new DisputeData();
		data.setHostUserID( object.get("userID").getS() );
		data.setHostPurchaseID( object.get("purchaseID").getS() );
		data.setEmail( object.get("email").getS() );
		data.setUserComment( object.get("userComment").getS() );
		data.setSupportComment( object.get("supportComment").getS() );
		data.setStatus( Integer.parseInt(object.get("status").getN()) );
		data.setHostTimeCreated( object.get("hostTimeCreated").getS() );
		data.setDisputeTimestamp( Long.parseLong( object.get("disputeTime").getN() ) );
		data.setUpdatedTimestamp( Long.parseLong( object.get("updatedTime").getN() ) );
		data.setTest( Integer.parseInt(object.get("test").getN()) );
		list.add( data );
	}

}
