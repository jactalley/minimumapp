package dynamo;

import java.util.Calendar;


public class LoginData 
{
	private String	userID;	
	private String	dailySessionID;	
	private long	firstTimestamp;
	private long	dailyTimestamp;
	private long	lastTimestamp;
	private int		consecutiveLoginCount;
	private int		loginCount;
	
	public LoginData( String userID, String dailySessionID, boolean firstLogin )
	{
		this.userID = userID;
		this.dailySessionID = dailySessionID;
		if ( firstLogin )
		{
			this.consecutiveLoginCount = 1;
			this.loginCount = 1;
			this.firstTimestamp = Calendar.getInstance().getTime().getTime();
			this.lastTimestamp = this.firstTimestamp;
			this.dailyTimestamp = this.firstTimestamp;
		}
	}

	public String getDailySessionID() 
	{
		return dailySessionID;
	}
	public void setDailySessionID(String dailySessionID) 
	{
		this.dailySessionID = dailySessionID;
	}
	public String getUserID() 
	{
		return userID;
	}
	public void setUserID(String userID) 
	{
		this.userID = userID;
	}
	public long getFirstTimestamp() 
	{
		return firstTimestamp;
	}
	public void setFirstTimestamp(long firstTimestamp) 
	{
		this.firstTimestamp = firstTimestamp;
	}
	public long getDailyTimestamp() 
	{
		return dailyTimestamp;
	}
	public void setDailyTimestamp(long dailyTimestamp) 
	{
		this.dailyTimestamp = dailyTimestamp;
	}
	public long getLastTimestamp() 
	{
		return lastTimestamp;
	}
	public void setLastTimestamp(long lastTimestamp) 
	{
		this.lastTimestamp = lastTimestamp;
	}
	public int getConsecutiveLoginCount() 
	{
		return consecutiveLoginCount;
	}
	public void setConsecutiveLoginCount(int consecutiveLoginCount) 
	{
		this.consecutiveLoginCount = consecutiveLoginCount;
	}
	public int getLoginCount() 
	{
		return loginCount;
	}
	public void setLoginCount(int loginCount) 
	{
		this.loginCount = loginCount;
	}
}
