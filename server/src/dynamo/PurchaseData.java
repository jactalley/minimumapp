package dynamo;

import services.PurchaseLog;


public class PurchaseData 
{
	public static final int	UNKNOWN_STATUS = 0;
	public static final int	UNCLAIMED_STATUS = 1;
	public static final int	CLAIMED_STATUS = 2;

	public PurchaseData(){}
	public PurchaseData( PurchaseLog oldLog )
	{
		this.hostUserID			= oldLog.getHostUserID();	
		this.hostPurchaseID		= oldLog.getHostPurchaseID();	
		this.sku				= oldLog.getSku();	
		this.chargeAction		= oldLog.getChargeAction();	
		this.chargeStatus		= oldLog.getChargeStatus();	
		this.currency			= oldLog.getCurrency();	
		this.amount				= oldLog.getAmount();
		this.exchangeRate		= oldLog.getExchangeRate();
		this.quantity			= oldLog.getQuantity();
		this.coins				= oldLog.getCoins();
		this.purchaseTimestamp	= oldLog.getPurchaseTimestamp().getTime();
		this.claimedTimestamp	= oldLog.getClaimedTimestamp().getTime();
		this.claimStatus		= oldLog.getClaimStatus();
		this.test				= oldLog.getTest();
	}
	
	private String hostUserID;	
	private String hostPurchaseID;	
	private String sku;	
	private String chargeAction;	
	private String chargeStatus;	
	private String currency;	
	private float amount;
	private float exchangeRate;
	private int quantity;
	private int coins;
	private long purchaseTimestamp;
	private long claimedTimestamp;
	private int claimStatus;
	private int test;
	
	public String getHostUserID() {
		return hostUserID;
	}
	public void setHostUserID(String hostUserID) {
		this.hostUserID = hostUserID;
	}
	public String getHostPurchaseID() {
		return hostPurchaseID;
	}
	public void setHostPurchaseID(String hostPurchaseID) {
		this.hostPurchaseID = hostPurchaseID;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getChargeAction() {
		return chargeAction;
	}
	public void setChargeAction(String chargeAction) {
		this.chargeAction = chargeAction;
	}
	public String getChargeStatus() {
		return chargeStatus;
	}
	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public float getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public long getPurchaseTimestamp() {
		return purchaseTimestamp;
	}
	public void setPurchaseTimestamp(long purchaseTimestamp) {
		this.purchaseTimestamp = purchaseTimestamp;
	}
	public long getClaimedTimestamp() {
		return claimedTimestamp;
	}
	public void setClaimedTimestamp(long claimedTimestamp) {
		this.claimedTimestamp = claimedTimestamp;
	}
	public int getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(int claimStatus) {
		this.claimStatus = claimStatus;
	}
	public int getTest() {
		return test;
	}
	public void setTest(int test) {
		this.test = test;
	}

}
