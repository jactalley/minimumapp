package dynamo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import services.Transaction;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class TransactionQueryProcessor implements DynamoDBQueryProcessor
{
	public List<Transaction> list = new ArrayList<Transaction>();

	public void	processItem( Map<String, AttributeValue> object )
	{
		Transaction data = new Transaction();
		data.setPlayerID( object.get("userID").getS() );
		data.setTimestamp( Long.parseLong( object.get("time").getN() ) );
		data.setID( object.get("txnID").getS() );
		data.setAction( object.get("action").getS() );
		data.setMarketID( Integer.parseInt(object.get("marketID").getN()) );
		data.setResultCode( Integer.parseInt(object.get("result").getN()) );

		if ( object.get("items") != null )
		{
			String 		dataString = object.get("items").getS();
			String[]	values = dataString.split(",");
			for ( int i = 0; i < values.length; ++i )
			{
				data.getData().add( Integer.parseInt( values[i] ) );
			}
		}
		
		list.add( data );
	}

}
