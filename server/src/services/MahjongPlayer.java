package services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.nio.ByteBuffer;
//import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;

import model.ItemDescription;
import model.ItemManager;

import dynamo.BinaryMahjongPlayer;
import dynamo.Challenge;
import dynamo.LoginData;
import dynamo.DKMDynamoDB;

public class MahjongPlayer
{
	private String currentSessionID;
	private String userID;
	private String hostUserID;
	private long livesTimestamp;
	private int initialLifeTime;
	private Set<OwnedItem> items;
	private Set<HighScore> highScores;
	private int consecutiveDailyLogins;
	private int totalLogins;
	private int secondsToServiceTime;
	
	// version 1.2
    private int lastPuzzleUnlocked;
    private long puzzleUnlockTimestamp;
    private int secondsToPuzzleUnlock;
    
    private int        majorVersion;
    private int        minorVersion;
	
	private HighScore currentProcessingHighScore;

	public MahjongPlayer()
	{
	}
	
	public MahjongPlayer( String hostID, String userID, long timestamp )
	{
		Initialize( hostID, userID, timestamp );
	}
	
	public MahjongPlayer( BinaryMahjongPlayer playerData ) throws Exception
	{
		CreateFromBinaryData( playerData );
	}

	static public final int START_COIN_COUNT			= 500;
	static public final int START_LIFE_COUNT			= 5;
	static public final int MAX_LIVES					= 5;
	static public final int HOURS_UNTIL_LIVES_TIMER_RESET = 3;// see Energy.java for time per energy/life gain
	
	static public final int START_LAST_PUZZLE_UNLOCKED_OLD_DATA	= 79;
    static public final int START_LAST_PUZZLE_UNLOCKED	= 79;
    static public final int SECONDS_TO_PUZZLE_UNLOCK	= 36*3600; // 36 hours

	public void Initialize( String hostID, String userID, long timestamp )
	{
		// This is the 'CREATE ACCOUNT' method...
		this.userID = userID;
		secondsToServiceTime = 0;
		totalLogins = 0;
		consecutiveDailyLogins = 0;
        lastPuzzleUnlocked = 0;
        secondsToPuzzleUnlock = SECONDS_TO_PUZZLE_UNLOCK;
        puzzleUnlockTimestamp = Calendar.getInstance().getTime().getTime();
		hostUserID = hostID;
		livesTimestamp = Calendar.getInstance().getTime().getTime();
		items = new HashSet<OwnedItem>();
		highScores = null;
		
		Transaction transaction = new Transaction( userID, TransactionType.CREATE_ACCOUNT, timestamp );
		transaction.AddChangeItemCount( ItemManager.COIN_ITEM_INDEX, START_COIN_COUNT );
		transaction.AddChangeItemCount( ItemManager.LIFE_ITEM_INDEX, START_LIFE_COUNT );
		transaction.AddChangeItemCount( ItemManager.UNLOCK_PUZZLES, START_LAST_PUZZLE_UNLOCKED );
		if ( ValidTransaction( transaction ) )
		{
			ApplyTransaction( transaction );
		}
		
		DKMDynamoDB.SaveTransaction( transaction );
	}
	
	public int CalculateLivesToGain()
	{
		int livesToGain = 0;
		
		long nowTimestamp = Calendar.getInstance().getTime().getTime();		
		long secondsElapsed = ( nowTimestamp - livesTimestamp ) / 1000;
		
		// "overnight" gets a full reset.
		if( secondsElapsed >= ( HOURS_UNTIL_LIVES_TIMER_RESET * 3600) )
		{
			livesToGain = MAX_LIVES;
			
			setLivesTimestamp( nowTimestamp );
			initialLifeTime = model.Energy.SECONDS_PER_ENERGY_GAIN;
		}
		// we may gain lives here, but we always update the timestamp.
		else
		{			
			livesToGain = (int) Math.floor(secondsElapsed / model.Energy.SECONDS_PER_ENERGY_GAIN);
			
			int secondsIn = (int) secondsElapsed % model.Energy.SECONDS_PER_ENERGY_GAIN;
			// mark the lives timestamp as backwards from "now" based on the elapsed calculation:
			setLivesTimestamp( nowTimestamp - (secondsIn * 1000) );
			initialLifeTime = model.Energy.SECONDS_PER_ENERGY_GAIN - secondsIn;			
		}
		
		return livesToGain;
	}

	public void CalculatePuzzlesToUnlock( LoginData loginData, Transaction transaction )
	{
		if( majorVersion == 1 && minorVersion < 2 )
		{
			// if we are upgrading from an old version, we need to unlock puzzles for the player since
			// their very first login
			puzzleUnlockTimestamp = loginData.getFirstTimestamp();
			transaction.AddChangeItemCount( ItemManager.UNLOCK_PUZZLES_AT_LOGIN, START_LAST_PUZZLE_UNLOCKED_OLD_DATA );			
		}
		
		long nowTimestamp = Calendar.getInstance().getTime().getTime();		
		long secondsElapsed = (nowTimestamp - puzzleUnlockTimestamp) / 1000;

		int puzzlesToUnlock = (int) Math.floor(secondsElapsed / SECONDS_TO_PUZZLE_UNLOCK);
		
		int secondsIn = (int) secondsElapsed % SECONDS_TO_PUZZLE_UNLOCK;
		// mark the time stamp as backwards from "now" based on the elapsed calculation:
		setPuzzleUnlockTimestamp( nowTimestamp - (secondsIn * 1000) );
		secondsToPuzzleUnlock = SECONDS_TO_PUZZLE_UNLOCK - secondsIn;
	
		if ( puzzlesToUnlock > 0 )
		{
			transaction.AddChangeItemCount( ItemManager.UNLOCK_PUZZLES_AT_LOGIN, puzzlesToUnlock );	
		}
	}
	
	public void addLives( int lives )
	{		
		// make sure we cap energy gained by time
		int newLives = livesCount() + lives;
		newLives = Math.max( 0, Math.min( newLives, MAX_LIVES ) );
		livesCountUpdate( newLives );
	}
	
	public void UpdateLivesTimestamp()
	{
		setLivesTimestamp( Calendar.getInstance().getTime().getTime() );
	}
	
	public OwnedItem	FindItemByIndex( int itemIndex )
	{
		for ( OwnedItem item : items )
		{
			if ( item.getItemIndex() == itemIndex )
			{
				return item;
			}
		}
		
		ItemDescription	description = ItemManager.ItemDescriptionFromIndex( itemIndex );
		if ( description == null ) return null;
		
		OwnedItem	newItem = new OwnedItem();
		newItem.setCount(0);
		newItem.setItemType(description.itemType);
		newItem.setItemID(description.itemID);
		newItem.setItemData(description.itemData);
		newItem.setItemIndex(itemIndex);
		items.add(newItem);
		
		return newItem;
	}
	
	public boolean ValidTransaction( Transaction transaction )
	{
		// this method should only be used if you know you have a valid transaction...
		// apply the transaction...
		for ( int i = 0; i < transaction.getData().size(); )
		{
			int thisItem = i;
			int itemIndex = transaction.getData().get(i);++i;
			int itemDelta = transaction.getData().get(i);++i;
			if ( !ValidTransactionItem( itemIndex, itemDelta, transaction.getData(), thisItem ) )
			{
				return false;
			}
		}
		
		return true;
	}
	
	public boolean ApplyTransaction( Transaction transaction )
	{
		currentProcessingHighScore = null;
		// this method should only be used if you know you have a valid transaction...
		// apply the transaction...
		boolean dataChanged = false;
		for ( int i = 0; i < transaction.getData().size(); )
		{
			int itemIndex = transaction.getData().get(i);++i;
			int itemDelta = transaction.getData().get(i);++i;
			dataChanged = ApplyTransactionItem( itemIndex, itemDelta ) || dataChanged;
		}
		
		return dataChanged;
	}
	
	private boolean ValidTransactionItem( int itemIndex, int itemDelta, List<Integer> allItems, int thisItem )
	{// see if this transaction item is possible 
		if ( itemIndex < 0 )
		{// this is just a marker item for tracking purposes, like purchasing time, or a command like update score
			return true;
		}
		
		if ( itemDelta >= 0 )
		{// adding items should always pass, but need to check for setting in-use flag
			if ( itemDelta == Transaction.ITEM_IN_USE_TAG )
			{
				OwnedItem item = FindItemByIndex( itemIndex );
				int itemCount = item.getCount();
				
				// see if this item gets changed within this transaction...
				for ( int i = 0; i < thisItem; )
				{
					int previousIndex = allItems.get(i);++i;
					int previousDelta = allItems.get(i);++i;
					if ( ( previousIndex == itemIndex ) &&
						 ( previousDelta != Transaction.ITEM_IN_USE_TAG ) )
					{
						itemCount += previousDelta;
					}
				}
				
				// we can use this item if own it, or we will own it while processing this transaction item
				return ( itemCount > 0 );
			}
			return true;
		}
		
		OwnedItem item = FindItemByIndex( itemIndex );
		int endCount = item.getCount() + itemDelta;
		if ( endCount < 0 ) return false;
		
		return true;
	}
	
	private boolean ApplyTransactionItem( int itemIndex, int itemDelta )
	{// assume this has been validated with a call to ValidTransactionItem() first
		if ( itemIndex < 0 )
		{
			//Instant-use purchases will go here
			if( itemIndex == ItemManager.PUZZLE_INDEX_SET )
			{
				boolean found = false;
				
				for ( HighScore playersScore : getHighScores() )
				{
					if( playersScore.getPuzzle() == itemDelta )
					{
						currentProcessingHighScore = playersScore;
						found = true;
					}
				}
				
				if( !found )
				{
					currentProcessingHighScore = new HighScore();
					currentProcessingHighScore.setPuzzle(itemDelta);
					currentProcessingHighScore.setPlayer(this);
					getHighScores().add(currentProcessingHighScore);
				}
			}
			
			if( itemIndex == ItemManager.PUZZLE_SCORE_SET )
			{
				if( currentProcessingHighScore == null ) return false;
				
				boolean completed = false;
				boolean updated = false;
				
				if( itemDelta < 0 )
				{
					itemDelta *= -1;
					completed = true;
				}
				
				if( itemDelta > currentProcessingHighScore.getScore() )
				{
					currentProcessingHighScore.setScore(itemDelta);
					updated = true;
				}
				
				if( !currentProcessingHighScore.getCompleted() && completed )
				{
					currentProcessingHighScore.setCompleted(completed);
					updated = true;
				}
				
				return updated;
			}
			
			if( itemIndex == ItemManager.PUZZLE_STARS_SET )
			{
				if( currentProcessingHighScore == null ) return false;
				
				if( itemDelta > currentProcessingHighScore.getStars() )
				{
					currentProcessingHighScore.setStars(itemDelta);
					return true;
				}
			}
			
			if( itemIndex == ItemManager.PUZZLE_ATTEMPTS_COINS_SET )
			{
				if( currentProcessingHighScore == null ) return false;
				
				if( itemDelta < 256 )
				{
					currentProcessingHighScore.setAttemptsOrCoins(itemDelta);
					return true;
				}
			}
			
            if( itemIndex == ItemManager.UNLOCK_PUZZLES )
            {
                lastPuzzleUnlocked += itemDelta;
                puzzleUnlockTimestamp = Calendar.getInstance().getTime().getTime();
                secondsToPuzzleUnlock = SECONDS_TO_PUZZLE_UNLOCK;
            }
			
            if( itemIndex == ItemManager.UNLOCK_PUZZLES_AT_LOGIN )
            {
                lastPuzzleUnlocked += itemDelta;
            }

			// this is just a marker item for tracking purposes (like purchasing time, but time isn't an item the player owns)
			return false;
		}
		
		// simply change the item counter...
		return ( AddItemByIndex( itemIndex, itemDelta ) != 0 );
	}
	
	public boolean UpdateOrAddHighScore( MahjongPlayer player, HighScore score )
	{
		if ( score == null  || score.getPuzzle() < 0 )
		{
			sLogger.error("Trying to update invalid score or puzzle!");
		}
		
		boolean found = false;
		boolean scoreUpdated = false;
		
		for ( HighScore playersScore : player.getHighScores() )
		{
			if ( playersScore.getPuzzle() == score.getPuzzle() )
			{
				// score can only go up
				if ( playersScore.getScore() < score.getScore() )
				{
					playersScore.setScore(score.getScore());
					playersScore.setStars(score.getStars());
					scoreUpdated = true;
				}

				// we need to always be able to set attempts, even if it's lower (might do so via the admin tool.)
				playersScore.setAttemptsOrCoins(score.getAttemptsOrCoins());
				scoreUpdated = true;

				// can only go from not completed -> completed.
				if ( !playersScore.getCompleted() && score.getCompleted() )
				{
					playersScore.setCompleted(score.getCompleted());
					scoreUpdated = true;
				}
				found = true;
				break;
			}
		}
		
		if ( !found )
		{
			HighScore newScore = new HighScore();
			newScore.setPuzzle(score.getPuzzle());
			newScore.setCompleted(score.getCompleted());
			newScore.setScore(score.getScore());
			newScore.setStars(score.getStars());
			newScore.setAttemptsOrCoins(score.getAttemptsOrCoins());
			newScore.setPlayer(player);
			player.getHighScores().add(newScore);
			scoreUpdated = true;
		}
		
		return scoreUpdated;
	}

	private int	AddItemByIndex( int itemIndex, int count )
	{
		int countAdded = 0;
		OwnedItem item = FindItemByIndex( itemIndex );
		
		int maxCount = ItemManager.MaxItemCount( item.getItemType() );
		
		if ( item.getCount() < maxCount || count < 0 )
		{
			int oldCount = item.getCount();// store old count for calcing correct added count
			
			int newCount = item.getCount() + count;
			if ( newCount < 0 )
			{
				newCount = 0;
			}
			else
			if ( newCount > maxCount )
			{
				newCount = maxCount;
			}
			item.setCount(newCount);
			
			countAdded = item.getCount() - oldCount;// calc correct added count
		}
		
		return countAdded;
	}
	
	public String getCurrentSessionID() {
		return currentSessionID;
	}

	public void setCurrentSessionID(String currentSessionID) {
		this.currentSessionID = currentSessionID;
	}
	
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getHostUserID() {
		return hostUserID;
	}

	public void setHostUserID(String hostUserID) {
		this.hostUserID = hostUserID;
	}

	// note: avoiding getLives so BlazeDS won't add a 'lives' property
	public int livesCount() 
	{
		for ( OwnedItem item : items )
		{
			if ( item.getItemIndex() == ItemManager.LIFE_ITEM_INDEX )
			{
				return item.getCount();
			}
		}

		return 0;
	}

	// note: avoiding setLives so BlazeDS won't add a 'lives' property
	public void livesCountUpdate(int lives) 
	{
		for ( OwnedItem item : items )
		{
			if ( item.getItemIndex() == ItemManager.LIFE_ITEM_INDEX )
			{
				item.setCount(lives);
				break;
			}
		}
	}

	public long getLivesTimestamp() {
		return livesTimestamp;
	}

	public void setLivesTimestamp(long livesTimestamp) {
		this.livesTimestamp = livesTimestamp;
	}

	public int getInitialLifeTime() {
		return initialLifeTime;
	}

	public void setInitialLifeTime(int initialLifeTime) {
		this.initialLifeTime = initialLifeTime;
	}
	
	public int GetCoinCount()
	{
		for ( OwnedItem item : items )
		{
			if ( item.getItemIndex() == ItemManager.COIN_ITEM_INDEX )
			{
				return item.getCount();
			}
		}

		return 0;
	}
	
	public int GetLevel()
	{
		int level = 0;
		return level;
	}

	public Set<OwnedItem> getItems() {
		return items;
	}

	public void setItems(Set<OwnedItem> items) {
		this.items = items;
	}

	public Set<HighScore> getHighScores() {
		return highScores;
	}

	public void setHighScores(Set<HighScore> highScores) {
		this.highScores = highScores;
	}

	public int getConsecutiveDailyLogins() {
		return consecutiveDailyLogins;
	}

	public void setConsecutiveDailyLogins(int consecutiveDailyLogins) {
		this.consecutiveDailyLogins = consecutiveDailyLogins;
	}

	public int getTotalLogins() {
		return totalLogins;
	}

	public void setTotalLogins(int totalLogins) {
		this.totalLogins = totalLogins;
	}
	
    public int getLastPuzzleUnlocked()
    {
        return lastPuzzleUnlocked;
    }
    
    public void setLastPuzzleUnlocked( int lastPuzzleUnlocked )
    {
        this.lastPuzzleUnlocked = lastPuzzleUnlocked;
    }
    
    public long getPuzzleUnlockTimestamp()
    {
        return puzzleUnlockTimestamp;
    }
    
    public void setPuzzleUnlockTimestamp( long puzzleUnlockTimestamp )
    {
        this.puzzleUnlockTimestamp = puzzleUnlockTimestamp;
    }
    
    public int getSecondsToPuzzleUnlock()
    {
        return secondsToPuzzleUnlock;
    }
    
    public void setSecondsToPuzzleUnlock( int secondsToPuzzleUnlock )
    {
        this.secondsToPuzzleUnlock = secondsToPuzzleUnlock;
    }

	public int getSecondsToServiceTime() {
		return secondsToServiceTime;
	}

	public void setSecondsToServiceTime(int secondsToServiceTime) {
		this.secondsToServiceTime = secondsToServiceTime;
	}
	
	public HighScore FindHighScore( int puzzleID )
	{
		for ( HighScore score : highScores )
		{
			if ( score.getPuzzle() == puzzleID )
			{
				return score;
			}
		}
		
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	
	public Challenge CreateChallenge(List<String> challengees, Integer powerLevel, Integer level, Integer score)
	{
		Challenge data = new Challenge();

		UUID id = UUID.randomUUID();
		data.challengeID = id.toString();
		
		data.userIds.add(userID);
		data.userIds.addAll(challengees);
		data.scores.add(score);
		
		for(int i = 0; i < challengees.size(); i++)
		{
			data.scores.add(0);
		}
		
		data.type = Challenge.STANDARD_CHALLENGE;
		data.status = Challenge.NEW;
		data.timeoutTime = Calendar.getInstance().getTime().getTime() + (48*3600000); //48 hours in the future
		data.resultsViewed = 0;
		data.level = level;
		data.powerLevel = powerLevel;
		
		return data;
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	/*
	 * This code block relates to saving the player data in a binary blob
	 * */
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	
	
	private static final Logger  				sLogger = Logger.getLogger(MahjongPlayer.class);
	
	public BinaryMahjongPlayer CreateBinaryData() throws Exception
	{
		try
		{
			BinaryMahjongPlayer binaryData = new BinaryMahjongPlayer();
			binaryData.sessionID = this.currentSessionID;
			binaryData.hostUserID = hostUserID;
			binaryData.userId = userID;
			binaryData.format = BinaryMahjongPlayer.UNCOMPRESSED_FORMAT;

			ByteArrayOutputStream outArray = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream( outArray );
			
			out.writeShort(BinaryMahjongPlayer.MAJOR_VERSION);
			out.writeShort(BinaryMahjongPlayer.MINOR_VERSION);
			
			out.writeUTF(hostUserID);

			out.writeLong( livesTimestamp );
			
            out.writeShort(lastPuzzleUnlocked);
            out.writeLong(puzzleUnlockTimestamp);

			//private Set<OwnedItem> items;
			// write out items in a packed array of counts.
			// only the first TOTAL_INT_SIZE_ITEM_COUNT items can have values outside the short range.
			int	itemCounts[] = new int[ItemManager.UNIQUE_ITEM_COUNT];
			for ( int i = 0; i < ItemManager.UNIQUE_ITEM_COUNT; i++ )
			{// initialize item counts to zero
				itemCounts[i] = 0;
			}
			int arrayIndex;
			for ( OwnedItem item : items )
			{
				if ( item.getItemIndex() >= 0 )
				{
					if ( item.getCount() > 0 )// we don't want to write out negative values
					{
						arrayIndex = ItemManager.ItemArrayIndexFromVirtualIndex( item.getItemIndex() );
						itemCounts[arrayIndex] = item.getCount();
					}
				}
			}
			
			
			
			// RETAINED TO BE BACKARDS COMPATIBILE WITH OLD DATA, BUT JUST 0.
			out.writeByte( ItemManager.UNIQUE_IN_USE_ITEM_COUNT );
			
			// write out the size of the unique item lists
			out.writeShort(ItemManager.UNIQUE_INT_ITEM_COUNT);
			out.writeShort(ItemManager.UNIQUE_SHORT_ITEM_COUNT);
			out.writeShort(ItemManager.UNIQUE_BYTE_ITEM_COUNT);
			
			
			// save integer counts
			int i = 0;
			int last = ItemManager.UNIQUE_INT_ITEM_COUNT;
			for ( ; i < last; ++i )
			{
				out.writeInt(itemCounts[i]);
			}
			
			// save short counts
			last += ItemManager.UNIQUE_SHORT_ITEM_COUNT;
			for ( ; i < last; ++i )
			{
				out.writeShort(itemCounts[i]);
			}
			
			// save byte counts
			last += ItemManager.UNIQUE_BYTE_ITEM_COUNT;
			for ( ; i < last; ++i )
			{
				out.writeByte(itemCounts[i]);
			}
			
			// write out score data
			int	lastStoryPuzzleWasCompleted = -1;
			int lastStoryPuzzle = -1;
			if ( highScores != null )
			{
				for ( HighScore score : highScores )
				{
					int puzzleID = score.getPuzzle();
					if ( puzzleID > lastStoryPuzzle )
					{
						lastStoryPuzzle = puzzleID;
						lastStoryPuzzleWasCompleted = score.getCompleted() ? 1 : 0;
					}
				}
			}

			out.writeByte(lastStoryPuzzleWasCompleted);
			out.writeShort(lastStoryPuzzle);
			
			if ( lastStoryPuzzle >= 0 )
			{
				int	storyScores[] = new int[lastStoryPuzzle+1];
				for ( int s = 0; s <= lastStoryPuzzle; s++ )
				{// initialize scores to -1 so we know they have been written later
					storyScores[s] = -1;
				}
				for ( HighScore score : highScores )
				{
					int puzzleID = score.getPuzzle();
					storyScores[puzzleID] = score.getScore() | (score.getAttemptsOrCoins() << 21) | (score.getStars() << 29);
				}
				
				// give puzzle zero a default score if it hasn't been assigned
				if ( storyScores[0] < 0 ) storyScores[0] = 1015;
				out.writeInt(storyScores[0]);
				// now write the rest of the scores, giving a previous puzzles score as the
				// puzzle score where we find holes in the data
				for ( int p = 1; p <= lastStoryPuzzle; p++ )
				{
					// make sure every puzzle has an associated score (don't let holes exist)
					if ( storyScores[p] < 0 ) storyScores[p] = storyScores[p>>1];
					out.writeInt(storyScores[p]);
				}
			}

			binaryData.data = ByteBuffer.wrap( outArray.toByteArray() );
			
			return binaryData;			
		}
		catch( Exception exception ) 
        {
        	sLogger.error("Failed to create binary data for player!",exception);
        	exception.printStackTrace();
        	throw(exception);
        }
	}
	
	public void	CreateFromBinaryData( BinaryMahjongPlayer playerData ) throws Exception
	{
		try
		{
			if ( !playerData.format.equals( BinaryMahjongPlayer.UNCOMPRESSED_FORMAT ) )
			{
				return;
			}
			
			currentSessionID = playerData.sessionID;
			hostUserID = playerData.hostUserID;
			userID = playerData.userId;
			
			// set some initial values for transient data
			secondsToServiceTime = 0;
			consecutiveDailyLogins = 0;
			totalLogins = 0;
			initialLifeTime = 0;
			
			ByteBuffer dataBuffer = playerData.data;
			ByteArrayInputStream inArray = new ByteArrayInputStream(dataBuffer.array());
			DataInputStream in = new DataInputStream( inArray );

			// store these for later upgrade logic
            majorVersion = in.readUnsignedShort();
            minorVersion = in.readUnsignedShort();
    		
			String internalHostUserID = in.readUTF();
			if ( !internalHostUserID.equals( hostUserID ) )
			{
	        	sLogger.error( "Table and internal host ID mis-match in player binary data! ( " + hostUserID + " != " + internalHostUserID + " )"  );
			}
			
			long timeMillis = in.readLong();
			livesTimestamp = timeMillis;
			
			
            if( minorVersion >= 2 )
            {
                lastPuzzleUnlocked = in.readShort();
                timeMillis = in.readLong();
                puzzleUnlockTimestamp = timeMillis;
            }
			else
			{
				lastPuzzleUnlocked = 0;
				puzzleUnlockTimestamp = 0;
			}

			// read the in-use count, but do nothing with it.  retained to be backwards compatible.
			in.readUnsignedByte();
			
			// write out the size of the unique item list
			int uniqueIntItemCount = in.readUnsignedShort();
			int uniqueShortItemCount = in.readUnsignedShort();
			int uniqueByteItemCount = in.readUnsignedShort();
			
			items = new HashSet<OwnedItem>();
			
			int i;

			for ( i = 0; i < uniqueIntItemCount; ++i )
			{
				int count = in.readInt();
				CreateOwnedItemFromBinary( i+ItemManager.START_INT_ITEM_INDEX, count );
			}

			for ( i = 0; i < uniqueShortItemCount; ++i )
			{
				int count = in.readUnsignedShort();
				CreateOwnedItemFromBinary( i+ItemManager.START_SHORT_ITEM_INDEX, count );
			}

			for ( i = 0; i < uniqueByteItemCount; ++i )
			{
				int count = in.readUnsignedByte();
				CreateOwnedItemFromBinary( i+ItemManager.START_BYTE_ITEM_INDEX, count );
			}
			
			highScores = new HashSet<HighScore>();
			
			int lastStoryPuzzleWasCompleted = in.readByte();
			int lastStoryPuzzle = in.readShort();
			int p;
			int score;
			for ( p = 0; p < lastStoryPuzzle; p++ )
			{
				score = in.readInt();
				CreateHighScoreFromBinary( p, score, 1 );
			}
			if ( p <= lastStoryPuzzle )
			{
				score = in.readInt();// read the final score, and mark complete from the value already read in
				CreateHighScoreFromBinary( p, score, lastStoryPuzzleWasCompleted );
			}
		}
		catch( Exception exception ) 
        {
        	sLogger.error("Failed to create player object from binary data! userID = "+playerData.userId ,exception);
        	exception.printStackTrace();
        	throw(exception);
        }

	}
	
	private OwnedItem CreateOwnedItemFromBinary( int itemIndex, int count )
	{
		if ( count < 0 )
		{
			return null;
		}
		
		ItemDescription	description = ItemManager.ItemDescriptionFromIndex( itemIndex );
		OwnedItem item = new OwnedItem( this, description, count );
		items.add(item);

		return item;
	}
	
	private HighScore CreateHighScoreFromBinary( int puzzleID, int score, int completed )
	{
		if ( puzzleID < 0 )
		{
			return null;
		}
		int stars = (score >> 29) & 0x3;
		int attemptsOrCoins = (score >> 21) & 0xFF;	
		score &= 0x1FFFFF;								// 2097151 = 0b00000000000111111111111111111111
		HighScore highScore = new HighScore();
		highScore.setPlayer(this);
		highScore.setPuzzle(puzzleID);
		highScore.setScore(score);
		highScore.setStars(stars);
		highScore.setAttemptsOrCoins(attemptsOrCoins);
		highScore.setCompleted( completed != 0 );
		highScores.add(highScore);
		return highScore;
	}
	
	/*
	This was used for testing backup & restore of player data...
	public boolean equivalent( MahjongPlayer other )
	{
		if ( !userID.equals(other.userID) )  return false;
		if ( !hostUserID.equals(other.hostUserID) )  return false;
		if ( items.size() != other.items.size() )  return false;
		for ( OwnedItem item : items )
		{
			OwnedItem otherItem = other.FindItem( item.getItemType(), item.getItemID(), item.getItemData() );
			if ( otherItem == null )  return false;
			if ( item.getCount() != otherItem.getCount() )  return false;
		}

		return true;
	}
	*/

}
