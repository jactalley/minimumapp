package services;

import static javax.persistence.GenerationType.IDENTITY;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="transaction_log")
@NamedQueries({
	@NamedQuery(name=TransactionLog.FIND_BY_USER_ID, query="from TransactionLog o where o.userID = :userID"),
	@NamedQuery(name=TransactionLog.FIND_BY_ORDER_ID, query="from TransactionLog o where o.id = :orderID")
})
public class TransactionLog
{
	public static final String	FIND_BY_ORDER_ID = "TransactionLog.FindByOrderID";
	public static final String	FIND_BY_USER_ID = "TransactionLog.FindByUserID";
	public static final String	ORDER_ID_PARM = "orderID";
	public static final String	USER_ID_PARM = "userID";
	
	public static final int	TRADE_COMPLETE_STATUS = 1;
	public static final int	PURCHASE_COMPLETE_STATUS = 2;
	public static final int	DAILY_BONUS_COMPLETE_STATUS = 4;
	public static final int	DISPUTED_STATUS = 256;
	public static final int	REFUNDED_STATUS = 512;
	public static final int	CLOSED_STATUS = 1024;
	
	@Id
	@GeneratedValue( strategy = IDENTITY )
	@Column(name="id")
	private int id;
	
	@org.hibernate.annotations.Index(name = "userIDIndex")
	@Column(name = "user_id", nullable = false, updatable = false)
	private int userID;	
	
	@org.hibernate.annotations.Index(name = "skuIndex")
	@Column(name = "sku", nullable = false, updatable = false)
	private int sku;	
	
	@Column(name="coins", nullable = false, updatable = false)
	private int coins;
	
	@Column(name="order_time", nullable = false, updatable = false)
	private Timestamp orderTimestamp;
	
	@Column(name="update_time", nullable = false, updatable = true)
	private Timestamp statusUpdateTimestamp;
	
	@Column(name="status", nullable = false, updatable = true)
	private int status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getSku() {
		return sku;
	}

	public void setSku(int sku) {
		this.sku = sku;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public Timestamp getOrderTimestamp() {
		return orderTimestamp;
	}

	public void setOrderTimestamp(Timestamp orderTimestamp) {
		this.orderTimestamp = orderTimestamp;
	}

	public Timestamp getStatusUpdateTimestamp() {
		return statusUpdateTimestamp;
	}

	public void setStatusUpdateTimestamp(Timestamp statusUpdateTimestamp) {
		this.statusUpdateTimestamp = statusUpdateTimestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
