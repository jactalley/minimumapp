package services;

import static javax.persistence.GenerationType.IDENTITY;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="purchase_log",
		uniqueConstraints = {@UniqueConstraint(columnNames={"user_id", "purchase_id"})})
@NamedQueries({
	@NamedQuery(name=PurchaseLog.FIND_BY_USER_ID, query="from PurchaseLog o where o.hostUserID = :userID"),
	@NamedQuery(name=PurchaseLog.FIND_BY_PURCHASE_ID, query="from PurchaseLog o where o.hostPurchaseID = :purchaseID")
})
public class PurchaseLog
{
	public static final String	FIND_BY_PURCHASE_ID = "PurchaseLog.FindByPurchaseID";
	public static final String	FIND_BY_USER_ID = "PurchaseLog.FindByUserID";
	public static final String	PURCHASE_ID_PARM = "purchaseID";
	public static final String	USER_ID_PARM = "userID";
	
	public static final String	COMPENSATION_SKU = "DKcompensation";
	public static final String	REFUND_SKU = "DKrefund";
	public static final String	GIFT_SKU = "DKgift";
	public static final String	REWARD_SKU = "DKreward";
	
	public static final int	UNKNOWN_STATUS = 0;
	public static final int	UNCLAIMED_STATUS = 1;
	public static final int	CLAIMED_STATUS = 2;
	

	@Id
	@GeneratedValue( strategy = IDENTITY )
	@Column(name="id")
	private int id;
	
	@org.hibernate.annotations.Index(name = "hostUserIDIndex")
	@Column(name = "user_id", nullable = false, updatable = false, length = 32)
	private String hostUserID;	
	
	@org.hibernate.annotations.Index(name = "hostPurchaseIDIndex")
	@Column(name = "purchase_id", nullable = false, updatable = false, length = 32)
	private String hostPurchaseID;	
	
	@org.hibernate.annotations.Index(name = "skuIndex")
	@Column(name = "sku", nullable = false, updatable = false, length = 32)
	private String sku;	
	
	@Column(name = "chargeAction", nullable = false, updatable = true, length = 32)
	private String chargeAction;	
	
	@Column(name = "chargeStatus", nullable = false, updatable = true, length = 32)
	private String chargeStatus;	
	
	@Column(name = "currency", nullable = false, updatable = false, length = 8)
	private String currency;	
	
	@Column(name="amount", nullable = false, updatable = false)
	private float amount;
	
	@Column(name="exchange_rate", nullable = false, updatable = false)
	private float exchangeRate;
	
	@Column(name="quantity", nullable = false, updatable = false)
	private int quantity;
	
	@Column(name="coins", nullable = false, updatable = false)
	private int coins;
	
	@Column(name="purchase_time", nullable = false, updatable = false)
	private Timestamp purchaseTimestamp;
	
	@Column(name="claimed_time", nullable = false, updatable = true)
	private Timestamp claimedTimestamp;
	
	@Column(name="claimStatus", nullable = false, updatable = true)
	private int claimStatus;
	
	@Column(name="test", nullable = false, updatable = false)
	private int test;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHostUserID() {
		return hostUserID;
	}

	public void setHostUserID(String hostUserID) {
		this.hostUserID = hostUserID;
	}

	public String getHostPurchaseID() {
		return hostPurchaseID;
	}

	public void setHostPurchaseID(String hostPurchaseID) {
		this.hostPurchaseID = hostPurchaseID;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getChargeAction() {
		return chargeAction;
	}

	public void setChargeAction(String chargeAction) {
		this.chargeAction = chargeAction;
	}

	public String getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public Timestamp getPurchaseTimestamp() {
		return purchaseTimestamp;
	}

	public void setPurchaseTimestamp(Timestamp purchaseTimestamp) {
		this.purchaseTimestamp = purchaseTimestamp;
	}

	public Timestamp getClaimedTimestamp() {
		return claimedTimestamp;
	}

	public void setClaimedTimestamp(Timestamp claimedTimestamp) {
		this.claimedTimestamp = claimedTimestamp;
	}

	public int getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(int claimStatus) {
		this.claimStatus = claimStatus;
	}

	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}
}
