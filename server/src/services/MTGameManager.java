package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import model.CoinManager;
import model.ItemManager;
import model.S3Manager;
import model.ServiceManager;

import org.apache.log4j.Logger;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Service;

//import pages.fbpayments;


import dynamo.BinaryMahjongPlayer;
import dynamo.Challenge;
import dynamo.DKMDynamoDB;
import dynamo.LoginData;
import dynamo.PurchaseData;
import dynamo.RequestLog2;
import dynamo.UserData;


@Service
@RemotingDestination
public class MTGameManager 
{
	private static final Logger  				sLogger = Logger.getLogger(MTGameManager.class);
	
	public MTGameManager()
	{
		try
		{
			DKMDynamoDB.Initialize();
			InitializeManagerProcesses();
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to initialize statics!",ex);
		}
	}
	
	public void InitializeManagerProcesses()
	{
		try
		{
			ServiceManager.Initialize();
			CoinManager.Instance().GetProduct("");
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to initialize ServiceManager!",ex);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////
	// Player data access
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public MahjongPlayer	GetPlayerData( UserInfo updateInfo, String clientSessionID )
	{
		String          crossAppID = updateInfo.getCrossAppID();
		String 			hostID = updateInfo.getHostUserID();
		
		try
		{
			long			timeOfAction = Calendar.getInstance().getTime().getTime();
			int 			timeToService = ServiceManager.ServiceTimeSecondsToStart();
			boolean			newUserData = false;
			UserData		userData = DKMDynamoDB.LoadUserData( hostID );
			if ( userData == null )
			{
				// create new userData
				userData = new UserData();
				userData.initialize( crossAppID, hostID, "?", "?" );
				newUserData = true;
			}
			if ( UpdateUserData( userData, updateInfo ) || newUserData )
			{
				DKMDynamoDB.SaveUserData(userData);
			}
			
			BinaryMahjongPlayer playerData = DKMDynamoDB.LoadBinaryPlayerData( userData.getUserId() );

			Transaction transaction;
			MahjongPlayer	player;
			if ( playerData == null )
			{// create a new player
				player = new MahjongPlayer( hostID, userData.getUserId(), timeOfAction );
				timeOfAction++;// increment to avoid user transaction table collision with CREATE_ACCOUNT action (time is the range key in the table)
				transaction = new Transaction( player.getUserID(), clientSessionID, TransactionType.LOGIN, timeOfAction );
			}
			else
			{// load existing player data and update the ticket tree to the current time
				player = new MahjongPlayer( playerData );
				transaction = new Transaction( player.getUserID(), clientSessionID, TransactionType.LOGIN, timeOfAction );
				
				// calculates lives to gain, and update the timestamp to the next life correctly, but don't apply them to the player yet.
				// we want this as a logged transaction, so just do this as part of the login.
				int livesToGain = player.CalculateLivesToGain();
				if( livesToGain > 0)
				{
					transaction.AddChangeItemCount( ItemManager.LIFE_ITEM_INDEX, livesToGain );
				}
			}
			
			// let's check to see if we already processed this transaction before processing it again
			int previousResult = DKMDynamoDB.TransactionHasBeenSaved( transaction );
			if ( previousResult == TransactionResult.NONE )
			{// this is the first time loading the player data in this play session
				UUID sessionID = UUID.randomUUID();
				player.setCurrentSessionID( sessionID.toString() );
				
				LoginData loginData = UpdateDailyLoginCount( player );
				boolean newPlayer = ( playerData == null );
				if ( loginData != null && !newPlayer )
				{
					// use the login data to update the player puzzle unlock count.
					player.CalculatePuzzlesToUnlock( loginData, transaction );
				}
				
				if ( player.ValidTransaction( transaction ) )
				{
					player.ApplyTransaction( transaction );
				}
				
				DKMDynamoDB.SaveTransaction( transaction );			
				
				// backup once a day (when they get their daily bonus)
				boolean saveBackup = ( player.getConsecutiveDailyLogins() != 0 );
				if ( saveBackup && playerData != null )
				{
					timeOfAction++;// increment to avoid user transaction table collision (time is the range key in the table)
					playerData.time = timeOfAction;
					DKMDynamoDB.SaveBackupBinaryPlayerData( playerData );
					DKMDynamoDB.SaveTransaction( new Transaction( player.getUserID(), TransactionType.DATA_BACKUP, timeOfAction ) );
				}
				
				SavePlayerData( player );
			}
			else
			{// this is a second+ attempt to load the player data (possibly caused by bad client-server internet connectivity)
				DetermineDailyLoginCount( player );
			}
			
			player.setSecondsToServiceTime( timeToService );
			
			return player;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to GetPlayerData (" + hostID + ")!",ex);
		}
		
		return null;
	}
	
	private MahjongPlayer	GetPlayerDataByID( String userID ) throws Exception
	{
		MahjongPlayer player = null;
		
		player = DKMDynamoDB.LoadPlayerData( userID );
		if ( player == null )
		{
			sLogger.warn("Failed to GetPlayerDataByID (" + userID + ")!");
		}
		
		return player;
	}
	
	private LoginData 		UpdateDailyLoginCount( MahjongPlayer player )
	{
		try
		{
			LoginData loginData = DKMDynamoDB.LoadLoginData( player.getUserID() );
			long now = Calendar.getInstance().getTime().getTime();
			if ( loginData == null )
			{
				loginData = new LoginData( player.getUserID(), player.getCurrentSessionID(), true );
				player.setConsecutiveDailyLogins(1);
				player.setTotalLogins(1);
			}
			else
			{
				long gmtOffset = 8*60*60*1000;//offset time to make days turn at midnight pacific time
				long todayDays = (now-gmtOffset)/(24*60*60*1000);
				long lastDays = (loginData.getDailyTimestamp()-gmtOffset)/(24*60*60*1000);
				int daysSinceLastLogin = (int)(todayDays - lastDays);
				if ( daysSinceLastLogin == 1 )
				{// login was 1 day ago
					loginData.setDailySessionID( player.getCurrentSessionID() );
					loginData.setConsecutiveLoginCount(loginData.getConsecutiveLoginCount()+1);
					loginData.setDailyTimestamp(now);
					player.setConsecutiveDailyLogins(loginData.getConsecutiveLoginCount());
				}
				else if ( daysSinceLastLogin > 1 )
				{// login more than 1 day ago
					loginData.setDailySessionID( player.getCurrentSessionID() );
					loginData.setConsecutiveLoginCount(1);
					loginData.setDailyTimestamp(now);
					player.setConsecutiveDailyLogins(1);
				}
				else
				{// login during same day again
					player.setConsecutiveDailyLogins(0);
				}
				
				loginData.setLoginCount(loginData.getLoginCount()+1);
				loginData.setLastTimestamp(now);
				player.setTotalLogins(loginData.getLoginCount());
			}
			
			DKMDynamoDB.SaveLoginData( loginData );
			
			return loginData;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to UpdateDailyLoginCount (" + player.getUserID() + ")!",ex);
			return null;
		}
	}
	
	private void		DetermineDailyLoginCount( MahjongPlayer player )
	{
		// this method is used on subsequent attempts to load user data in a session.  
		// We want the data, but don't want to save anything since it's already saved during an earlier attempt to load user data.
		try
		{
			LoginData loginData = DKMDynamoDB.LoadLoginData( player.getUserID() );
			if ( loginData == null )
			{// this should never happen, but just in case
				player.setConsecutiveDailyLogins(1);
				player.setTotalLogins(1);
			}
			else
			{
				// determine if player's current login was the daily login
				if ( loginData.getDailySessionID().equals( player.getCurrentSessionID() ) )
				{ 
					player.setConsecutiveDailyLogins( loginData.getConsecutiveLoginCount() );
				}
				else
				{
					player.setConsecutiveDailyLogins( 0 );
				}
				
				player.setTotalLogins(loginData.getLoginCount());
			}
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to DetermineDailyLoginCount (" + player.getUserID() + ")!",ex);
		}
	}
	
	private boolean 	UpdateUserData( UserData userData, UserInfo updateInfo )
	{
		boolean modified = false;
		
		if ( !userData.getCrossAppID().equals(updateInfo.getCrossAppID()) 
				&& updateInfo.getCrossAppID().length() > 0 )
		{
			userData.setCrossAppID(updateInfo.getCrossAppID());
			modified = true;
		}
		
		if ( !userData.getFirstName().equals(updateInfo.getFirstName())
				&& updateInfo.getFirstName().length() > 0 )
		{
			userData.setFirstName(updateInfo.getFirstName());
			modified = true;
		}
		
		if ( !userData.getLastName().equals(updateInfo.getLastName()) 
				&& updateInfo.getLastName().length() > 0 )
		{
			userData.setLastName(updateInfo.getLastName());
			modified = true;
		}
		
		return modified;
	}
	
	private void		SavePlayerData( MahjongPlayer player ) throws Exception
	{
		DKMDynamoDB.SavePlayerData( player );
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	// Friend data access
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public List<MahjongPlayer>	GetFriendData( List<String> hostIDs )
	{
		try
		{
			if ( hostIDs.isEmpty() )
			{// return empty list if empty list is passed in
				sLogger.info("Requested Empty List in GetFriendData!");
				return new ArrayList<MahjongPlayer>();
			}
			
			List<MahjongPlayer> list = new ArrayList<MahjongPlayer>();
			for ( int i = 0; i < hostIDs.size(); i++ )
			{
				MahjongPlayer friend = GetOneFriendData( hostIDs.get(i) );
				if ( friend != null )
				{
					list.add( friend );
				}
			}
			
			return list;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to GetFriendData (" + hostIDs.size() + ")!",ex);
		}
		
		return null;
	}
	
	private MahjongPlayer	GetOneFriendData( String hostID )
	{
		try
		{
			UserData		userData = DKMDynamoDB.LoadUserData( hostID );
			MahjongPlayer	player = null;
			
			if ( userData != null )
			{
				player = DKMDynamoDB.LoadPlayerData( userData.getUserId() );
			}
			
			return player;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to GetOneFriendData (" + hostID + ")!",ex);
		}
		
		return null;
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Friend Request eligibility access
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public List<RequestLog2>	GetFriendRequestEligibility( String userID )
	{
		try
		{
			List<RequestLog2>	logList = DKMDynamoDB.LoadRequestEligibility( userID );
			return logList;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to GetFriendRequestEligibility (" + userID + ")!",ex);
		}
		
		return null;
	}
	
	@RemotingInclude
	public void	UpdateFriendRequestEligibility( String userID, List<String> hostIDs )
	{
		try
		{
			List<RequestLog2>	logList = DKMDynamoDB.LoadRequestEligibility( userID );
	
			// get current Time to mark the friends with
			long currentTimestamp = Calendar.getInstance().getTime().getTime();
			
			for ( int i = 0; i < hostIDs.size(); i++ )
			{
				RequestLog2 log = null;
				for ( int j = 0; j < logList.size(); j++ )
				{
					if ( logList.get(j).getFriendID().equals(hostIDs.get(i)) )
					{
						log = logList.get(j);
						break;
					}
				}
				
				if ( log == null )
				{
					log = new RequestLog2();
					log.setFriendID(hostIDs.get(i));
					logList.add(log);
				}
				
				log.setRequestTimestamp(currentTimestamp);
			}
			
			DKMDynamoDB.SaveRequestEligibility( userID, logList );
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to UpdateFriendRequestEligibility (" + userID + ":" + hostIDs.size() + ")!",ex);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Life Management
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public void	UpdateOnLivesTimer( String userID, int lives )
	{
		//sLogger.error("Called UpdateOnEnergyTimer()");
		try
		{
			MahjongPlayer player = GetPlayerDataByID( userID );
			if ( player == null )
			{
				return;
			}
			
			int livesAdjustment = lives - player.livesCount();
			
			// perform a transaction to get us to the number of lives the client says we should have.
			if( livesAdjustment != 0 )
			{
				player.UpdateLivesTimestamp();
				
				Transaction transaction = new Transaction( player.getUserID(), TransactionType.LIFE_GAIN, Calendar.getInstance().getTime().getTime() );
				transaction.AddChangeItemCount( ItemManager.LIFE_ITEM_INDEX, livesAdjustment );
				if ( player.ValidTransaction( transaction ) )
				{
					if ( player.ApplyTransaction( transaction ) )
					{
						SavePlayerData( player );
						DKMDynamoDB.SaveTransaction( transaction );
					}
				}				
			}

		}
		catch (Exception ex)
		{
			sLogger.error("Failed to UpdateOnLivesTimer (" + userID + ":" + lives + ")!",ex);
		}
	}
	
	@RemotingInclude
	public void UpdateLivesTimestamp( String userID )
	{
		try
		{
			MahjongPlayer player = GetPlayerDataByID( userID );
			player.UpdateLivesTimestamp();
		}
		catch (Exception ex)
		{
			sLogger.error("Failed to UpdateLivesTimestamp (" + userID + ")!",ex);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Coin management
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public ClaimCoinsResult ClaimCoins( String playerHostId )
	{
		try
		{
			ClaimCoinsResult result = new ClaimCoinsResult();
			List<PurchaseData> purchases = DKMDynamoDB.LoadAllUnclaimedPurchaseData( playerHostId );
			
			MahjongPlayer player = GetOneFriendData( playerHostId );
			if ( player == null ) return result;
			
			int coinsClaimed = 0;
			
			if ( !purchases.isEmpty() )
			{
				long timeOfAction = Calendar.getInstance().getTime().getTime();
				Transaction transaction = new Transaction( player.getUserID(), TransactionType.COINS_CLAIMED, timeOfAction );

				for ( int i = 0; i < purchases.size(); i++ )
				{
					PurchaseData purchaseLog = purchases.get(i);
					if ( purchaseLog.getClaimStatus() == PurchaseData.UNCLAIMED_STATUS )
					{
						coinsClaimed += purchaseLog.getCoins();
						transaction.AddChangeItemCount( ItemManager.COIN_ITEM_INDEX, purchaseLog.getCoins() );
						purchaseLog.setClaimStatus(PurchaseData.CLAIMED_STATUS);
						purchaseLog.setClaimedTimestamp( timeOfAction );
						DKMDynamoDB.SavePurchaseData( purchaseLog );
					}
				}
				
				if ( coinsClaimed > 0 )
				{
					if ( player.ValidTransaction( transaction ) )
					{
						if ( player.ApplyTransaction( transaction ) )
						{
							SavePlayerData( player );
						}
					}
				}
				
				DKMDynamoDB.ReleaseUnclaimedPurchaseData( purchases );
				DKMDynamoDB.SaveTransaction( transaction );
			}
			
			result.setCoinsClaimed(coinsClaimed);
			return result;
			//return player.GetCoinCount();// we used to return the balance, but now that we allow delayed transaction processing in a queue, we need to use a delta
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to ClaimCoins (" + playerHostId + ")!",ex);
		}
		
		return new ClaimCoinsResult();
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Item transaction management
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public TransactionResult TransactionUpdate( String sessionID, Transaction transaction )
	{
		try
		{
			String				playerID = transaction.getPlayerID();
			int 				timeToService = ServiceManager.ServiceTimeSecondsToStart();
			long 				transactionTime = Calendar.getInstance().getTime().getTime();
			TransactionResult	result;
			
			// get the player data for the transaction
			MahjongPlayer 		player = GetPlayerDataByID( playerID );
			if ( player == null )
			{
				result = new TransactionResult( transaction.getID() );
				result.setSecondsToServiceTime( timeToService );
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_PLAYER);
				result.setInfo("PlayerID NOT VALID!");
				return result;
			}
			
			if ( !sessionID.equals( player.getCurrentSessionID() ) )
			{
				result = new TransactionResult( transaction.getID() );
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_SESSION );
				result.setInfo( "Only one active session allowed!" );
				return result;
			}
			
			transaction.setTimestamp( transactionTime );
			result = ApplyTransaction( transaction, player, timeToService );
			
			if ( result.getResult() == TransactionResult.SUCCESS )
			{
				SavePlayerData( player );
			}
			else
			if ( result.getResult() == ( TransactionResult.SUCCESS + TransactionResult.NO_SAVE ) )
			{// remove the no save flag from the result the client will see
				result.setResult( TransactionResult.SUCCESS );
			}
			
			DKMDynamoDB.SaveTransaction( transaction );
			
			return result;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed TransactionUpdate!",ex);
		}

		TransactionResult	result = new TransactionResult( transaction.getID(), TransactionResult.PROCESS_EXCEPTION );
		return result;
	}
	
	@RemotingInclude
	public List<TransactionResult> MultipleTransactionUpdate( String sessionID, List<Transaction> transactions )
	{
		List<TransactionResult> results = new ArrayList<TransactionResult>();
		TransactionResult		result;
		
		try
		{
			if ( transactions.size() < 1 ) return results;
			
			// get the player data for the transactions
			String			playerID = transactions.get(0).getPlayerID();
			MahjongPlayer	player = GetPlayerDataByID( playerID );
			if ( player == null )
			{
				result = new TransactionResult( transactions.get(0).getID() );
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_PLAYER );
				result.setInfo( "PlayerID NOT VALID!" );
				results.add( result );
				return results;
			}
			
			if ( !sessionID.equals( player.getCurrentSessionID() ) )
			{
				result = new TransactionResult( transactions.get(0).getID() );
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_SESSION );
				result.setInfo( "Only one active session allowed!" );
				results.add( result );
				return results;
			}
				
			int 			timeToService = ServiceManager.ServiceTimeSecondsToStart();
			long			transactionTime = Calendar.getInstance().getTime().getTime();
			
			boolean savePlayerData = false;
			for ( int i = 0; i < transactions.size(); ++i )
			{
				Transaction transaction = transactions.get(i);
				result = ApplyTransaction( transaction, player, timeToService );
				if ( result.getResult() == TransactionResult.SUCCESS )
				{
					savePlayerData = true;
				}
				else
				if ( result.getResult() == ( TransactionResult.SUCCESS + TransactionResult.NO_SAVE ) )
				{// remove the no save flag from the result the client will see
					result.setResult( TransactionResult.SUCCESS );
				}
				results.add( result );
			}
			
			// after knowing we have safely applied the transactions we will save player and transaction data...
			if ( savePlayerData )
			{
				SavePlayerData( player );
			}
			
			for ( int i = 0; i < transactions.size(); ++i )
			{
				Transaction transaction = transactions.get(i);
				transaction.setTimestamp( transactionTime );
				++transactionTime;// keep transaction from colliding in the user txn table (time is range key)
				DKMDynamoDB.SaveTransaction( transaction );
			}

			
			return results;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed MultiTransactionUpdate!",ex);
		}
		
		result = new TransactionResult( "MultipleTXN", TransactionResult.PROCESS_EXCEPTION );
		results.add( 0, result );

		return results;
	}
	
	private TransactionResult ApplyTransaction( Transaction transaction, MahjongPlayer player, int timeToService )
	{
		try
		{
			TransactionResult	result = new TransactionResult( transaction.getID() );
			result.setSecondsToServiceTime( timeToService );
			
			// TODO: if desired, we could validate the transaction using the marketID in the transaction 
			// to see if the item changes match the ID provided, 
			// or we could load the transaction data based on the marketID instead of accepting transaction data from the client
			// we would also want to validate that nothing is being transacted without coins if it is supposed to be available only with coins
			
			if ( transaction.getAttemptCount() > 1 )
			{// let's check to see if we already processed this transaction before processing it again
				int previousResult = DKMDynamoDB.TransactionHasBeenSaved( transaction );
				if ( previousResult != TransactionResult.NONE )
				{
					result.setResult( previousResult + TransactionResult.PREVIOUSLY_COMPLETED );
					return result;
				}
			}
			
			// make sure we have a valid transaction data set
			if ( player.ValidTransaction( transaction ) )
			{
				if ( !player.ApplyTransaction( transaction ) )
				{
					result.setResult( TransactionResult.NO_SAVE + result.getResult() );
				}
			}
			else
			{
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_TRANSACTION );
				result.setInfo( "Invalid Transaction" );
			}

			// log the transaction
			transaction.setResultCode( result.getResult() );
			
			return result;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed ApplyTransaction!",ex);
		}

		TransactionResult	result = new TransactionResult( transaction.getID(), TransactionResult.PROCESS_EXCEPTION );
		return result;
	}
	
	@RemotingInclude
	public void LogTransaction( Transaction transaction )
	{// for use by the client to log user actions that don't affect the user state
		try
		{
			// log the transaction
			long transactionTime = Calendar.getInstance().getTime().getTime();
			transaction.setTimestamp( transactionTime );
			DKMDynamoDB.SaveTransaction( transaction );
		}
		catch( Exception ex )
		{
			sLogger.error("Failed LogTransaction!",ex);
		}
	}
	

	///////////////////////////////////////////////////////////////////////////////
	// Challenge management
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public void IssueChallenge( Challenge challenge )
	{
		DKMDynamoDB.SaveChallenge(challenge, DKMDynamoDB.CHALLENGE_TABLE);
		DKMDynamoDB.AddChallengeToMapTable(challenge);
	}
	
	@RemotingInclude
	public void UpdateChallenge( Challenge challenge )
	{
		if(challenge.status == Challenge.NEW || challenge.status == Challenge.VIEWING_RESULTS)
		{
			DKMDynamoDB.SaveChallenge(challenge, DKMDynamoDB.CHALLENGE_TABLE);
		}
		else if(challenge.status == Challenge.COMPLETE)
		{
			DKMDynamoDB.MoveChallengeToCompleteTable(challenge);
		}
	}
	
	@RemotingInclude
	public List<Challenge> GetChallengesForUser( String userID )
	{
		return DKMDynamoDB.ChallengesForUser(userID);
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Maintenance polling
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public int		GetTimeToService()
	{
		try
		{
			int 			timeToService = ServiceManager.ServiceTimeSecondsToStart();
			return timeToService;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to GetTimeToService!",ex);
		}
		
		return 0x7fffffff;// return a time way-way in the future, so we can ignore it
	}
	
	// the following methods provide access to S3 file sharing
	
		@RemotingInclude
		public void SaveAsTextFile( String userID, String filename, String text )
		{
			//sLogger.error("Called SaveAsTextFile()");
			S3Manager s3 = new S3Manager();
			s3.SaveAsTextFile( userID, filename, text );
		}
		
		@RemotingInclude
		public String GetFileAsText( String userID, String filename )
		{
			//sLogger.error("Called GetFileAsText()");
			S3Manager s3 = new S3Manager();
			String text = s3.GetFileAsText( userID, filename );
			return text;
		}
		
		@RemotingInclude
		public String GetFileListing( String userID )
		{
			//sLogger.error("Called GetFileListing()");
			S3Manager s3 = new S3Manager();
			String listing = s3.GetFileListing( userID );
			return listing;
		}
}