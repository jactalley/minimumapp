package services;

import model.ItemDescription;

public class OwnedItem 
{
	private MahjongPlayer player;
	private int itemType;
	private int itemID;
	private int itemData;
	private int count;
	private int requestCount;
	private int gifterID;
	private int itemIndex;
	private int countDelta;// used to track total count change from rewards/gifts/used/etc

	public OwnedItem()
	{
		itemIndex = -1;
		countDelta = 0;
	}

	public OwnedItem( MahjongPlayer player, ItemDescription description, int count )
	{
		this.player = player;
		this.itemType = description.itemType;
		this.itemID = description.itemID;
		this.itemData = description.itemData;
		this.count = count;
		this.requestCount = 0;
		this.gifterID = -1;
		this.itemIndex = description.itemIndex;
		this.countDelta = 0;
	}

	public MahjongPlayer getPlayer() {
		return player;
	}

	public void setPlayer(MahjongPlayer player) {
		this.player = player;
	}

	public int getItemType() {
		return itemType;
	}

	public void setItemType(int itemType) {
		this.itemType = itemType;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public int getItemData() {
		return itemData;
	}

	public void setItemData(int itemData) {
		this.itemData = itemData;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		count = Math.max(0, count);
		countDelta += count - this.count;
		this.count = count;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	public int getGifterID() {
		return gifterID;
	}

	public void setGifterID(int gifterID) {
		this.gifterID = gifterID;
	}

	public int getItemIndex() {
		return itemIndex;
	}

	public void setItemIndex(int itemIndex) {
		this.itemIndex = itemIndex;
	}
	
	public int getCountDelta() {
		return countDelta;
	}
		
}
