package services;

public class ClaimCoinsResult 
{
	private int 	coinsClaimed = 0;
	
	public int getCoinsClaimed() {
		return coinsClaimed;
	}

	public void setCoinsClaimed(int value) {
		this.coinsClaimed = value;
	}

}
