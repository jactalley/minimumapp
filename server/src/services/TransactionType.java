package services;

public class TransactionType 
{
	public static final String	SUPPORT 					 = "SUPPORT";
	public static final String	DATA_BACKUP 				= "DATA_BACKUP";
	public static final String	DATA_RESTORE				= "DATA_RESTORE";
	public static final String	CREATE_ACCOUNT 				= "CREATE_ACCOUNT";
	public static final String	LOGIN 						= "LOGIN";
	public static final String	COIN_PURCHASE 				= "COIN_PURCHASE";
	public static final String	POWER_USED 					= "POWER_USED";
	public static final String	COINS_CLAIMED 				= "COINS_CLAIMED";
	public static final String	MARK_ITEM_IN_USE			= "MARK_ITEM_IN_USE";
	public static final String	USE_ITEM					= "USE_ITEM";
	public static final String	QUIT_GAME 					= "QUIT_GAME";
	public static final String  LOG_PUZZLE					= "LOG_PUZZLE";
	public static final String  START_PUZZLE				= "START_PUZZLE";
	public static final String  LIFE_GAIN					= "LIFE_GAIN";
}
