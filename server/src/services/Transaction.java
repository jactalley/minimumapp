package services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Transaction
{
	public static final int ITEM_IN_USE_TAG = 1111111111;// a tag value big enough it will never be used as a delta count, and recognizable in visual inspections
	
	long					timestamp;
	private String 			playerID;
	private String 			ID;
	private String 			action;
	private int				marketID;
	private int				resultCode;
	private int				attemptCount;
	private List<Integer>	data = new ArrayList<Integer>();

	public Transaction()
	{
	}
	
	public Transaction( String playerID, String action, long timestamp )
	{
		UUID id = UUID.randomUUID();
		this.timestamp = timestamp;
		this.playerID = playerID;
		this.ID = id.toString();
		this.action = action;
		this.marketID = 0;
		this.resultCode = TransactionResult.SUCCESS;
	}

	public Transaction( String playerID, String id, String action, long timestamp )
	{
		this.timestamp = timestamp;
		this.playerID = playerID;
		this.ID = id;
		this.action = action;
		this.marketID = 0;
		this.resultCode = TransactionResult.SUCCESS;
	}

	public Transaction( String playerID, String id, String action, int marketID, List<Integer>	data, int resultCode, long timestamp )
	{
		this.timestamp = timestamp;
		this.playerID = playerID;
		this.ID = id == null ? UUID.randomUUID().toString() : id;
		this.action = action;
		this.marketID = marketID;
		this.resultCode = resultCode;
		if ( data != null )
		{
			this.data = data;
		}
	}
	
	public void AddChangeItemCount( int itemID, int deltaCount )
	{
		data.add( itemID );
		data.add( deltaCount );
	}
	
	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getID() {
		return ID;
	}

	public void setID(String id) {
		ID = id;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public int getMarketID() {
		return marketID;
	}

	public void setMarketID(int marketID) {
		this.marketID = marketID;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public List<Integer> getData() {
		return data;
	}

	public void setData(List<Integer> data) {
		this.data = data;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public int getAttemptCount() {
		return attemptCount;
	}

	public void setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
	}
}
