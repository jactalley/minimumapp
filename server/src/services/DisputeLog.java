package services;

import static javax.persistence.GenerationType.IDENTITY;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="dispute_log",
		uniqueConstraints = {@UniqueConstraint(columnNames={"user_id", "purchase_id", "host_time_created"})})
@NamedQueries({
	@NamedQuery(name=DisputeLog.FIND_BY_USER_ID, query="from DisputeLog o where o.hostUserID = :userID"),
	@NamedQuery(name=DisputeLog.FIND_BY_PURCHASE_ID, query="from DisputeLog o where o.hostPurchaseID = :purchaseID")
})
public class DisputeLog
{
	public static final String	FIND_BY_PURCHASE_ID = "DisputeLog.FindByPurchaseID";
	public static final String	FIND_BY_USER_ID = "DisputeLog.FindByUserID";
	public static final String	PURCHASE_ID_PARM = "purchaseID";
	public static final String	USER_ID_PARM = "userID";
	
	public static final int	UNKNOWN_STATUS = 0;
	public static final int	NEW_STATUS = 1;
	public static final int	OPEN_STATUS = 2;
	public static final int	WAITING_FOR_USER_STATUS = 3;
	public static final int	WAITING_FOR_HOST_STATUS = 4;// ie. waiting for facebook to reply about something (email or message or something else "manual")
	public static final int	RESOLVED_STATUS = 5;
	public static final int	CLOSED_STATUS = 6;
	

	@Id
	@GeneratedValue( strategy = IDENTITY )
	@Column(name="id")
	private int id;
	
	@org.hibernate.annotations.Index(name = "hostUserIDIndex")
	@Column(name = "user_id", nullable = false, updatable = false, length = 32)
	private String hostUserID;	
	
	@org.hibernate.annotations.Index(name = "hostPurchaseIDIndex")
	@Column(name = "purchase_id", nullable = false, updatable = false, length = 32)
	private String hostPurchaseID;	
	
	@Column(name="email", nullable = false, length = 255)
	private String email;
	
	@Column(name="user_comment", nullable = false, updatable = false, length = 1023)
	private String userComment;
	
	@Column(name="support_comment", nullable = false, updatable = true, length = 1023)
	private String supportComment;
	
	@Column(name="status", nullable = false, updatable = true)
	private int status;
	
	@Column(name = "host_time_created", nullable = false, updatable = false, length = 32)
	private String hostTimeCreated;	
	
	@Column(name="dispute_time", nullable = false, updatable = false)
	private Timestamp disputeTimestamp;
	
	@Column(name="updated_time", nullable = false, updatable = true)
	private Timestamp updatedTimestamp;
	
	@Column(name="test", nullable = false, updatable = false)
	private int test;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHostUserID() {
		return hostUserID;
	}

	public void setHostUserID(String hostUserID) {
		this.hostUserID = hostUserID;
	}

	public String getHostPurchaseID() {
		return hostPurchaseID;
	}

	public void setHostPurchaseID(String hostPurchaseID) {
		this.hostPurchaseID = hostPurchaseID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getSupportComment() {
		return supportComment;
	}

	public void setSupportComment(String supportComment) {
		this.supportComment = supportComment;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getHostTimeCreated() {
		return hostTimeCreated;
	}

	public void setHostTimeCreated(String hostTimeCreated) {
		this.hostTimeCreated = hostTimeCreated;
	}

	public Timestamp getDisputeTimestamp() {
		return disputeTimestamp;
	}

	public void setDisputeTimestamp(Timestamp disputeTimestamp) {
		this.disputeTimestamp = disputeTimestamp;
	}

	public Timestamp getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}

}
