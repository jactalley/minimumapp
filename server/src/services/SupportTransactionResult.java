package services;

// support transactions need to return the player data in addition to the standard transaction result, so that panels can update themselves.
public class SupportTransactionResult
{
	private TransactionResult mTransactionResult;
	private MahjongPlayer     mPlayerData;
	
	public SupportTransactionResult( TransactionResult result, MahjongPlayer playerData )
	{
		mTransactionResult = result;
		mPlayerData = playerData;
	}
	
	public void setTransactionResult( TransactionResult result )
	{
		mTransactionResult = result;
	}
	
	public TransactionResult getTransactionResult()
	{
		return mTransactionResult;
	}
	
	public void setPlayerData( MahjongPlayer playerData )
	{
		mPlayerData = playerData;
	}
	
	public MahjongPlayer getPlayerData()
	{
		return mPlayerData;
	}
}
