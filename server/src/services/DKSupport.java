package services;

//import java.sql.Timestamp;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

//import model.CollectionTableManager;
//import model.DailyBonusManager;
import model.ItemManager;
import model.ServiceManager;
//import model.ServiceManager;
//import model.LevelAccessManager;
//import model.MarketManager;
//import model.RewardManager;
//import model.TileCollectionTableManager;
//import model.TileSetManager;

import org.apache.log4j.Logger;
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
//import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;

import app.FBID;

import dynamo.DKMDynamoDB;
import dynamo.DisputeData;
//import dynamo.LoginData;// TODO:need to add view of player login data to support tool
import dynamo.PurchaseData;
import dynamo.ServiceTimeData;
import dynamo.SupportUserData;
import dynamo.UserData;

@Service
@RemotingDestination
public class DKSupport 
{
	private static final Logger  				sLogger = Logger.getLogger(DKSupport.class);
	
	private HashMap<String,SupportUserData>	sActiveUsers = new HashMap<String,SupportUserData>();

	/////////////////////////////////////////////////////////////////////
	
	public DKSupport()
	{
		try
		{
			DKMDynamoDB.Initialize();
			initializeSupportUser();
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to initialize DKSupport statics!",ex);
		}
	}

	/////////////////////////////////////////////////////////////////////
	
	public void initializeSupportUser()
	{
		SupportUserData user;
		try 
		{
			user = DKMDynamoDB.LoadSupportUserData( SupportUserData.DEFAULT_ADMIN_ID );
		
			if ( user == null )
			{
				user = new SupportUserData();
				user.CreateDefaultAdmin();
				DKMDynamoDB.SaveSupportUserData(user);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public String	Login( String userID, String password )
	{
		try 
		{
			CheckUserTimeout();
			
			// user is not active, load them in
			SupportUserData user = DKMDynamoDB.LoadSupportUserData( userID );
			if ( user != null )
			{
				if ( user.CheckPassword(password) )
				{
					user.UpdateActivityTime();
					user.CreateSessionKey();
					sActiveUsers.put(user.getSessionKey(), user);
					return user.getSessionKey();
				}
				else
				{
					user = null;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "";
	}

	/////////////////////////////////////////////////////////////////////
	
	private boolean	SessionIsActive( String sessionKey )
	{
		SupportUserData user = sActiveUsers.get(sessionKey);
		if ( user != null )
		{
			long inactiveTime = Calendar.getInstance().getTime().getTime() - user.getActivityTime();
			if ( inactiveTime <= 1800000 )// 1800000 == 30min*60sec*1000millis - testing for 30 minute timeout
			{
				user.UpdateActivityTime();
				return true;
			}
			else
			{
				sActiveUsers.remove(sessionKey);
			}
		}
		
		return false;
	}

	/////////////////////////////////////////////////////////////////////
		
	private void CheckUserTimeout()
	{
		Iterator<SupportUserData> it = sActiveUsers.values().iterator();
		
		long currentTime = Calendar.getInstance().getTime().getTime();
		
		while ( it.hasNext() )
		{
			SupportUserData user = it.next();
			long inactiveTime = currentTime - user.getActivityTime();
			if ( inactiveTime > 1800000 )// 1800000 == 30min*60sec*1000millis - testing for 30 minute timeout
			{
				it.remove();
			}
		}
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int	CreateUser( String sessionKey, String userID, String password, int authority )
	{
		try
		{
			if ( !SessionIsActive(sessionKey) )
			{
				return -1;
			}
			
			SupportUserData creatingUser = sActiveUsers.get(sessionKey);
			if ( creatingUser.getAuthority() == SupportUserData.ADMIN_AUTHORITY )
			{
				SupportUserData user = new SupportUserData();
				user.CreateUser(userID, password, authority);
				DKMDynamoDB.SaveSupportUserData(user);
				return 1;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public UserData	GetUserDataByID( String sessionKey, String hostID )
	{
		try
		{
			if ( !SessionIsActive(sessionKey) )
			{
				return null;
			}
			
			UserData		userData = DKMDynamoDB.LoadUserData( hostID );
	
			return userData;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public List<UserData>	GetUserDataByName( String sessionKey, String firstName, String lastName )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		List<UserData> newUsers = DKMDynamoDB.LoadUserDataByName( lastName );
		List<UserData> list = new ArrayList<UserData>();

		for ( int i = 0; i < newUsers.size(); i++ )
		{
			if ( newUsers.get(i).getFirstName().equals( firstName ) )
			{
				list.add( newUsers.get(i) );
			}
		}
		
		return list;
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public MahjongPlayer	GetPlayerData( String sessionKey, String hostID )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		MahjongPlayer player = null;

		try
		{
			UserData	userData = GetUserDataByID( sessionKey, hostID );
			if ( userData != null )
			{
				player = DKMDynamoDB.LoadPlayerData( userData.getUserId() );
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return player;
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public List<Transaction>	GetTransactionDataForTimeRange( String sessionKey, String playerID, long startTime, long endTime )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		List<Transaction> list = DKMDynamoDB.LoadTransactionsForTimeRange( playerID, startTime, endTime );
		
		return list;
	}
	
	@RemotingInclude
	public List<Transaction>	GetTransactionData( String sessionKey, String playerID )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		List<Transaction> list = DKMDynamoDB.LoadTransactions(playerID);

		return list;
	}
	

	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public List<PurchaseData>	GetPurchaseData( String sessionKey, String hostID )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		// get data from dynamo
		List<PurchaseData> list = DKMDynamoDB.LoadAllPurchaseData( hostID );
		
		return list;
	}
	

	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public List<DisputeData>	GetAllDisputeData( String sessionKey )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		List<DisputeData> list = DKMDynamoDB.LoadAllDisputeData();
		return list;
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public List<DisputeData>	GetNewDisputeData( String sessionKey )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		List<DisputeData> list = DKMDynamoDB.LoadAllNewDisputeData();
		return list;
	}

	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public int			UpdateDisputeData( String sessionKey, DisputeData dispute )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		try
		{
			DKMDynamoDB.SaveDisputeData( dispute );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		
		return 0;
	}
	
	private MahjongPlayer	GetPlayerDataByID( String userID ) throws Exception
	{
		MahjongPlayer player = null;
		
		player = DKMDynamoDB.LoadPlayerData( userID );
		if ( player == null )
		{
			sLogger.warn("Failed to GetPlayerDataByID (" + userID + ")!");
		}
		
		return player;
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Scores Management
	///////////////////////////////////////////////////////////////////////////////
	@RemotingInclude
	public MahjongPlayer RemoveScores( String sessionKey, String userID, int lowestPuzzleIDToRemove ) throws Exception
	{
		// first, do a backup
		BackupUserData( sessionKey, userID );
		
		
		MahjongPlayer player = DKMDynamoDB.LoadPlayerData( userID );
		
		// scan for all scores to remove, then tell the set to remove them all.
		List<HighScore> scoresToRemove = new ArrayList<HighScore>();
		for( HighScore hs : player.getHighScores() )
		{
			if( hs.getPuzzle() >= lowestPuzzleIDToRemove )
			{
				scoresToRemove.add( hs );
			}
		}
		player.getHighScores().removeAll( scoresToRemove );
		
		DKMDynamoDB.SavePlayerData( player );

		// log a transaction saying SCORES_REMOVED (and indicate which ones were removed in the data).
		Transaction transaction = new Transaction( player.getUserID(), TransactionType.SUPPORT, Calendar.getInstance().getTime().getTime() );
		transaction.AddChangeItemCount( ItemManager.REMOVE_SCORES_INDEX, lowestPuzzleIDToRemove );
		DKMDynamoDB.SaveTransaction( transaction );
		
		return player;
	}
	
    @RemotingInclude
    public MahjongPlayer AdjustLastPuzzleUnlocked( String sessionKey, String userID, int adjustBy ) throws Exception
    {
        // first, do a backup
        BackupUserData( sessionKey, userID );
        
        MahjongPlayer player = DKMDynamoDB.LoadPlayerData( userID );
        player.setLastPuzzleUnlocked( player.getLastPuzzleUnlocked() + adjustBy );
        player.setPuzzleUnlockTimestamp( Calendar.getInstance().getTime().getTime() );
        DKMDynamoDB.SavePlayerData( player );

        // log a transaction saying UNLOCK_PUZZLES (and indicate the delta).
        Transaction transaction = new Transaction( player.getUserID(), TransactionType.SUPPORT, Calendar.getInstance().getTime().getTime() );
        transaction.AddChangeItemCount( ItemManager.UNLOCK_PUZZLES, adjustBy );
        DKMDynamoDB.SaveTransaction( transaction );
        
        return player;
    }

	
	///////////////////////////////////////////////////////////////////////////////
	// Item transaction management
	///////////////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public SupportTransactionResult TransactionUpdate( Transaction transaction )
	{
		try
		{
			String				playerID = transaction.getPlayerID();
			int 				timeToService = ServiceManager.ServiceTimeSecondsToStart();
			long 				transactionTime = Calendar.getInstance().getTime().getTime();
			TransactionResult	result;
			
			// get the player data for the transaction
			MahjongPlayer 		player = GetPlayerDataByID( playerID );
			if ( player == null )
			{
				result = new TransactionResult( transaction.getID() );
				result.setSecondsToServiceTime( timeToService );
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_PLAYER);
				result.setInfo("PlayerID NOT VALID!");
				return new SupportTransactionResult( result, player );
			}
						
			transaction.setTimestamp( transactionTime );
			result = ApplyTransaction( transaction, player, timeToService );
			
			if ( result.getResult() == TransactionResult.SUCCESS )
			{
				SavePlayerData( player );
			}
			else
			if ( result.getResult() == ( TransactionResult.SUCCESS + TransactionResult.NO_SAVE ) )
			{// remove the no save flag from the result the client will see
				result.setResult( TransactionResult.SUCCESS );
			}
			
			DKMDynamoDB.SaveTransaction( transaction );
			
			player = GetPlayerDataByID( playerID );
			return new SupportTransactionResult( result, player );
		}
		catch( Exception ex )
		{
			sLogger.error("Failed TransactionUpdate!",ex);
		}

		TransactionResult	result = new TransactionResult( transaction.getID(), TransactionResult.PROCESS_EXCEPTION );
		return new SupportTransactionResult( result, null );
	}
	
	private TransactionResult ApplyTransaction( Transaction transaction, MahjongPlayer player, int timeToService )
	{
		try
		{
			TransactionResult	result = new TransactionResult( transaction.getID() );
			result.setSecondsToServiceTime( timeToService );
			
			// TODO: if desired, we could validate the transaction using the marketID in the transaction 
			// to see if the item changes match the ID provided, 
			// or we could load the transaction data based on the marketID instead of accepting transaction data from the client
			// we would also want to validate that nothing is being transacted without coins if it is supposed to be available only with coins
			
			if ( transaction.getAttemptCount() > 1 )
			{// let's check to see if we already processed this transaction before processing it again
				int previousResult = DKMDynamoDB.TransactionHasBeenSaved( transaction );
				if ( previousResult != TransactionResult.NONE )
				{
					result.setResult( previousResult + TransactionResult.PREVIOUSLY_COMPLETED );
					return result;
				}
			}
			
			// make sure we have a valid transaction data set
			if ( player.ValidTransaction( transaction ) )
			{
				if ( !player.ApplyTransaction( transaction ) )
				{
					result.setResult( TransactionResult.NO_SAVE + result.getResult() );
				}
			}
			else
			{
				result.setResult( TransactionResult.FAIL + TransactionResult.INVALID_TRANSACTION );
				result.setInfo( "Invalid Transaction" );
			}

			// log the transaction
			transaction.setResultCode( result.getResult() );
			
			return result;
		}
		catch( Exception ex )
		{
			sLogger.error("Failed ApplyTransaction!",ex);
		}

		TransactionResult	result = new TransactionResult( transaction.getID(), TransactionResult.PROCESS_EXCEPTION );
		return result;
	}
	
	private void		SavePlayerData( MahjongPlayer player ) throws Exception
	{
		DKMDynamoDB.SavePlayerData( player );
	}

	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public String	GetFacebookPurchaseData( String sessionKey, String purchaseID )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		try
		{
			// url format... https://graph.facebook.com/payment_id?access_token=APP_ACCESS_TOKEN
			String url = "https://graph.facebook.com/" + purchaseID + "?access_token=" + FBID.APP_ACCESS_TOKEN + "&fields=id,user,application,actions,refundable_amount,items,country,created_time,test,fraud_status,payout_foreign_exchange_rate,disputes";
			 
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	 
			int responseCode = con.getResponseCode();
			
			if ( responseCode <= 199 || responseCode >= 300 )
			{
				// we expect to get a response of 2XX or we need to log the problem
				sLogger.error("DKSupport: Bad response code (" + responseCode + ") accessing fb payment object id: " + purchaseID);
				return null;
			}
	 
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			String		responseString = response.toString();
			
			return responseString;
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			sLogger.error( "DKSupport:(Ex)Problem accessing fb payment object: " + exception.getMessage(), exception );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			sLogger.error("DKSupport:(Th)Problem accessing fb payment object: " + thr.getMessage(), thr);
		}
		
		return null;
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public String	RequestFacebookPurchaseRefund( String sessionKey, String userID, String purchaseID, long disputeTime, float amount, String supportComment )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		try
		{
			String currency = "USD";
			PurchaseData purchaseData = DKMDynamoDB.LoadPurchaseData( userID, purchaseID );
			if ( purchaseData != null )
			{
				currency = purchaseData.getCurrency();
			}
			
			String amountString = Float.toString(amount);
			// url format... https://graph.facebook.com/payment_id/refunds?access_token=APP_ACCESS_TOKEN&method=POST&currency=3_LETTER_CODE&amount=AMOUNT
			String url = "https://graph.facebook.com/" + purchaseID + "/refunds";
			String urlParameters = "access_token=" + FBID.APP_ACCESS_TOKEN + "&currency=" + currency + "&amount=" + amountString;//&method=POST
			 
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			
			if ( responseCode <= 199 || responseCode >= 300 )
			{
				// we expect to get a response of 2XX or we need to log the problem
				sLogger.error("DKSupport: Bad response code (" + responseCode + ") refunding fb payment for user id: " + userID + " , and payment ID:" + purchaseID);
				return null;
			}
	 
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			String		responseString = response.toString();
			
			if ( responseString.equals( "true" ) )
			{
				DisputeData disputeData = DKMDynamoDB.LoadDisputeData( purchaseID, disputeTime);
				if ( disputeData != null )
				{
					disputeData.setStatus(DisputeData.REFUNDED_STATUS);
					disputeData.setSupportComment(supportComment);
					disputeData.setUpdatedTimestamp(Calendar.getInstance().getTime().getTime());
					DKMDynamoDB.SaveDisputeData(disputeData);
				}
			}
			
			return responseString;
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			sLogger.error( "DKSupport:(Ex)Problem accessing fb payment object: " + exception.getMessage(), exception );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			sLogger.error("DKSupport:(Th)Problem accessing fb payment object: " + thr.getMessage(), thr);
		}
		
		return null;
	}	

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public String	ReplaceFacebookPurchaseCoins( String sessionKey, String hostID, String purchaseID, long disputeTime, int coins, String supportComment )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return null;
		}
		
		//https://graph.facebook.com/payment_id/dispute?access_token=APP_ACCESS_TOKEN&method=POST&reason=GRANTED_REPLACEMENT_ITEM
		
		try
		{
			MahjongPlayer player = GetPlayerData( sessionKey, hostID );
			Transaction transaction = new Transaction( player.getUserID(), TransactionType.SUPPORT, Calendar.getInstance().getTime().getTime() );
			transaction.AddChangeItemCount( ItemManager.COIN_ITEM_INDEX, coins );
			if ( player.ValidTransaction( transaction ) )
			{
				if ( player.ApplyTransaction( transaction ) )
				{
					DKMDynamoDB.SavePlayerData( player );
					DKMDynamoDB.SaveTransaction( transaction );
				}
			}
			else
			{
				return "invalid";
			}

			// url format... https://graph.facebook.com/payment_id/dispute?access_token=APP_ACCESS_TOKEN&method=POST&reason=GRANTED_REPLACEMENT_ITEM
			String url = "https://graph.facebook.com/" + purchaseID + "/dispute";
			String urlParameters = "access_token=" + FBID.APP_ACCESS_TOKEN + "&reason=GRANTED_REPLACEMENT_ITEM";//&method=POST
			 
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			
			if ( responseCode <= 199 || responseCode >= 300 )
			{
				// we expect to get a response of 2XX or we need to log the problem
				sLogger.error("DKSupport: Bad response code (" + responseCode + ") updating fb payment for user id: " + hostID + " , and payment ID:" + purchaseID);
				return null;
			}
	 
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			String		responseString = response.toString();
			
			if ( responseString.equals( "true" ) )
			{
				DisputeData disputeData = DKMDynamoDB.LoadDisputeData( purchaseID, disputeTime );
				if ( disputeData != null )
				{
					disputeData.setStatus(DisputeData.REPLACED_STATUS);
					disputeData.setSupportComment(supportComment);
					disputeData.setUpdatedTimestamp(Calendar.getInstance().getTime().getTime());
					DKMDynamoDB.SaveDisputeData(disputeData);
				}
			}
			
			return responseString;
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			sLogger.error( "DKSupport:(Ex)Problem accessing fb payment object: " + exception.getMessage(), exception );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			sLogger.error("DKSupport:(Th)Problem accessing fb payment object: " + thr.getMessage(), thr);
		}
		
		return null;
	}	

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int	MarkPurchaseDisputeProcessed( String sessionKey, String purchaseID, long disputeTime, int status, String supportComment )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		try
		{
			DisputeData disputeData = DKMDynamoDB.LoadDisputeData( purchaseID, disputeTime );
			if ( disputeData != null )
			{
				disputeData.setStatus(status);
				disputeData.setSupportComment(supportComment);
				disputeData.setUpdatedTimestamp(Calendar.getInstance().getTime().getTime());
				DKMDynamoDB.SaveDisputeData(disputeData);
			}
			else
			{
				return -1;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		
		return 0;
	}	
	
	/////////////////////////////////////////////////////////////////////
	
	@RemotingInclude
	public int				SetServiceTime( String sessionKey, int minutesInFuture, int duration, String name )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		try
		{
			ServiceTimeData serviceTime = DKMDynamoDB.LoadServiceTimeData( ServiceTimeData.ID );
			if ( serviceTime == null )
			{
				serviceTime = new ServiceTimeData();
				serviceTime.setId(ServiceTimeData.ID);
			}
	
			long timestamp = Calendar.getInstance().getTime().getTime() + (minutesInFuture*60*1000);
			serviceTime.setActive(1);
			serviceTime.setDuration(duration*60);// input is in minutes, but storage is in seconds
			serviceTime.setName(name);
			serviceTime.setTime(timestamp);
			
			DKMDynamoDB.SaveServiceTimeData( serviceTime );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		
		return 0;		
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int				ResetServiceTime( String sessionKey )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		try
		{
			ServiceTimeData serviceTime = DKMDynamoDB.LoadServiceTimeData( ServiceTimeData.ID );
			if ( serviceTime == null )
			{
				serviceTime = new ServiceTimeData();
				serviceTime.setId(ServiceTimeData.ID);
			}
			
			long timestamp = Calendar.getInstance().getTime().getTime();
			serviceTime.setActive(0);
			serviceTime.setDuration(0);
			serviceTime.setName("None");
			serviceTime.setTime(timestamp);
			
			DKMDynamoDB.SaveServiceTimeData( serviceTime );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		
		return 0;		
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int				BackupUserData( String sessionKey, String userID )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}

		try
		{
			int result = DKMDynamoDB.BackupPlayerData( userID ) != 0L ? 0 : 1;
			return result;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int				BackupAllUserData( String sessionKey )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		sLogger.error( "Starting Backup" );
		
		try
		{
			DKMDynamoDB.BackupAllPlayerData();
		}
		catch( Exception ex )
		{
			sLogger.error("Failed to backup PlayerData!",ex);
		}
		
		sLogger.error( "Completed Backup" );
		
		return DKMDynamoDB.UsersBackedUp();
	}

	/////////////////////////////////////////////////////////////////////
		
	@RemotingInclude
	public int				UserDataBackupCount( String sessionKey )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		return DKMDynamoDB.UsersBackedUp();
	}

	/////////////////////////////////////////////////////////////////////
	
	public static final int BACKUP_RESTORE_FAILED = 1;
	public static final int REAPPLY_TRANSACTIONS_FAILED = 2;
	
	@RemotingInclude
	public int				RestoreBackupUserData( String sessionKey, String userID, long time )
	{
		if ( !SessionIsActive(sessionKey) )
		{
			return -1;
		}
		
		try
		{
			boolean result = DKMDynamoDB.RestoreBackupPlayerData( userID, time );
			return result ? 0 : BACKUP_RESTORE_FAILED;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
	}

	/////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////
	/*
	public String	CreateTestSession()
	{
		SupportUserData user = new SupportUserData();
		String			sessionKey = "TEST";
		sActiveUsers.put(sessionKey, user);
		return sessionKey;
	}
	*/
	/////////////////////////////////////////////////////////////////////

}
