package services;


public class HighScore 
{
	private MahjongPlayer player;
	private int puzzle;
	private int score;
	private Boolean completed;
	private int attemptsOrCoins;
	private int stars;
	
	public HighScore()
	{
		completed = false;
	}

	public MahjongPlayer getPlayer() {
		return player;
	}

	public void setPlayer(MahjongPlayer player) {
		this.player = player;
	}

	public int getPuzzle() {
		return puzzle;
	}

	public void setPuzzle(int puzzle) {
		this.puzzle = puzzle;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public int getAttemptsOrCoins() {
		return attemptsOrCoins;
	}

	public void setAttemptsOrCoins(int attempts) {
		this.attemptsOrCoins = attempts;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}
}
