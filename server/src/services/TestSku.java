package services;

public class TestSku 
{
	public String getSkuID() {
		return SkuID;
	}
	public void setSkuID(String skuID) {
		SkuID = skuID;
	}

	public String getProductURL() {
		return ProductURL;
	}
	public void setProductURL(String productURL) {
		ProductURL = productURL;
	}
	
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	
	public int getCost() {
		return Cost;
	}
	public void setCost(int cost) {
		Cost = cost;
	}
	
	public int getCoins() {
		return Coins;
	}
	public void setCoins(int coins) {
		Coins = coins;
	}
	
	public float getCostInDollars() {
		return CostInDollars;
	}
	public void setCostInDollars(float costInDollars) {
		CostInDollars = costInDollars;
	}

	private String SkuID;
	private String ProductURL;
	private String Platform;
	private int Cost;
	private int Coins;
	private float CostInDollars;
}
