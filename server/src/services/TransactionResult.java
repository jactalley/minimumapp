package services;

public class TransactionResult
{
	public static final int NONE 					= 0;
	public static final int SUCCESS 				= 1;
	public static final int FAIL 					= 2;
	public static final int PROCESS_EXCEPTION 		= 4;
	public static final int PREVIOUSLY_COMPLETED 	= 0x1000;// this flag can be bitwise combined with the success and fail flags
	public static final int TOO_MANY_ATTEMPTS 		= 0x2000;// this flag can be bitwise combined with the success and fail flags
	public static final int NO_SAVE					= 0x4000;
	public static final int INVALID_SESSION 		= 0x8000;
	public static final int INVALID_PLAYER 			= 0x0100;
	public static final int INVALID_TRANSACTION 	= 0x0200;
	
	private String	transactionID;
	private int		result;
	private String	info;
	private int 	secondsToServiceTime;// used for maintenance communication
	
	public TransactionResult( String transactionID )
	{
		this.transactionID = transactionID;
		secondsToServiceTime = 0x7fffffff;// default to a time way-way in the future, so we can ignore it
		info = "";// default to no added info
		result = SUCCESS;// default to a success status
	}
	
	public TransactionResult( String transactionID, int resultCode )
	{
		this.transactionID = transactionID;
		secondsToServiceTime = 0x7fffffff;// default to a time way-way in the future, so we can ignore it
		info = "";// default to no added info
		result = resultCode;
	}
	
	public boolean isSuccessful()
	{
		return ( ( result & SUCCESS ) != 0 );
	}
	
	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getSecondsToServiceTime() {
		return secondsToServiceTime;
	}

	public void setSecondsToServiceTime(int secondsToServiceTime) {
		this.secondsToServiceTime = secondsToServiceTime;
	}
	
}
