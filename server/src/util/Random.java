package util;

public class Random 
{
	public Random()
	{
		setSeed(1);
	}
	public Random( int seed )
	{
		setSeed( seed );
	}
	
	public int getSeed()
	{
		return mSeed;
	}
		
	public void setSeed( int seed )
	{
		mSeed = seed;
		
		// initialize the tables
		int	mj;
		int	mk;
		int	i;
		int	ii;
		int	k;
		
		mj = MSEED - mSeed;
		mj %= MBIG;
		mMa[55] = mj;
		mk = 1;
		
		for ( i = 1; i <= 54; i++ )
		{
			ii = ( 21*i ) % 55;
			mMa[ii] = mk;
			mk = mj - mk;
			if ( mk < MZ )
			{
				mk += MBIG;
			}
			mj = mMa[ii];
		}
		
		for ( k = 1; k <= 4; k++ )
		{
			for ( i = 1; i <= 55; i++ )
			{
				mMa[i] -= mMa[1+(i+30)%55];
				if ( mMa[i] < MZ )
				{
					mMa[i] += MBIG;
				}
				if ( mMa[i] >= MBIG )
				{
					mMa[i] -= MBIG;
				}
			}
		}
		
		mInext = 0;
		mInextp = 31;
	}
	
	// returns -1.0 to 1.0
	public float Float()
	{
		return SignedFloat();
	}
	
	// returns -1.0 to 1.0
	public float SignedFloat()
	{
		float result = ( 2.0f * UnsignedFloat() ) - 1.0f;
		return result;
	}
	
	// returns 0.0 to 1.0
	public float UnsignedFloat()
	{
		if ( ++mInext == 56 ) mInext = 1;
		if ( ++mInextp == 56 ) mInextp = 1;
		int	mj = mMa[mInext] - mMa[mInextp];
		if ( mj < MZ )
		{
			mj += MBIG;
		}
		mMa[mInext] = mj;
		float	result = mj*FAC;
		return result;
	}
	
	public int Int()
	{
		return Int(101);
	}
	
	public int Int( int max )
	{
		int result = (int)(UnsignedFloat() * ( max ));
		return result;
	}
	
	public int IntInRange( int min, int max )
	{
		int result = min + (int)(UnsignedFloat() * ( max - min ));
		return result;
	}
	
	private static final int MBIG		= 1000000000;
	private static final int MSEED		= 161803398;
	private static final int MZ			= 0;
	private static final float FAC		= (1.0f/MBIG);
	
	private int mSeed; // "iff" in the source from Numeric Recipes
	private int mInext;
	private int mInextp;
	private int[] mMa = new int[56];
}
