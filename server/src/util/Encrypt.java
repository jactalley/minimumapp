package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Encrypt 
{
	public static String CryptWithMD5( String pass )
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] passBytes = pass.getBytes();
			md.reset();
			byte[] digested = md.digest(passBytes);
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<digested.length;i++)
			{
			    sb.append(Integer.toHexString(0xff & digested[i]));
			}
			return sb.toString();
		} 
		catch (NoSuchAlgorithmException ex) 
		{
			Logger.getLogger(Encrypt.class).error(ex);
		}
        
		return null;
   }
}
