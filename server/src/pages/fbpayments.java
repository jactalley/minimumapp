package pages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.CoinSku;
import model.CoinManager;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import dynamo.DKMDynamoDB;
import dynamo.DisputeData;
import dynamo.PurchaseData;

import java.util.Calendar;
import java.util.List;
import app.FBID;

/**
 * Servlet implementation class credits
 */
public class fbpayments extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	
	private static final Logger  				sLogger = Logger.getLogger(fbpayments.class);

	/**
     * @see HttpServlet#HttpServlet()
     */
    public fbpayments() 
    {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String hub_mode = request.getParameter("hub.mode");
		String hub_challenge = request.getParameter("hub.challenge");
		String hub_verify_token = request.getParameter("hub.verify_token");
		
		//response.setContentType("text/plain");
		response.setContentType("application/plain");
		if ( hub_mode.equals("subscribe") && hub_verify_token.equals("dkm_fb_payments") )
		{
			response.getWriter().println(hub_challenge);
		}
		else
		{
			response.getWriter().println("nonono");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		/*	The following json format is expected in the POST content
			{
			  "object": "payments",
			  "entry": [
			    {
			      "id": "296989303750203",
			      "time": 1347996346,
			      "changed_fields": [
			        "actions"
			      ]
			    },
			    {
			      "id": "296989303750203",
			      "time": 1347996346,
			      "changed_fields": [
			        "disputes"
			      ]
			    }
			  ]
			}
		*/
		
		try
		{
			String contentType = request.getContentType();  // get the incoming type
			if (contentType == null) return;  // nothing incoming, nothing to do
			//response.setContentType(contentType);  // set outgoing type to be incoming type
			
			BufferedReader	reader = request.getReader();
			String	updateData = "";
			String line = reader.readLine();
			while (line != null) 
			{
				updateData += line;
				line = reader.readLine();
			}
			
			JSONParser parser = new JSONParser();    
			JSONObject updateJSON = (JSONObject) parser.parse(updateData);    

			String	objectType = (String)updateJSON.get("object");
			if ( objectType.equals("payments") )
			{
				JSONArray	contentArray = (JSONArray)updateJSON.get("entry");
				
				int updateCount = contentArray.size();
				for (  int i = 0; i < updateCount; i++ )
				{
					JSONObject	content = (JSONObject)contentArray.get(i);
					
					String	id = (String)content.get("id");
					
					/* the change_fields param is always "actions" 
					 * even when the changing field is "disputes"
					 * so I'm changing to always update both the 
					 * payments and the disputes
					*/
					JSONObject	paymentJSON = getFacebookPaymentObject( id );
					updateFacebookPayment( paymentJSON );
					updateFacebookDispute( paymentJSON );
				}
			}
			else
			{
				getServletContext().log( "FB Realtime update for payments did not have payments as the object type!" );
			}	
		}
		catch ( Exception exception )
		{
			exception.printStackTrace();
			getServletContext().log( "(Ex)Problem processing fb payments realtime update: " + exception.getMessage() );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			getServletContext().log("(Th)Problem processing fb payments realtime update: " + thr.getMessage());
		}
	}
	
	protected JSONObject	getFacebookPaymentObject( String paymentID )
	{
		/*  payment open graph objects look like the following example...
		{
		  "id": "335633293233538", 
		  "user": : {
		    "name": "Connor Treacy",
		    "id": "696580152"
		  }, 
		  "application": {
		    "name": "Friend Smash", 
		    "namespace": "friendsmashsample", 
		    "id": "577408975624572"
		  }, 
		  "actions": [
		    {
		      "type": "charge", 
		      "status": "completed", 
		      "currency": "GBP", 
		      "amount": "0.69", 
		      "time_created": "2013-08-23T14:37:22+0000", 
		      "time_updated": "2013-08-23T14:37:24+0000"
		    },
		    {
		      "type": "refund", 
		      "status": "completed", 
		      "currency": "GBP", 
		      "amount": "0.69", 
		      "time_created": "2013-08-23T14:37:22+0000", 
		      "time_updated": "2013-08-23T14:37:24+0000"
		    }
		  ], 
		  "refundable_amount": {
		    "currency": "GBP", 
		    "amount": "0.69"
		  }, 
		  "items": [
		    {
		      "type": "IN_APP_PURCHASE", 
		      "product": "http://www.friendsmash.com/og/friend_smash_coin.html", 
		      "quantity": 1
		    }
		  ], 
		  "country": "GB", 
		  "created_time": "2013-08-23T14:37:22+0000", 
		  "test": 1, 
		  "fraud_status": "UNKNOWN", 
		  "payout_foreign_exchange_rate": 1.540539495,
		  "disputes": [
		     {
		        "user_comment": "I didn't receive my item! I want a refund, please!",
		        "time_created": "2013-03-24T18:21:02+0000",
		        "user_email": "email\u0040domain.com"
		     }
		  ]			
		}		 
	*/
		try
		{
			//https://graph.facebook.com/967671286696705?access_token=1789556937951605|OQiaE0Y6Gsm7vqeVSXKcVsOoodA&fields=id,user,application,actions,refundable_amount,items,country,created_time,test,fraud_status,payout_foreign_exchange_rate,disputes
			// url format... https://graph.facebook.com/payment_id?access_token=APP_ACCESS_TOKEN
			String url = "https://graph.facebook.com/" + paymentID + "?access_token=" + FBID.APP_ACCESS_TOKEN + "&fields=id,user,application,actions,refundable_amount,items,country,created_time,test,fraud_status,payout_foreign_exchange_rate,disputes";
			 
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	 
			int responseCode = con.getResponseCode();
			
			if ( responseCode <= 199 || responseCode >= 300 )
			{
				// we expect to get a response of 2XX or we need to log the problem
				getServletContext().log( "Bad response code (" + responseCode + ") accessing fb payment object id: " + paymentID );
				return null;
			}
	 
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			String		responseString = response.toString();

			JSONParser	parser = new JSONParser();    
			JSONObject	responseJSON = (JSONObject) parser.parse(responseString);
			
			return responseJSON;
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			getServletContext().log( "(Ex)Problem accessing fb payment object: " + exception.getMessage() );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			getServletContext().log("(Th)Problem accessing fb payment object: " + thr.getMessage());
		}
		
		return null;
	}
	
	protected void	updateFacebookPayment( JSONObject paymentJSON )
	{
		try
		{
			//JSONObject	paymentJSON = getFacebookPaymentObject( paymentID );
			String		purchase_id = (String) paymentJSON.get("id");
			
			JSONObject	user = (JSONObject)paymentJSON.get("user");
			String		user_id = (String) user.get("id");
			
			// we need to check to see if "test even exists before reading it
			Integer test = 0;
			if ( paymentJSON.containsKey("test") )
			{
				test = ((Long)paymentJSON.get("test")).intValue();
			}
			
			// exchange rate is a Float, except when the value is a whole number, at which point it is a Long
			// so we just ask for the string and convert to Float to avoid checking for value type
			Float		exchange_rate = Float.parseFloat(paymentJSON.get("payout_foreign_exchange_rate").toString());

			JSONArray	itemsArray = (JSONArray)paymentJSON.get("items");
			JSONObject	item = (JSONObject)itemsArray.get(0);
			if ( itemsArray.size() !=  1 )
			{
				getServletContext().log( "Problem processing fb payment update request, items Array size > 1.  paymentID: " + purchase_id );
			}
			Integer		quantity = ((Long)item.get("quantity")).intValue();
			String		product = (String) item.get("product");
			CoinSku		coinSku = CoinManager.Instance().GetProduct( product );
			String		sku = coinSku.getSkuID();
			Integer 	coins = coinSku.getCoins() * quantity;
	
			String		actionType;
			String		status;
			String		currency;
			Float		amount;
			
			JSONArray	actionsArray = (JSONArray)paymentJSON.get("actions");
			int			actionCount = actionsArray.size();
			JSONObject	currentAction = (JSONObject)actionsArray.get(0);
			actionType = (String) currentAction.get("type");
			status = (String) currentAction.get("status");
			currency = (String) currentAction.get("currency");
			amount = Float.parseFloat((String)currentAction.get("amount"));
			for ( int a = 1; a < actionCount; a++ )
			{
				String		thisActionType = (String) currentAction.get("type");
				String		thisStatus = (String) currentAction.get("status");
				String		thisCurrency = (String) currentAction.get("currency");
				Float		thisAmount = Float.parseFloat((String)currentAction.get("amount"));
				
				// only update the values based on changes
				if ( !actionType.equals("charge") )
				{
					actionType = thisActionType;
					status = thisStatus;
					currency = thisCurrency;
					amount = thisAmount;
				}
			}
			
			PurchaseData	purchaseLog = getPurchaseLog( user_id, purchase_id );
			
			if ( purchaseLog == null )
			{
				CreatePurchaseLog( user_id, purchase_id, actionType, status, sku, currency, amount, exchange_rate, quantity, coins, test );			
			}
			else
			{
				UpdatePurchaseLog( purchaseLog, user_id, purchase_id, actionType, status, sku, currency, amount, exchange_rate, quantity, coins, test );			
			}
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			sLogger.error( "(Ex)Problem processing fb payment update request: ", exception );
			getServletContext().log( "(Ex)Problem processing fb payment update request: " + exception.getMessage() );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			sLogger.error( "(Th)Problem processing fb payment update request: ", thr );
			getServletContext().log("(Th)Problem processing fb payment update request: " + thr.getMessage());
		}
	}
	
	protected PurchaseData	getPurchaseLog( String user_id, String purchase_id ) throws Exception
	{
		PurchaseData purchaseData = DKMDynamoDB.LoadPurchaseData( user_id, purchase_id );
		return purchaseData;
	}
	
	protected void	UpdatePurchaseLog( PurchaseData	purchaseLog, String user_id, String purchase_id, String actionType, String status, String sku, String currency, Float amount, Float exchange_rate, Integer quantity, Integer coins, Integer test ) throws Exception
	{
		// always update action and status if they have changed
		boolean  logChanged = false;
		if ( !actionType.equals( purchaseLog.getChargeAction() ) )
		{
			purchaseLog.setChargeAction(actionType);
			logChanged = true;
		}
		if ( !status.equals( purchaseLog.getChargeStatus() ) )
		{
			purchaseLog.setChargeStatus(status);
			logChanged = true;
		}
		if ( actionType.equals("charge") && status.equals("completed") && ( purchaseLog.getClaimStatus() == PurchaseData.UNKNOWN_STATUS ) )
		{
			purchaseLog.setClaimStatus(PurchaseData.UNCLAIMED_STATUS);
			logChanged = true;
			getServletContext().log( "Potential problem: fb payment (id:" + purchase_id + ") PurchaseLog claim status updated - should not happen under normal circumstances." );
		}
		
		if ( logChanged )
		{
			DKMDynamoDB.SavePurchaseData( purchaseLog);
		}
	}
	
	protected void	CreatePurchaseLog( String user_id, String purchase_id, String actionType, String status, String sku, String currency, Float amount, Float exchange_rate, Integer quantity, Integer coins, Integer test ) throws Exception
	{
		int	claimStatus = PurchaseData.UNCLAIMED_STATUS;
		if ( actionType.equals("charge") && ( status.equals("failed") || status.equals("initiated") ) )
		{
			claimStatus = PurchaseData.UNKNOWN_STATUS;
			getServletContext().log( "Problem: fb payment (id:" + purchase_id + ") PurchaseLog created with status: " + status );
		}
		
		PurchaseData	purchase = new PurchaseData();
		purchase.setHostUserID(user_id);
		purchase.setHostPurchaseID(purchase_id);
		purchase.setSku(sku);
		purchase.setChargeAction(actionType);
		purchase.setChargeStatus(status);
		purchase.setCurrency(currency);
		purchase.setAmount(amount);
		purchase.setExchangeRate(exchange_rate);
		purchase.setQuantity(quantity);
		purchase.setCoins(coins);
		purchase.setClaimStatus(claimStatus);
		purchase.setTest(test);
		long timestamp = Calendar.getInstance().getTime().getTime();
		purchase.setPurchaseTimestamp(timestamp);
		purchase.setClaimedTimestamp(timestamp);
		
		DKMDynamoDB.SavePurchaseData( purchase );
	}
	
	//@SuppressWarnings("unchecked")
	protected void	updateFacebookDispute( JSONObject paymentJSON )
	{
		/*
		  "disputes": [
		     {
		        "user_comment": "I didn't receive my item! I want a refund, please!",
		        "time_created": "2013-03-24T18:21:02+0000",
		        "user_email": "email\u0040domain.com"
		     }
		  ]			
		 */
		
		try
		{
			if ( paymentJSON.containsKey("disputes") )
			{
				//JSONObject	paymentJSON = getFacebookPaymentObject( paymentID );
				String		purchase_id = (String) paymentJSON.get("id");
				
				JSONObject	user = (JSONObject)paymentJSON.get("user");
				String		user_id = (String) user.get("id");
				
				// find all disputes previously logged for this purchase
				//List<DisputeLog> loggedDisputes = (List<DisputeLog>)mTemplate.findByNamedQueryAndNamedParam(DisputeLog.FIND_BY_PURCHASE_ID, DisputeLog.PURCHASE_ID_PARM, purchase_id);
				List<DisputeData>	loggedDisputes = DKMDynamoDB.LoadAllDisputeData( purchase_id );
				
				// we need to check to see if "test even exists before reading it
				Integer test = 0;
				if ( paymentJSON.containsKey("test") )
				{
					test = ((Long)paymentJSON.get("test")).intValue();
				}				
				
				JSONArray	disputesArray = (JSONArray)paymentJSON.get("disputes");
				int			disputeCount = disputesArray.size();
				for ( int d = 0; d < disputeCount; d++ )
				{
					JSONObject	currentDispute = (JSONObject)disputesArray.get(d);
					String		userComment = (String) currentDispute.get("user_comment");
					String		timeCreated = (String) currentDispute.get("time_created");
					String		userEmail = (String) currentDispute.get("user_email");
					
					if ( userComment == null || userComment.length() == 0 )
					{
						userComment = "NO COMMENT WAS WRITTEN.";
					}
					if ( userEmail == null || userEmail.length() == 0 )
					{
						userEmail = "NO EMAIL AVAILABLE.";
					}
					
					if ( loggedDisputes.size() == 0 )
					{
						CreateDisputeLog( user_id, purchase_id, timeCreated, userComment, userEmail, test, 0 );
					}
					else
					{
						// disputes have already been logged for this purchase, 
						// so make sure we don't try to duplicate
						for ( int i = 0; i < loggedDisputes.size(); i++ )
						{
							// check to see if this dispute update has already been logged
							DisputeData log = loggedDisputes.get(i);
							if ( !log.getHostTimeCreated().equals(timeCreated) )
							{
								CreateDisputeLog( user_id, purchase_id, timeCreated, userComment, userEmail, test, i );
							}
						}
					}
				}
			}
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
			sLogger.error( "(Ex)Problem processing fb payment update dispute: ", exception );
			getServletContext().log( "(Ex)Problem processing fb payment update dispute: " + exception.getMessage() );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			sLogger.error( "(Th)Problem processing fb payment update dispute: ", thr );
			getServletContext().log("(Th)Problem processing fb payment update dispute: " + thr.getMessage());
		}		
	}
	
	protected void	CreateDisputeLog( String user_id, String purchase_id, String timeCreated, String userComment, String userEmail, Integer test, Integer timeOffset )
	{
		try 
		{
			//search for disputes by purchase id, then create any not found in the returned list
			DisputeData	dispute = new DisputeData();
			dispute.setHostUserID(user_id);
			dispute.setHostPurchaseID(purchase_id);
			dispute.setEmail(userEmail);
			dispute.setUserComment(userComment);
			dispute.setSupportComment("New dispute.");
			dispute.setStatus(DisputeData.NEW_STATUS);
			dispute.setHostTimeCreated(timeCreated);
			dispute.setTest(test);
			// timeOffset is needed because the time field is used as part of the 
			// unique identifier key (along with purchaseID) in the database
			long timestamp = Calendar.getInstance().getTime().getTime() + timeOffset;
			dispute.setDisputeTimestamp(timestamp);
			dispute.setUpdatedTimestamp(timestamp);
			DKMDynamoDB.SaveDisputeData( dispute );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
