package pages;

import java.io.BufferedReader;
import java.io.IOException;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import app.FBID;

import util.FBUtil;

/**
 * Servlet implementation class gameupdate
 */
public class gameupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public gameupdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.getWriter().println("nonono");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try
		{
			String contentType = request.getContentType();  // get the incoming type
			if (contentType == null) return;  // nothing incoming, nothing to do
			response.setContentType("application/json");  // set outgoing type to be incoming type
			
			BufferedReader	reader = request.getReader();
			String	updateData = "";
			String line = reader.readLine();
			while (line != null) 
			{
				updateData += line;
				line = reader.readLine();
			}
			
			Map decodedRequest = FBUtil.parse_signed_request(updateData, FBID.SECRET);	//Will throw an error if the signature is invalid
			
			JSONParser parser = new JSONParser();    
			JSONObject updateJSON = (JSONObject) parser.parse((String)decodedRequest.get("request_payload"));

			String	actionType = (String)updateJSON.get("action");
			if ( actionType.equals("save") )
			{
				
			}
			else if (actionType.equals("load"))
			{
				JSONObject responseData = new JSONObject();
				responseData.put("myTurn", true);
				response.getWriter().println(responseData.toJSONString());
			}
			else
			{
				getServletContext().log( "Game update did not have a valid object type!" );
			}	
		}
		catch ( Exception exception )
		{
			exception.printStackTrace();
			getServletContext().log( "(Ex)Problem processing game update: " + exception.getMessage() );
		}
		catch ( Throwable thr )
		{
			thr.printStackTrace();
			getServletContext().log("(Th)Problem processing game update: " + thr.getMessage());
		}
	}

}
