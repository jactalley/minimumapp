package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
//import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Manager 
{
	//static String sBucketName = "MJPuzzles";
	//static String sBucketName = "DKPuzzles";
	static String sBucketName = "dkp.puzzles.dragonkinggames.com";
	
	public void SaveAsTextFile( String userID, String filename, String text )
	{
		try {
        	
			AmazonS3 s3 = new AmazonS3Client(new PropertiesCredentials(
			        S3Manager.class.getResourceAsStream("AwsCredentials.properties")));
			
			String key = userID + "/" + filename;
			
            s3.putObject(new PutObjectRequest(sBucketName, key, createTempFile( text )));

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with S3, "
			        + "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch ( Exception exception ) {
			exception.printStackTrace();
		}

	}
	
    private static File createTempFile( String text ) throws IOException
    {
        String filename = "puzzle-" + UUID.randomUUID();
        File file = File.createTempFile( filename, ".xml" );
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write(text);
        writer.close();

        return file;
    }
	
	public String GetFileAsText( String userID, String filename )
	{
		try {
			String filetext = "";
			AmazonS3 s3 = new AmazonS3Client(new PropertiesCredentials(
			        S3Manager.class.getResourceAsStream("AwsCredentials.properties")));
			
			String key = userID + "/" + filename;
	        S3Object object = s3.getObject(new GetObjectRequest(sBucketName, key));
	        BufferedReader reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
	        while (true) {
	            String line = reader.readLine();
	            if (line == null) break;
	            filetext += line + "\n";
	        }
			return filetext;
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with S3, "
			        + "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch ( Exception exception ) {
			exception.printStackTrace();
		}
		
		return "";
	}
	
	public String GetFileListing( String userID )
	{
		try {
			String listing = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<files>\n";
			
			AmazonS3 s3 = new AmazonS3Client(new PropertiesCredentials(
			        S3Manager.class.getResourceAsStream("AwsCredentials.properties")));
			
			String prefix = userID;
			
            ObjectListing objectListing = s3.listObjects(new ListObjectsRequest()
            												.withBucketName(sBucketName)
            												.withPrefix(prefix));
		    for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) 
		    {
		    	listing += "\t<file name=\"" + objectSummary.getKey() + "\" />\n";
		    }
		    
		    listing += "</files>";
			
			return listing;
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it "
			        + "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
			        + "a serious internal problem while trying to communicate with S3, "
			        + "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch ( Exception exception ) {
			exception.printStackTrace();
		}

		return "";
	}
}
