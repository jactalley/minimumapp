package model;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//import org.hibernate.SessionFactory;

import dynamo.DKMDynamoDB;
import dynamo.ServiceTimeData;

public class ServiceManager 
{
	//private static SessionFactory		sSessionFactory;
	//private static HibernateTemplate	sTemplate;
	
	private static ScheduledExecutorService		sScheduler;
	private static final long					SCHEDULE_CHECK_PERIOD = 300; // 5 minutes in seconds
	
	private static ServiceTimeData			sServiceTime = null;
	
	public static void Initialize( /*SessionFactory sessionFactory*/ )
	{
		//sSessionFactory = sessionFactory;
		//sTemplate = new HibernateTemplate(sSessionFactory);
        sScheduler = Executors.newScheduledThreadPool(1);
        Runnable serviceCheck = new CheckServiceTimeTask();
        sScheduler.scheduleAtFixedRate(serviceCheck, SCHEDULE_CHECK_PERIOD, SCHEDULE_CHECK_PERIOD, TimeUnit.SECONDS);
	}
	
	private static final class CheckServiceTimeTask implements Runnable 
	{
		public void run() 
		{
			try 
			{
				sServiceTime = DKMDynamoDB.LoadServiceTimeData( ServiceTimeData.ID );
			
				if ( sServiceTime == null )
				{
					sServiceTime = new ServiceTimeData();
					sServiceTime.setId(ServiceTimeData.ID);
					sServiceTime.setActive(0);
					sServiceTime.setDuration(0);
					sServiceTime.setName("None");
					sServiceTime.setTime(Calendar.getInstance().getTime().getTime());
					DKMDynamoDB.SaveServiceTimeData( sServiceTime );
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public static boolean ServiceTimeActive()
	{
		if ( sServiceTime == null ) return false;
		return sServiceTime.getActive() != 0;
	}
	
	public static int ServiceTimeSecondsToStart()
	{
		if ( !ServiceTimeActive() ) return 0x7fffffff;// return a time way-way in the future, so we can ignore it
		
		long startTime = sServiceTime.getTime();
		long now = Calendar.getInstance().getTime().getTime();
		int deltaSeconds = (int) ((startTime - now)/1000);
		
		return deltaSeconds;
	}	
	
	public static int ServiceDuration()
	{
		if ( !ServiceTimeActive() ) return 0;
		return sServiceTime.getDuration();
	}	
	
	public static String ServiceName()
	{
		if ( !ServiceTimeActive() ) return "None";
		return sServiceTime.getName();
	}	
}
