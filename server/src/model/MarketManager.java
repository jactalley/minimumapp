package model;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import app.FBID;


public class MarketManager 
{
	private HashMap<String,HashMap<String,MarketCollection>>	mCollections = new HashMap<String,HashMap<String,MarketCollection>>();
	private HashMap<String,MarketItem>							mPurchaseTable = new HashMap<String,MarketItem>();
	
	public MarketManager()
	{
		parseXmlFile();
	}
	
	public HashMap<String,MarketCollection>	GetCollectionTable(String type)
	{
		return mCollections.get(type);
	}
	
	public MarketCollection					GetCollection(String type, String name)
	{
		HashMap<String,MarketCollection>	typeMap = GetCollectionTable(type);
		if ( typeMap == null ) return null;
		return typeMap.get(name);
	}
	
	public MarketItem						GetPurchaseItem( String itemTag )
	{
		return mPurchaseTable.get(itemTag);
	}

	private void parseXmlFile()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			//Document dom = db.parse("http://md.dragonkinggames.com/assets/server_data/market.xml");
			//Document dom = db.parse("http://s3.amazonaws.com/pstaging.dragonkinggames.com/assets/server_data/market.xml");
			Document dom = db.parse(FBID.APP_STATIC_DATA + "market.xml");
			Element docElement = dom.getDocumentElement();

			NodeList nl = docElement.getElementsByTagName("type");
			if(nl != null && nl.getLength() > 0) 
			{
				for(int i = 0 ; i < nl.getLength();i++) 
				{
					Element typeElement = (Element)nl.item(i);
					parseXmlType( typeElement );
				}
			}
			
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private void parseXmlType( Element typeElement )
	{
		HashMap<String,MarketCollection>	table = new HashMap<String,MarketCollection>();

		String	key = typeElement.getAttribute("name");
		mCollections.put(key, table);
		
		NodeList nl = typeElement.getElementsByTagName("collection");
		if(nl != null && nl.getLength() > 0) 
		{
			for(int i = 0 ; i < nl.getLength();i++) 
			{
				Element entry = (Element)nl.item(i);
				MarketCollection collection = MarketCollection.CreateCollection( entry, key, mPurchaseTable );
				table.put(collection.getName(), collection);
			}
		}
	}
}
