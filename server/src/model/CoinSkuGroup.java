package model;

import java.util.HashMap;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CoinSkuGroup 
{
	public static CoinSkuGroup CreateGroup( Element xmlElement, HashMap<String,CoinSku> purchaseTable, HashMap<String,CoinSku> productTable )
	{
		CoinSkuGroup group = new CoinSkuGroup();
		
		group.setPlatform( xmlElement.getAttribute("name") );
		
		NodeList nl = xmlElement.getElementsByTagName("sale");
		if(nl != null && nl.getLength() > 0) 
		{
			for(int i = 0 ; i < nl.getLength();i++) 
			{
				Element element = (Element)nl.item(i);
				CoinSku sku = CoinSku.CreateSku( element, group.getPlatform(), purchaseTable, productTable );
				group.getSkus().add(sku);
			}
		}
		
		return group;
	}

	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public Vector<CoinSku> getSkus() {
		return Skus;
	}
	
	private String Platform;
	private Vector<CoinSku> Skus = new Vector<CoinSku>();

}
