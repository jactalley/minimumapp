package model;


public class ItemDescription 
{
	public int itemIndex;
	public int itemType;
	public int itemID;
	public int itemData;
	
	ItemDescription( int index, int type, int id, int data )
	{
		itemIndex = index;
		itemType = type;
		itemID = id;
		itemData = data;
	}
	
	public boolean MatchesIndex( int index )
	{
		return index == itemIndex;
	}
	
	public boolean MatchesData( int type, int id, int data )
	{
		return type == itemType && id == itemID && data == itemData;
	}
}
