package model;

import java.util.HashMap;

import org.w3c.dom.Element;

public class MarketItem 
{
	public static MarketItem CreateItem( Element xmlElement, String collection_type, HashMap<String,MarketItem> purchaseTable )
	{
		MarketItem item = new MarketItem();
		
		// create the item
		item.setCollectionType( collection_type );
		item.setType( xmlElement.getAttribute("type") );
		item.setName( xmlElement.getAttribute("name") );
		item.setCost( Integer.parseInt(xmlElement.getAttribute("cost")) );
		item.setCount( Integer.parseInt(xmlElement.getAttribute("count")) );
		item.setMarketID( Integer.parseInt(xmlElement.getAttribute("market_id")) );
		item.setTypeID( Integer.parseInt(xmlElement.getAttribute("type_id")) );
		item.setItemID( Integer.parseInt(xmlElement.getAttribute("item_id")) );
		item.setData( Integer.parseInt(xmlElement.getAttribute("data")) );
		
		// add the item to the purchase table
		//String key = Integer.toString(item.getTypeID()) + "-" + Integer.toString(item.getItemID()) + "-" + Integer.toString(item.getData());
		String key = Integer.toString(item.getMarketID());
		purchaseTable.put(key, item);
		
		return item;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getCollectionType() {
		return CollectionType;
	}
	public void setCollectionType(String type) {
		CollectionType = type;
	}

	public int getCost() {
		return Cost;
	}

	public void setCost(int cost) {
		Cost = cost;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public int getMarketID() {
		return MarketID;
	}

	public void setMarketID(int marketID) {
		MarketID = marketID;
	}

	public int getTypeID() {
		return TypeID;
	}

	public void setTypeID(int typeID) {
		TypeID = typeID;
	}

	public int getItemID() {
		return ItemID;
	}

	public void setItemID(int itemID) {
		ItemID = itemID;
	}

	public int getData() {
		return Data;
	}

	public void setData(int data) {
		Data = data;
	}

	private String Name;
	private String Type;
	private String CollectionType;
	private int Cost;
	private int Count;
	private int MarketID;
	private int TypeID;
	private int ItemID;
	private int Data;
}
