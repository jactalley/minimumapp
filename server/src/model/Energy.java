package model;

public class Energy 
{
	public static final int	BASE_ENERGY						= 5;
	public static final int	ENERGY_PER_LEVEL				= 0;
	public static final int	SECONDS_PER_ENERGY_GAIN			= 1800;
	public static final int	AUTO_ENERGY_GAIN				= 1;
	
	public static final int	ENERGY_PER_TILE_SELECT			= 0;
	public static final int	ENERGY_PER_TILE_UNSELECT		= 0;
	public static final int	ENERGY_PER_AUTO_TILE_MATCH		= -5;
	public static final int	ENERGY_PER_HINT					= -5;
	public static final int	ENERGY_PER_SHUFFLE				= -10;
	public static final int	ENERGY_PER_SHOW_OPEN			= -5;
	public static final int ENERGY_PER_SELECT_ANY			= -10;
	public static final int ENERGY_PER_DRAGON_EYES			= -10;
	public static final int ENERGY_PER_QUIT					= 1;
	public static final int ENERGY_PER_GAME_START			= 0;

	public static int	MaxEnergyForLevel( int level )
	{
		return model.Energy.BASE_ENERGY + (level-1)*model.Energy.ENERGY_PER_LEVEL;
	}
}
