package model;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import app.FBID;


public class CoinManager 
{
	private static final Logger  				sLogger = Logger.getLogger(CoinManager.class);

	private static CoinManager sCoinManager = new CoinManager();
    
	private HashMap<String,CoinSkuGroup>			mGroups = new HashMap<String,CoinSkuGroup>();
	private HashMap<String,CoinSku>					mPurchaseTable = new HashMap<String,CoinSku>();
	private HashMap<String,CoinSku>					mProductTable = new HashMap<String,CoinSku>();
	
	public static CoinManager Instance()
	{
		return sCoinManager;
	}
	
	public CoinManager()
	{
		parseXmlFile();
	}
	
	public HashMap<String,CoinSkuGroup>	GetPlatformTable(String platform)
	{
		return mGroups;
	}
	
	public CoinSkuGroup					GetPlatformGroup(String platform)
	{
		return mGroups.get(platform);
	}
	
	public CoinSku						GetCoinSku( String skuID )
	{
		return mPurchaseTable.get(skuID);
	}
	
	public CoinSku						GetProduct( String productURL )
	{
		return mProductTable.get(productURL);
	}

	private void parseXmlFile()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			//Document dom = db.parse("http://md.dragonkinggames.com/assets/server_data/coins_live.xml");
			//Document dom = db.parse("http://s3.amazonaws.com/pstaging.dragonkinggames.com/assets/server_data/coins.xml");
			Document dom = db.parse(FBID.APP_STATIC_DATA + "coins.xml");
			Element docElement = dom.getDocumentElement();

			NodeList nl = docElement.getElementsByTagName("platform");
			if(nl != null && nl.getLength() > 0) 
			{
				for(int i = 0 ; i < nl.getLength();i++) 
				{
					Element entry = (Element)nl.item(i);
					CoinSkuGroup group = CoinSkuGroup.CreateGroup( entry, mPurchaseTable, mProductTable );
					mGroups.put(group.getPlatform(), group);
				}
			}
			
			sLogger.warn("CoinManager successfully loaded");
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
			sLogger.warn(pce.getMessage());
		}catch(SAXException se) {
			se.printStackTrace();
			sLogger.warn(se.getMessage());
		}catch(IOException ioe) {
			ioe.printStackTrace();
			sLogger.warn(ioe.getMessage());
		}
	}
}
