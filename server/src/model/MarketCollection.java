package model;

import java.util.HashMap;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class MarketCollection 
{
	public static MarketCollection CreateCollection( Element xmlElement, String collection_type, HashMap<String,MarketItem> purchaseTable )
	{
		MarketCollection collection = new MarketCollection();
		
		collection.setType( collection_type );
		collection.setName( xmlElement.getAttribute("name") );
		
		NodeList nl = xmlElement.getElementsByTagName("item");
		if(nl != null && nl.getLength() > 0) 
		{
			for(int i = 0 ; i < nl.getLength();i++) 
			{
				Element element = (Element)nl.item(i);
				MarketItem item = MarketItem.CreateItem( element, collection_type, purchaseTable );
				collection.getItems().add(item);
			}
		}
		
		return collection;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public Vector<MarketItem> getItems() {
		return Items;
	}
	
	private String Name;
	private String Type;
	private Vector<MarketItem> Items = new Vector<MarketItem>();

}
