package model;

import java.util.HashMap;

import org.w3c.dom.Element;

import app.FBID;

public class CoinSku 
{
	public static CoinSku CreateSku( Element xmlElement, String platform, HashMap<String,CoinSku> purchaseTable, HashMap<String,CoinSku> productTable )
	{
		CoinSku sku = new CoinSku();
		
		// create the item
		sku.setPlatform( platform );
		sku.setSkuID( xmlElement.getAttribute("sku") );
		sku.setProductURL( FBID.APP_OPEN_GRAPH_URL + xmlElement.getAttribute("ogurl") );
		sku.setCost( Integer.parseInt(xmlElement.getAttribute("cost")) );
		sku.setCoins( Integer.parseInt(xmlElement.getAttribute("coins")) );
		sku.setCostInDollars( Float.parseFloat(xmlElement.getAttribute("costInDollars")) );
		
		// add the item to the purchase&product tables
		purchaseTable.put(sku.getSkuID(), sku);
		productTable.put(sku.getProductURL(), sku);
		
		return sku;
	}

	public String getSkuID() {
		return SkuID;
	}
	public void setSkuID(String skuID) {
		SkuID = skuID;
	}

	public String getProductURL() {
		return ProductURL;
	}
	public void setProductURL(String productURL) {
		ProductURL = productURL;
	}
	
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	
	public int getCost() {
		return Cost;
	}
	public void setCost(int cost) {
		Cost = cost;
	}
	
	public int getCoins() {
		return Coins;
	}
	public void setCoins(int coins) {
		Coins = coins;
	}
	
	public float getCostInDollars() {
		return CostInDollars;
	}
	public void setCostInDollars(float costInDollars) {
		CostInDollars = costInDollars;
	}

	private String SkuID;
	private String ProductURL;
	private String Platform;
	private int Cost;
	private int Coins;
	private float CostInDollars;
}
