package model;

import org.apache.log4j.Logger;

import dynamo.DynamoDBManager;

public class ItemManager 
{
	
	private static final Logger  				sLogger = Logger.getLogger(DynamoDBManager.class);

	// these negative indices are passed in the transaction data to indicate non-owned items were part of the transaction
	public static final int PEARL_ZAP_POWER_INDEX 				= -1;
	public static final int HAMMER_POWER_INDEX 					= -2;
	public static final int RANDOM_BLAST_POWER_INDEX 			= -3;
	public static final int KEY_POWER_INDEX 					= -4;
	public static final int ADD_TIME_POWER_INDEX 				= -5;
	public static final int LONG_SWAP_POWER_INDEX 				= -6;
	public static final int FLIP_BLOCKS_POWER_INDEX 			= -7;
	public static final int	CREATE_WILD_POWER_INDEX 			= -8;
	public static final int	OVERLOAD_BEAMS_POWER_INDEX 			= -9;
	public static final int	QUICK_HINT_POWER_INDEX 				= -10;
	public static final int	PEARL_BOMB_POWER_INDEX	 			= -11;
	public static final int	BLOCK_BOMB_POWER_INDEX 				= -12;
	public static final int	SHUFFLE_POWER_INDEX 				= -13;
	public static final int	LOCK_PAUSE_POWER_INDEX 				= -14;
	public static final int	SLOW_DRAIN_POWER_INDEX 				= -15;
	
	public static final int LIFE_REFILL_INDEX					= -128;
	public static final int ADD_TIME_INDEX						= -129;
	public static final int REMOVE_SCORES_INDEX                 = -130;  // admin transaction which tells from which puzzle onward the scores were removed.
	public static final int UNLOCK_PUZZLES						= -131;  // transaction which tells a delta of puzzles to unlock and updates the unlock timestamp
	public static final int UNLOCK_PUZZLES_AT_LOGIN				= -132;	// transaction which tells a delta of puzzles to unlock, but doesn't update the unlock timestamp

	public static final int PUZZLE_INDEX_SET					= -256;
	public static final int PUZZLE_SCORE_SET					= -257;
	public static final int PUZZLE_STARS_SET					= -258;
	public static final int PUZZLE_ATTEMPTS_COINS_SET			= -259;

	public static final int	UNKNOWN_TYPE 				= 0;
	public static final int COIN_TYPE 					= 1;
	public static final int LIFE_TYPE					= 2;

	public static final int DEFAULT_COIN_ID 			= 1;
	public static final int DEFAULT_LIFE_ID 			= 2;
	
	public static final int UNIQUE_IN_USE_ITEM_COUNT 			= 0;
	
	public static final int CONSUMABLE_ITEM_INDEX_COUNT			= 2;	// coins, lives
	
	public static final int UNIQUE_INT_ITEM_COUNT 				= CONSUMABLE_ITEM_INDEX_COUNT;
	public static final int UNIQUE_SHORT_ITEM_COUNT 			= 0;
	public static final int UNIQUE_BYTE_ITEM_COUNT 				= 0;
	public static final int UNIQUE_ITEM_COUNT 					= UNIQUE_INT_ITEM_COUNT + UNIQUE_SHORT_ITEM_COUNT + UNIQUE_BYTE_ITEM_COUNT;
	
	public static final int START_INT_ITEM_INDEX				= 0;
	public static final int START_SHORT_ITEM_INDEX				= 10000;
	public static final int START_BYTE_ITEM_INDEX				= 1000000;
	
	public static final int END_INT_ITEM_INDEX					= START_INT_ITEM_INDEX + UNIQUE_INT_ITEM_COUNT;
	public static final int END_SHORT_ITEM_INDEX				= START_SHORT_ITEM_INDEX + UNIQUE_SHORT_ITEM_COUNT;
	public static final int END_BYTE_ITEM_INDEX					= START_BYTE_ITEM_INDEX + UNIQUE_BYTE_ITEM_COUNT;
	
	public static final int INT_ITEM_BASE_ARRAY_INDEX			= 0;
	public static final int SHORT_ITEM_BASE_ARRAY_INDEX			= UNIQUE_INT_ITEM_COUNT;
	public static final int BYTE_ITEM_BASE_ARRAY_INDEX			= SHORT_ITEM_BASE_ARRAY_INDEX + UNIQUE_SHORT_ITEM_COUNT;
	
	public static final int COIN_ITEM_INDEX					= 0+START_INT_ITEM_INDEX;
	public static final int LIFE_ITEM_INDEX					= 1+START_INT_ITEM_INDEX;

	public static int	MaxItemCount( int type )
	{
		// value set to -1 if not used...
		int	maxCounts[] = { 0, 2000000000, 5 };
		
		return maxCounts[type];
	}
	
	// ItemDescription's are in index order.  Do NOT change this ordering unless you REALLY know what you are doing
	private static final ItemDescription	sItemDescriptions[] =
	{
		// consumable items
		new ItemDescription( COIN_ITEM_INDEX, 				COIN_TYPE, 			DEFAULT_COIN_ID, 			0 ),
		new ItemDescription( LIFE_ITEM_INDEX,				LIFE_TYPE,			DEFAULT_LIFE_ID,			0 )
		
		// ticket items
		

		// treasure chest items
		
		
		// treasure key items
		
		

		
		// launch tree items
		

		// launch duab items
		
	};

	public static int	ItemArrayIndexFromVirtualIndex( int index )
	{
		// map from a virtual index to the physical index
		int arrayIndex = index;
		if ( index >= START_BYTE_ITEM_INDEX && index < END_BYTE_ITEM_INDEX )
		{
			arrayIndex -= START_BYTE_ITEM_INDEX;
			arrayIndex += BYTE_ITEM_BASE_ARRAY_INDEX;
		}
		else
		if ( index >= START_SHORT_ITEM_INDEX && index < END_SHORT_ITEM_INDEX )
		{
			arrayIndex -= START_SHORT_ITEM_INDEX;
			arrayIndex += SHORT_ITEM_BASE_ARRAY_INDEX;
		}
		else
		if ( index >= START_INT_ITEM_INDEX && index < END_INT_ITEM_INDEX )
		{
			arrayIndex -= START_INT_ITEM_INDEX;
			arrayIndex += INT_ITEM_BASE_ARRAY_INDEX;
		}
		else
		{
	    	String message = "ItemArrayIndexFromVirtualIndex did not find an object for index: " + Integer.toString(index);
	    	sLogger.error(message);
			return -1;
		}
		
		if ( arrayIndex >= 0 && arrayIndex < sItemDescriptions.length )
		{
			return arrayIndex;
		}
		else
		{
	    	String message = "ItemArrayIndexFromVirtualIndex array index out of range, index: " + Integer.toString(index);
	    	sLogger.error(message);
			return -1;
		}
		
	}

	public static ItemDescription	ItemDescriptionFromIndex( int index )
	{
		int arrayIndex = ItemArrayIndexFromVirtualIndex( index );
		if ( arrayIndex >= 0 )
		{
			return sItemDescriptions[arrayIndex];
		}
		
		return null;
	}
	
	public static int	ItemIndexFromDescription( int type, int id, int data )
	{
		for ( int index = 0; index < sItemDescriptions.length; index++ )
		{
			if ( sItemDescriptions[index].MatchesData(type, id, data) )
			{
				return sItemDescriptions[index].itemIndex;
			}
		}
		
    	String message = "ItemIndexFromDescription did not find an object with info: { "
		        + Integer.toString(type) + ", " + Integer.toString(id) + ", " + Integer.toString(data) + " }";
    	sLogger.error(message);
		
		return -1;
	}
	
}
