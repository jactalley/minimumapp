package model;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import app.FBID;

public class LevelAccessManager 
{
	private HashMap<String,LevelAccessAsset>	mAssets = new HashMap<String,LevelAccessAsset>();
	
	public LevelAccessManager()
	{
		parseXmlFile();
	}
	
	public LevelAccessAsset	GetAsset(String name)
	{
		return mAssets.get(name);
	}
	
	public boolean AssetLocked( String name, int playerLevel )
	{
		LevelAccessAsset asset = mAssets.get(name);
		if ( asset == null )
		{
			return false;
		}
		return ( asset.getAccessLevel() > playerLevel );
	}
	
	private void parseXmlFile()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			//Document dom = db.parse("http://md.dragonkinggames.com/assets/server_data/level_access.xml");
			//Document dom = db.parse("http://s3.amazonaws.com/pstaging.dragonkinggames.com/assets/server_data/level_access.xml");
			Document dom = db.parse(FBID.APP_STATIC_DATA + "level_access.xml");
			Element docElement = dom.getDocumentElement();

			NodeList nl = docElement.getElementsByTagName("type");
			if(nl != null && nl.getLength() > 0) 
			{
				for(int i = 0 ; i < nl.getLength();i++) 
				{
					Element typeElement = (Element)nl.item(i);
					parseXmlType(typeElement);
				}
			}
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private void parseXmlType( Element typeElement )
	{
		String	typeName = typeElement.getAttribute("name");
		NodeList nl = typeElement.getElementsByTagName("asset");
		if(nl != null && nl.getLength() > 0) 
		{
			for(int i = 0 ; i < nl.getLength();i++) 
			{
				Element assetElement = (Element)nl.item(i);
				LevelAccessAsset asset = LevelAccessAsset.CreateAsset( assetElement, typeName );
				mAssets.put(asset.getName(), asset);
			}
		}
	}
}
