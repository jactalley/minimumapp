package model;

import org.w3c.dom.Element;

public class LevelAccessAsset 
{
	public static LevelAccessAsset CreateAsset( Element xmlElement, String type )
	{
		LevelAccessAsset asset = new LevelAccessAsset();
		
		asset.setType( type );
		asset.setName( xmlElement.getAttribute("name") );
		asset.setAccessLevel( Integer.parseInt(xmlElement.getAttribute("level")) );
		
		return asset;
	}
	
	public int getAccessLevel() {
		return mAccessLevel;
	}
	public void setAccessLevel(int AccessLevel) {
		this.mAccessLevel = AccessLevel;
	}
	public String getName() {
		return mName;
	}
	public void setName(String Name) {
		this.mName = Name;
	}
	public String getType() {
		return mType;
	}
	public void setType(String Type) {
		this.mType = Type;
	}
	
	private int		mAccessLevel;
	private String	mName;
	private String	mType;
}
